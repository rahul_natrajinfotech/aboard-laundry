package com.natrajinfotech.aboardlaundry.adapter

import android.app.Activity
import android.content.Context
import android.support.v4.app.ActivityCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.model.GetOrderHistoryResponseModel
import kotlinx.android.synthetic.main.row_order_history.view.*

/**
 * Created by Dipendra Sharma  on 22-01-2019.
 */
class RvOrderHistoryAdaptor(
    var mContext: Context,
    var mActivity: Activity,
    var orderList: ArrayList<GetOrderHistoryResponseModel.OrderHistoryData>,
    var clickLisner: View.OnClickListener
) : RecyclerView.Adapter<RvOrderHistoryAdaptor.OrderHistoryViewHolder>() {
    private var orderStatusArray = arrayOf(
        "Order Placed",
        "Out for Pickup",
        "In Process",
        "Laundry is Cleaned",
        "Out for Delivery",
        "Order Delivered",
        "Order Cancelled"
    )
    private var orderStatusColorArray = arrayOf(
        R.color.colorGreen,
        R.color.colorOrderOrange,
        R.color.colorOrderOrange,
        R.color.colorGreen,
        R.color.colorOrderOrange,
        R.color.colorGreen,
        R.color.colorOrderRed
    )

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): OrderHistoryViewHolder {
        return OrderHistoryViewHolder(LayoutInflater.from(mContext).inflate(R.layout.row_order_history, p0, false))
    }

    override fun getItemCount(): Int {
        return orderList.size
    }

    override fun onBindViewHolder(p0: OrderHistoryViewHolder, p1: Int) {
        val orderData = orderList[p1]
        var servicesData = ""
        if (orderData.item_details?.size == 0) {
            servicesData = "Quick Order"
        }
        for (i in orderData.item_details!!.indices) {
            if (servicesData.isEmpty()) {
                servicesData = orderData.item_details!![i].services_name
            } else {
                servicesData = servicesData + "/" + orderData.item_details!![i].services_name
            }

        }
        p0.tvOrderServices.text = servicesData
        p0.tvOrderPlaceDate.text = "Place On : " + orderData.order_date





        p0.tvOrderStatus.text = "Status :" + orderData.order_status

        for (j in orderStatusArray.indices) {
            if (orderStatusArray[j].equals(orderData.order_status)) {
                p0.tvOrderStatus.setTextColor(ActivityCompat.getColor(mContext, orderStatusColorArray[j]))
            }
        }


        p0.tvOrderId.text = "Order Id :" + orderData.order_number
        p0.tvPrice.text = "Price :" + orderData.total
        if (orderData.delivery_type.equals("Normal")) {
            p0.imgDeliveryType.setImageResource(R.drawable.normal_delivery)
        } else {
            p0.imgDeliveryType.setImageResource(R.drawable.express_delivery)
        }
        if (orderData.order_status.equals("Order Placed")) {
            p0.imgCancelOrder.visibility = View.VISIBLE

        } else {
            p0.imgCancelOrder.visibility = View.GONE

        }
        p0.tvViewOrder.tag = p1
        p0.imgCancelOrder.tag = p1
        p0.imgTrackOrder.tag = p1

        p0.tvViewOrder.setOnClickListener(clickLisner)
        p0.imgCancelOrder.setOnClickListener(clickLisner)
        p0.imgTrackOrder.setOnClickListener(clickLisner)
    }

    class OrderHistoryViewHolder(mView: View) : RecyclerView.ViewHolder(mView) {
        val tvOrderServices = mView.tvType
        val tvOrderPlaceDate = mView.tvOrderPlaceDate
        val tvOrderId = mView.tvOrderId
        val tvPrice = mView.tvPrice
        val tvOrderStatus = mView.tvOrderStatus
        val tvViewOrder = mView.tvViewOrder
        val imgCancelOrder = mView.imgCancelOrder
        val imgTrackOrder = mView.imgTrackOrder
        val imgDeliveryType = mView.imgDeliveryType

    }
}