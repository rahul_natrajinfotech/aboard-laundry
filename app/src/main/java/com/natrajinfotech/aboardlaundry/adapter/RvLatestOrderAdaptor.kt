package com.natrajinfotech.aboardlaundry.adapter

import android.app.Activity
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.model.GetLatestOrderResponseModel
import kotlinx.android.synthetic.main.row_latest_order.view.*

/**
 * Created by Dipendra Sharma  on 11-01-2019.
 */
class RvLatestOrderAdaptor(var mContext:Context,var mActivity:Activity,var latestOrderList:ArrayList<GetLatestOrderResponseModel.LatestOrderData>,var clickLisner:View.OnClickListener):RecyclerView.Adapter<RvLatestOrderAdaptor.LatestOrderViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): LatestOrderViewHolder {
return LatestOrderViewHolder(LayoutInflater.from(mContext).inflate(R.layout.row_latest_order,p0,false))
    }

    override fun getItemCount(): Int {

        return latestOrderList.size
    }

    override fun onBindViewHolder(p0: LatestOrderViewHolder, p1: Int) {
        val latestData=latestOrderList[p1]
        p0.tvOrderId.text="Order Id :"+latestData.order_id
        p0.tvOrderStatus.text=latestData.order_status
p0.cvLatestMain.tag=p1
        p0.cvLatestMain.setOnClickListener(clickLisner)
    }


    class LatestOrderViewHolder(mView:View):RecyclerView.ViewHolder(mView){
val tvOrderId=mView.tvOrderId
        val tvOrderStatus=mView.tvOrderStatus
val cvLatestMain=mView.cvLatestMain
    }
}