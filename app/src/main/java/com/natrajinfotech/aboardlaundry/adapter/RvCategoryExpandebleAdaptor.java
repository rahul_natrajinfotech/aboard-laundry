package com.natrajinfotech.aboardlaundry.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.UiThread;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.natrajinfotech.aboardlaundry.R;
import com.natrajinfotech.aboardlaundry.model.GetItemListResponseModel;
import com.natrajinfotech.aboardlaundry.utils.CategoryDetailViewHolder;
import com.natrajinfotech.aboardlaundry.utils.CategoryItemDetailViewHolder;
import com.natrajinfotech.aboardlaundry.utils.ExpandableRecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

public class RvCategoryExpandebleAdaptor extends ExpandableRecyclerAdapter<GetItemListResponseModel.GroupListData, GetItemListResponseModel.GroupItemListData, CategoryDetailViewHolder, CategoryItemDetailViewHolder>  {


private Context mContext;
private Activity mActivity;
    private LayoutInflater mInflater;
    public List<GetItemListResponseModel.GroupListData> mCategoryList=new ArrayList<>();
    public List<GetItemListResponseModel.GroupListData> mFilterCategoryList;

    public RvCategoryExpandebleAdaptor(Context context,Activity mActivity, @NonNull List<GetItemListResponseModel.GroupListData> categoryList) {
        super(categoryList);
        mCategoryList=new ArrayList<>();
        mCategoryList = categoryList;
        mFilterCategoryList=new ArrayList<GetItemListResponseModel.GroupListData>();
        mFilterCategoryList.addAll(categoryList);

        mInflater = LayoutInflater.from(context);
        mContext=context;
        this.mActivity=mActivity;
    }

    @UiThread
    @NonNull
    @Override
    public CategoryDetailViewHolder onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
        View categoryView;
                categoryView = mInflater.inflate(R.layout.row_category_detail_view, parentViewGroup, false);
                  return new CategoryDetailViewHolder(categoryView,mContext);
    }

    @UiThread
    @NonNull
    @Override
    public CategoryItemDetailViewHolder onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {
        View categoryItemView;
                categoryItemView = mInflater.inflate(R.layout.row_category_item_detail, childViewGroup, false);

                      return new CategoryItemDetailViewHolder(categoryItemView,mContext,mActivity);
    }

    @UiThread
    @Override
    public void onBindParentViewHolder(@NonNull CategoryDetailViewHolder categoryViewHolder, int parentPosition, @NonNull GetItemListResponseModel.GroupListData category) {
        categoryViewHolder.bind(category);
    }

    @UiThread
    @Override
    public void onBindChildViewHolder(@NonNull CategoryItemDetailViewHolder categoryItemViewHolder, int parentPosition, int childPosition, @NonNull GetItemListResponseModel.GroupItemListData categoryItem) {
        categoryItemViewHolder.bind(categoryItem);
    }

    @Override
    public int getParentViewType(int parentPosition) {

        return 0;
    }

    @Override
    public int getChildViewType(int parentPosition, int childPosition) {

        return 3;
    }

    @Override
    public boolean isParentViewType(int viewType) {
        return viewType == 0|| viewType == 1;
    }


    public void SetFilterSearchArrayItems(List<GetItemListResponseModel.GroupListData> itemListData) {
        this.mCategoryList= itemListData;
        Log.e("method_changed","Method Called");
// notifyDataSetChanged();
        notifyParentDataSetChanged(true);
    }
}
