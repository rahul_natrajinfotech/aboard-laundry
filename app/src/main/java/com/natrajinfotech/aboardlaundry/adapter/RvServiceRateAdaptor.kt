package com.natrajinfotech.aboardlaundry.adapter

import android.app.Activity
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.model.GetItemServiceRateResponseModel
import kotlinx.android.synthetic.main.row_item_services_rate.view.*

/**
 * Created by Dipendra Sharma  on 05-01-2019.
 */
class RvServiceRateAdaptor(var mContext:Context,var mActivity:Activity,var servicesList:ArrayList<GetItemServiceRateResponseModel.ItemServiceRateData>):RecyclerView.Adapter<RvServiceRateAdaptor.ServicesViewHolder>() {
    var mAdaptor:RvServiceRateAdaptor
    init {
        mAdaptor=this
    }
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ServicesViewHolder {
return ServicesViewHolder(LayoutInflater.from(mContext).inflate(R.layout.row_item_services_rate,p0,false))
    }

    override fun getItemCount(): Int {
        return servicesList.size
    }

    override fun onBindViewHolder(p0: ServicesViewHolder, p1: Int) {
        val serviceData=servicesList[p1]
        Glide.with(mContext)
            .load(serviceData.icon_path)
            .placeholder(R.mipmap.ic_launcher)
            .into(p0.imgService)
        p0.tvServiceName.text=serviceData.services_name
        val serviceRate="Price : "+serviceData.rate
        p0.tvServicePrice.text=serviceRate
        p0.tvQuantity.text=serviceData.quantity.toString()
        p0.tvQuantity.tag=p1
        p0.tvDecreaseQuantity.tag=p1
        p0.tvIncreaseQuantity.tag=p1
        p0.tvIncreaseQuantity.setOnClickListener(object :View.OnClickListener {
            override fun onClick(p0: View?) {
val currentPosition:Int= p0!!.tag as Int

                val data=servicesList[currentPosition]
                val increaseQuantity=data.quantity+1
                servicesList.set(currentPosition,GetItemServiceRateResponseModel.ItemServiceRateData(data.id,data.services_name,data.icons,data.rate,data.icon_path,increaseQuantity))
                mAdaptor.notifyItemChanged(currentPosition)
            }


        })
        p0.tvDecreaseQuantity.setOnClickListener(object :View.OnClickListener {
            override fun onClick(p0: View?) {
                val currentPosition:Int= p0!!.tag as Int

                val data=servicesList[currentPosition]
                if (data.quantity>0){
                val decreaseQuantity=data.quantity-1
                servicesList.set(currentPosition,GetItemServiceRateResponseModel.ItemServiceRateData(data.id,data.services_name,data.icons,data.rate,data.icon_path,decreaseQuantity))
                mAdaptor.notifyItemChanged(currentPosition)
                }
            }


        })
    }

    class ServicesViewHolder(mView:View):RecyclerView.ViewHolder(mView){
val imgService=mView.imgService
        val tvServiceName=mView.tvServiceName
        val tvServicePrice=mView.tvServiceRate
        val tvDecreaseQuantity=mView.tvDecreaseQuantity
        val tvIncreaseQuantity=mView.tvIncreaseQuantity
        val tvQuantity=mView.tvItemQuantity


    }

}