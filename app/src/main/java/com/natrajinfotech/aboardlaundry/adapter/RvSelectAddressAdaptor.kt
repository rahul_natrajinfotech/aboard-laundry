package com.natrajinfotech.aboardlaundry.adapter

import android.app.Activity
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.model.GetUserAddressListResponceModel
import com.natrajinfotech.aboardlaundry.utils.AppSharedPreference
import kotlinx.android.synthetic.main.row_select_address.view.*

/**
 * Created by Dipendra Sharma  on 12-01-2019.
 */
class RvSelectAddressAdaptor(var mContext:Context,var mActivity:Activity,var addressList:ArrayList<GetUserAddressListResponceModel.AddressListData>,var clickLisner:View.OnClickListener,var from:String):RecyclerView.Adapter<RvSelectAddressAdaptor.SelectAddressViewHolder>() {


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): SelectAddressViewHolder {
return SelectAddressViewHolder(LayoutInflater.from(mContext).inflate(R.layout.row_select_address,p0,false))
    }

    override fun getItemCount(): Int {
        return addressList.size
    }

    override fun onBindViewHolder(p0: SelectAddressViewHolder, p1: Int) {
        val addressData=addressList[p1]
        AppSharedPreference(mContext).getInt("addressId")
p0.rbSelect.isChecked=addressData.isSelectedAddress
        p0.tvName.text=addressData.name
        p0.tvAddress.text=addressData.block_no+" ,"+addressData.building_no+" ,"+addressData.street_no+" ,"+addressData.area_name
        p0.tvMobile.text=addressData.phone_number
        p0.rbSelect.tag=p1
        p0.btnEditAddress.tag=p1
        p0.btnDeleteAddress.tag=p1
p0.rbSelect.setOnClickListener(clickLisner)
        p0.btnEditAddress.setOnClickListener(clickLisner)
p0.btnDeleteAddress.setOnClickListener(clickLisner)
//this condition will apply in case when we will call this adaptor from user address settting class.
        if (from.equals("profile")){
            p0.rbSelect.visibility=View.INVISIBLE
        }else{
            p0.rbSelect.visibility=View.VISIBLE
        }
    }


    class SelectAddressViewHolder(mView:View):RecyclerView.ViewHolder(mView){
    val rbSelect=mView.rdAddress
    val tvName=mView.tvName
        val tvAddress=mView.tvAddress
        val tvMobile=mView.tvMobile
val btnEditAddress=mView.btnEditAddress
val btnDeleteAddress=mView.btnDeleteAddress
    }
}