package com.natrajinfotech.aboardlaundry.adapter

import android.app.Activity
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.model.GetNotificationResponseModel
import kotlinx.android.synthetic.main.row_notification_data.view.*



/**
 * Created by Dipendra Sharma  on 30-01-2019.
 */
class RvNotificationAdaptor(var mContext:Context,var mActivity:Activity,var notificationList:ArrayList<GetNotificationResponseModel.NotificationData>):RecyclerView.Adapter<RvNotificationAdaptor.NotificationViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): NotificationViewHolder {
return NotificationViewHolder(LayoutInflater.from(mContext).inflate(R.layout.row_notification_data,p0,false))
    }

    override fun getItemCount(): Int {
        return notificationList.size
    }

    override fun onBindViewHolder(p0: NotificationViewHolder, p1: Int) {
        val data=notificationList[p1]
if (data.type.equals("offer")){
    p0.imgNotification.setImageResource(R.drawable.offers)

    Log.v("dip","test")
}else if (data.type.equals("order_ready")){
p0.imgNotification.setImageResource(R.drawable.order_ready)
}else if (data.type.equals("pickup")){
    p0.imgNotification.setImageResource(R.drawable.driver_is_ready_for_pickup)

}
        p0.tvNotificationHeader.text=data.title
        p0.tvNotificationDetail.text=data.description


    }

    class NotificationViewHolder(mView:View):RecyclerView.ViewHolder(mView){
val viewBackGround= mView.view_background!!
        val viewForground=mView.view_foreground
val cvMainNotification=mView.cvMainNotification
        val imgNotification=mView.imgNotification
        val tvNotificationHeader=mView.tvNotificationHeader
        val tvNotificationDetail=mView.tvNotificationDetail
        val imgViewNotification=mView.imgViewNotification
    }

    fun removeItem(position: Int) {
        notificationList.removeAt(position)
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position)
    }

    fun restoreItem(item: GetNotificationResponseModel.NotificationData, position: Int) {
        notificationList.add(position, item)
        // notify item added by position
        notifyItemInserted(position)
    }


}