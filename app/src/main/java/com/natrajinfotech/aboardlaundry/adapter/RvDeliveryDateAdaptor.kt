package com.natrajinfotech.aboardlaundry.adapter

import android.app.Activity
import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.model.CalenderDataModel

import kotlinx.android.synthetic.main.row_grid_select_delivery_date.view.*

/**
 * Created by Dipendra Sharma  on 18-01-2019.
 */
class RvDeliveryDateAdaptor(var mContext: Context, var mActivity: Activity, var dateList:ArrayList<CalenderDataModel>, var clickLisner: View.OnClickListener):
    RecyclerView.Adapter<RvDeliveryDateAdaptor.CalenderViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): CalenderViewHolder {
        return CalenderViewHolder(LayoutInflater.from(mContext).inflate(R.layout.row_grid_select_delivery_date,p0,false))
    }

    override fun getItemCount(): Int {
        return dateList.size
    }

    override fun onBindViewHolder(p0: CalenderViewHolder, p1: Int) {
        val date=dateList[p1]
        Log.v("dip","date :"+date.date)
        p0.tvDate.text=date.date
        p0.tvDay.text=date.day
        p0.llDateContainer.tag=p1
        p0.llDateContainer.setOnClickListener(clickLisner)
        Log.v("dip","is date current :"+date.isCurrentDate)
        if (date.isCurrentDate){
            p0.tvDay.setTextColor(ContextCompat.getColor(mContext,R.color.colorAccent))
            p0.tvDate.setTextColor(ContextCompat.getColor(mContext,R.color.colorAccent))
        }else{
if (date.day.equals("sun")){
    p0.tvDay.setTextColor(ContextCompat.getColor(mContext,R.color.colorRed))
    p0.tvDate.setTextColor(ContextCompat.getColor(mContext,R.color.colorRed))

}else{
            p0.tvDay.setTextColor(ContextCompat.getColor(mContext,R.color.colorBlack))
            p0.tvDate.setTextColor(ContextCompat.getColor(mContext,R.color.colorBlack))
}

        }
        //for setting background of selected date:
        if (date.isSelectedDate){
            p0.llDateContainer.background= ContextCompat.getDrawable(mContext, R.drawable.bg_cal_date_gray_orange_stroke)
        }else{
            p0.llDateContainer.background= ContextCompat.getDrawable(mContext, R.drawable.bg_cal_date_gray)
        }

    }


    class CalenderViewHolder(mView: View): RecyclerView.ViewHolder(mView){
        val tvDate=mView.tvDate
        val tvDay=mView.tvDay
        val llDateContainer=mView.llDeliveryDateContener
    }
}