package com.natrajinfotech.aboardlaundry.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.UiThread;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.natrajinfotech.aboardlaundry.R;
import com.natrajinfotech.aboardlaundry.model.OrderBasketDbModel;
import com.natrajinfotech.aboardlaundry.model.OrderBasketListModel;
import com.natrajinfotech.aboardlaundry.utils.*;

import java.util.List;

/**
 * Created by Dipendra Sharma  on 08-01-2019.
 */
public class RvBasketAdaptor extends ExpandableRecyclerAdapter<OrderBasketListModel, OrderBasketDbModel, BasketServiceViewHolder, BasketItemViewHolder> {


    private Context mContext;
    private Activity mActivity;
    private LayoutInflater mInflater;
    public List<OrderBasketListModel> mServicesList;
RvBasketAdaptor mAdaptor;

    public RvBasketAdaptor(Context context, Activity mActivity, @NonNull List<OrderBasketListModel> serviceBasketList) {
        super(serviceBasketList);
        mServicesList = serviceBasketList;
        mInflater = LayoutInflater.from(context);
        mContext=context;
        this.mActivity=mActivity;
        this.mAdaptor=this;
    }

    @UiThread
    @NonNull
    @Override
    public BasketServiceViewHolder onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
        View serviceView;
        serviceView = mInflater.inflate(R.layout.row_mybasket_detail, parentViewGroup, false);
        return new BasketServiceViewHolder(serviceView);
    }

    @UiThread
    @NonNull
    @Override
    public BasketItemViewHolder onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {
        View basketItemView;
        basketItemView = mInflater.inflate(R.layout.row_mybasket_details, childViewGroup, false);

        return new BasketItemViewHolder(basketItemView,mContext,mActivity,mAdaptor);
    }

    @UiThread
    @Override
    public void onBindParentViewHolder(@NonNull BasketServiceViewHolder serviceViewHolder, int parentPosition, @NonNull OrderBasketListModel services) {
        serviceViewHolder.bind(services);
    }

    @UiThread
    @Override
    public void onBindChildViewHolder(@NonNull BasketItemViewHolder BasketItemViewHolder, int parentPosition, int childPosition, @NonNull OrderBasketDbModel basketItem) {
        BasketItemViewHolder.bind(basketItem,parentPosition,childPosition);
    }

    @Override
    public int getParentViewType(int parentPosition) {

        return 0;
    }

    @Override
    public int getChildViewType(int parentPosition, int childPosition) {

        return 3;
    }

    @Override
    public boolean isParentViewType(int viewType) {
        return viewType == 0|| viewType == 1;
    }

}

