package com.natrajinfotech.aboardlaundry.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.model.GetCancelReasonResponseModel
import kotlinx.android.synthetic.main.row_reason_list.view.*

/**
 * Created by Dipendra Sharma  on 25-02-2019.
 */
class RvCancelReasonAdaptor(var mContext:Context,var reasonList:ArrayList<GetCancelReasonResponseModel.CancelReasonData>,var clickLisner:View.OnClickListener):RecyclerView.Adapter<RvCancelReasonAdaptor.CancelReasonViewHolder>() {


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): CancelReasonViewHolder {
return CancelReasonViewHolder(LayoutInflater.from(mContext).inflate(R.layout.row_reason_list,p0,false))
    }

    override fun getItemCount(): Int {
        return reasonList.size
    }

    override fun onBindViewHolder(p0: CancelReasonViewHolder, p1: Int) {
        val reasonData=reasonList[p1]
p0.tvReason.text=reasonData.reason
            p0.rbReason.isChecked=reasonData.isCheck
p0.rbReason.tag=p1
        p0.rbReason.setOnClickListener(clickLisner)

    }

    class CancelReasonViewHolder(mView:View):RecyclerView.ViewHolder(mView){
        val rbReason= mView.rbReason!!
        val tvReason=mView.tvReason

    }

}