package com.natrajinfotech.aboardlaundry.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.natrajinfotech.aboardlaundry.R;
import com.natrajinfotech.aboardlaundry.model.HomeSliderModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SPECBEE on 8/4/2017.
 */

public class HomePageSliderAdapter extends PagerAdapter {
    private Context context;
    private List<String> colorName;
    private ArrayList<HomeSliderModel> sliderList;
private View.OnClickListener clickLisner;
    public HomePageSliderAdapter(Context context, List<String> colorName, ArrayList<HomeSliderModel> sliderList, View.OnClickListener onClick) {
        this.context = context;
        this.colorName = colorName;
        this.sliderList = sliderList;
        this.clickLisner=onClick;
    }

    @Override
    public int getCount() {
        return colorName.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item_slider, null);

        TextView textView = (TextView) view.findViewById(R.id.textView);
        TextView tvTitle = (TextView) view.findViewById(R.id.tvSliderTitle);
        TextView tvMsg = (TextView) view.findViewById(R.id.tvSliderMessage);
        TextView tvQuickOrder = (TextView) view.findViewById(R.id.tvQuickOrder);
        TextView tvSelectCloths = (TextView) view.findViewById(R.id.tvSelectCloths);
        ImageView imgSlider = (ImageView) view.findViewById(R.id.imgSlider);
        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.linearLayout);



        tvTitle.setText(sliderList.get(position).getSliderTitle());
        tvMsg.setText(sliderList.get(position).getSliderMsg());
        tvQuickOrder.setOnClickListener(clickLisner);
        tvSelectCloths.setOnClickListener(clickLisner);

        imgSlider.setImageDrawable(context.getResources().getDrawable(sliderList.get(position).getSliderImage()));

        ViewPager viewPager = (ViewPager) container;
        viewPager.addView(view, 0);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ViewPager viewPager = (ViewPager) container;
        View view = (View) object;
        viewPager.removeView(view);
    }
}
