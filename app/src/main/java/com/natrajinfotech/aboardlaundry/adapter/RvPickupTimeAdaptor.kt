package com.natrajinfotech.aboardlaundry.adapter

import android.app.Activity
import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.natrajinfotech.aboardlaundry.R
import kotlinx.android.synthetic.main.row_grid_select_time.view.*

/**
 * Created by Dipendra Sharma  on 18-01-2019.
 */
class RvPickupTimeAdaptor(var mContext:Context,var mActivity:Activity,var timeArray:Array<String>,var clickLisner:View.OnClickListener):RecyclerView.Adapter<RvPickupTimeAdaptor.PickupTimeViewHolder>() {
var  mAdaptor:RvPickupTimeAdaptor
    var selectedPosition=-1
    init {
        mAdaptor=this
    }
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): PickupTimeViewHolder {
return PickupTimeViewHolder(LayoutInflater.from(mContext).inflate(R.layout.row_grid_select_time,p0,false))
    }

    override fun getItemCount(): Int {
        return timeArray.size
    }

    override fun onBindViewHolder(p0: PickupTimeViewHolder, p1: Int) {
p0.tvTimeSlot.text=timeArray[p1]
        p0.llTimeContainer.tag=p1
        p0.llTimeContainer.setOnClickListener(clickLisner)
        if (p1==selectedPosition){
            p0.llTimeContainer.background=ContextCompat.getDrawable(mContext,R.drawable.bg_cal_time_gray_orange_stroke)
        }else{
            p0.llTimeContainer.background=ContextCompat.getDrawable(mContext,R.drawable.bg_cal_time_gray)

        }
    }

    class PickupTimeViewHolder(mView:View):RecyclerView.ViewHolder(mView){
val tvTimeSlot=mView.tvTimeSlot
val llTimeContainer=mView.llTimeContainer
    }

    fun selecteSlot(userPosition:Int){
        this.selectedPosition=userPosition
        mAdaptor.notifyDataSetChanged()

    }

}