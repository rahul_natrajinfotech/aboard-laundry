package com.natrajinfotech.aboardlaundry.ui

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.view.View
import android.widget.Toast
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.RetroFitHelper.UserManagement
import com.natrajinfotech.aboardlaundry.utils.BaseActivity


import com.natrajinfotech.aboardlaundry.utils.NetworkHandler
import com.natrajinfotechindia.brainiton.utils.AppAlerts
import com.natrajinfotech.aboardlaundry.utils.AppProgressDialog
import kotlinx.android.synthetic.main.activity_welcome_aboard.*


class SignInActivity : BaseActivity(), View.OnClickListener {
    var email: String = ""
    var password: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome_aboard)
        inItView()
    }
//L̥
    private fun inItView() {
        ivNext.setOnClickListener(this@SignInActivity)
        tvRegister.setOnClickListener {
            val mDialog = AppProgressDialog(this@SignInActivity)
            mDialog.show()
            UserManagement(this@SignInActivity, this@SignInActivity, mDialog).getAreaList()
        }

        tvForgotPassword.setOnClickListener {
            val intent= Intent(this,ForgotPasswordActivity::class.java)
            startActivity(intent)
            //showToast(mContext, "Coming soon!")
        }

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.ivNext -> {
                getData()
                if (isValidInput()) {
                    if (NetworkHandler(mContext).isNetworkAvailable()) {

                        doLogin()
                    } else {
                        AppAlerts().showAlertMessage(
                            this@SignInActivity,
                            "Info",
                            resources.getString(R.string.networkError)
                        )

                    }

                }
            }

        }
    }

    private fun doLogin() {
        val mDialog = AppProgressDialog(this@SignInActivity)
        mDialog.show()
        val requestData = HashMap<String, String>()
        requestData["email"] = email
        requestData["password"] = password
        UserManagement(this@SignInActivity, this@SignInActivity, mDialog, requestData).doLogin()
    }

    private fun isValidInput(): Boolean {
        if (email.isEmpty() && password.isEmpty()) {
            Toast.makeText(this@SignInActivity, "Please enter valid input", Toast.LENGTH_LONG).show()
            return false
        } else if (email.isEmpty() || !isValidEmailId(email)) {
            etEmail.setError(resources.getString(R.string.emailError))
            return false
        } else if (password.isEmpty()) {
            etPassword.setError(resources.getString(R.string.passwordError))
            return false
        } else {
            return true

        }
    }

    private fun getData() {
        email = etEmail.text.toString().trim()
        password = etPassword.text.toString().trim()

    }

}

//For Email Validation
private fun isValidEmailId(email: CharSequence): Boolean {
    return if (!TextUtils.isEmpty(email)) {
        Patterns.EMAIL_ADDRESS.matcher(email).matches()
    } else false
}

