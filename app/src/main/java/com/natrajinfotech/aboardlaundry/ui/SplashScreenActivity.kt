package com.natrajinfotech.aboardlaundry.ui

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.RetroFitHelper.UserManagement
import com.natrajinfotech.aboardlaundry.utils.AppSharedPreference
import com.natrajinfotech.aboardlaundry.utils.AppProgressDialog
import java.util.*

    class SplashScreenActivity:AppCompatActivity(), View.OnClickListener {
    private lateinit var permissionsNeeded:ArrayList<String>
    private lateinit var permissionsList: ArrayList<String>
    private val REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        permissionsNeeded = ArrayList()
        permissionsList = ArrayList()
        permitions()
        }

    override fun onClick(v: View?) {
    }

//for run time permission
private fun permitions() {

    if (!addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE))
        permissionsNeeded.add("Read Your SD Card")

    if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
        permissionsNeeded.add("WRITE_EXTERNAL_STORAGE")

    if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
        permissionsNeeded.add("Get Your Location")
    if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION))
        permissionsNeeded.add("Get Your COARSE Location")

    if (permissionsList.size > 0) {
        if (permissionsNeeded.size > 0)         {
            // Need Rationale
            /*String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                showMessageOKCancel(message,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(SplashActivity.this, permissionsList.toArray(new String[permissionsList.size()]),
                                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                            }
                        });*/
            ActivityCompat.requestPermissions(
                this, permissionsList.toTypedArray(),
                REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS
            )
            return
        }
        ActivityCompat.requestPermissions(
            this, permissionsList.toTypedArray(),
            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS
        )
        return
    }

    moveToNextActivity()

}

    private fun moveToNextActivity() {
        Handler().postDelayed({
            try {

                val isLogin= AppSharedPreference(this@SplashScreenActivity).getBoolean("isLogin")
                if (isLogin){
                    val mDialog= AppProgressDialog(this@SplashScreenActivity)
                    mDialog.show()
                    UserManagement(this@SplashScreenActivity, this@SplashScreenActivity, mDialog).getCompanyDetail()

                }else{
                    val intent= Intent(baseContext, WalkThroughActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }catch (e: Exception){
                e.printStackTrace()
            }
        },3000)


    }


    private fun addPermission(permissionsList: MutableList<String>, permission: String): Boolean {
        if (ContextCompat.checkSelfPermission(this@SplashScreenActivity, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission)
            // Check for Rationale Option
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this@SplashScreenActivity, permission))
                return false
        }
        return true
    }


    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS -> {
                val perms = HashMap<String, Int>()
                // Initial
                perms[Manifest.permission.WRITE_EXTERNAL_STORAGE] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.READ_EXTERNAL_STORAGE] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.ACCESS_FINE_LOCATION] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.ACCESS_COARSE_LOCATION] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.CAMERA] = PackageManager.PERMISSION_GRANTED


                // Fill with results
                for (i in permissions.indices)
                    //perms[permissions[i]] = grantResults[i]
                // Check for ACCESS_FINE_LOCATION
                if (perms[Manifest.permission.WRITE_EXTERNAL_STORAGE] == PackageManager.PERMISSION_GRANTED
                    && perms[Manifest.permission.READ_EXTERNAL_STORAGE] == PackageManager.PERMISSION_GRANTED
                    && perms[Manifest.permission.ACCESS_FINE_LOCATION] == PackageManager.PERMISSION_GRANTED
                    && perms[Manifest.permission.ACCESS_COARSE_LOCATION] == PackageManager.PERMISSION_GRANTED
                    && perms[Manifest.permission.CAMERA] == PackageManager.PERMISSION_GRANTED) {
                    // All Permissions Granted
                    // insertDummyContact();
                    moveToNextActivity()
                } else {
                    // Permission Denied
                    Toast.makeText(this, "Some Permission is Denied", Toast.LENGTH_SHORT)
                        .show()
                    //moveToNextActivity()
                }
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }


}
