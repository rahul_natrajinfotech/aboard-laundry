package com.natrajinfotech.aboardlaundry.ui

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Menu
import android.view.View
import com.bumptech.glide.Glide
import com.j256.ormlite.android.apptools.OpenHelperManager
import com.natrajinfotech.aboardlaundry.Database.OrmLiteDb
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.RetroFitHelper.ItemManagement
import com.natrajinfotech.aboardlaundry.adapter.RvServiceRateAdaptor
import com.natrajinfotech.aboardlaundry.model.GetItemServiceRateResponseModel
import com.natrajinfotech.aboardlaundry.model.OrderBasketDbModel
import com.natrajinfotech.aboardlaundry.utils.AppProgressDialog
import com.natrajinfotech.aboardlaundry.utils.BaseActivity
import kotlinx.android.synthetic.main.activity_select_types.*


class SelectServicesActivity : BaseActivity(), View.OnClickListener {
    lateinit var mAdaptor: RvServiceRateAdaptor
    private var itemName: String = ""
    private var itemImage: String = ""
    private var itemId: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_types)

showHomeBackOnToolbar(toolBar)
        this.title = "Select Type"


        inItView()
    }

    private fun inItView() {
        itemName = intent.getStringExtra("itemName")
        itemImage = intent.getStringExtra("itemImage")
        itemId = intent.getStringExtra("itemId")
        tvItemName.text = itemName
        Glide.with(this@SelectServicesActivity)
            .load(itemImage)
            .placeholder(R.mipmap.ic_launcher)
            .into(imgItem)

        @Suppress("UNCHECKED_CAST")
        val servicesList: ArrayList<GetItemServiceRateResponseModel.ItemServiceRateData>? =
            intent.getSerializableExtra("rateList") as ArrayList<GetItemServiceRateResponseModel.ItemServiceRateData>?
        for (i in servicesList!!.indices) {
            val id = servicesList[i].id.toString()
            val queryBuilder = gethelper().getBucketDao().queryBuilder()
            val where = queryBuilder.where()
            where.eq("itemId", itemId)
            where.and()
            where.eq("servicesId", id)
            val prepairQuery = queryBuilder.prepare()
            val previousBasketList = gethelper().getBucketDao().query(prepairQuery)


            Log.v("dip", "basket size :" + previousBasketList.size)
            if (previousBasketList.size > 0) {
                servicesList[i] = GetItemServiceRateResponseModel.ItemServiceRateData(
                    servicesList[i].id,
                    servicesList[i].services_name,
                    servicesList[i].icons,
                    servicesList[i].rate,
                    servicesList[i].icon_path,
                    previousBasketList[0].servicesItemQuantity.toInt()
                )
            }
        }
        rvServices.layoutManager = LinearLayoutManager(this@SelectServicesActivity)

        mAdaptor = RvServiceRateAdaptor(this@SelectServicesActivity, this@SelectServicesActivity, servicesList)
        rvServices.adapter = mAdaptor
        tvAddBasket.setOnClickListener {
            insertUpdateItemIntoDb()

        }
    }

    private fun insertUpdateItemIntoDb() {
        val mDialog = AppProgressDialog(this@SelectServicesActivity)
        mDialog.show()
        try {
            var serviceId: String
            var serviceName: String
            var serviceRate: String
            var serviceQuantity: String
            var servicesImagePath: String
            for (i in mAdaptor.servicesList.indices) {
                serviceId = mAdaptor.servicesList[i].id.toString()
                serviceName = mAdaptor.servicesList[i].services_name
                serviceRate = mAdaptor.servicesList[i].rate.toString()
                serviceQuantity = mAdaptor.servicesList[i].quantity.toString()
                servicesImagePath = mAdaptor.servicesList[i].icon_path
                val queryBuilder = gethelper().getBucketDao().queryBuilder()
                val where = queryBuilder.where()
                where.eq("itemId", itemId)
                where.and()
                where.eq("servicesId", serviceId)
                val prepairQuery = queryBuilder.prepare()
                val basketItemList = gethelper().getBucketDao().query(prepairQuery)
                if (basketItemList.size == 0 && mAdaptor.servicesList[i].quantity > 0) {

                    val insertModel = OrderBasketDbModel(
                        itemId,
                        itemName,
                        itemImage,
                        serviceId,
                        serviceName,
                        serviceRate,
                        serviceQuantity,
                        servicesImagePath
                    )
                    gethelper().getBucketDao().create(insertModel)

                } else if (basketItemList.size > 0 && mAdaptor.servicesList[i].quantity == 0) {
                    val deleteBuilder = gethelper().getBucketDao().deleteBuilder()

                    val mWhere =deleteBuilder.where()
                    mWhere.eq("itemId", itemId)
                    mWhere.and()
                    mWhere.eq("servicesId", serviceId)
                    deleteBuilder.prepare()

                    deleteBuilder.delete()

                } else if (basketItemList.size > 0 && mAdaptor.servicesList[i].quantity > 0) {
                    val updateBuilder = gethelper().getBucketDao().updateBuilder()
                    //updateBuilder!!.updateColumnValue("itemName" /* column */, itemName/* value */)
                    //updateBuilder!!.updateColumnValue("itemImagePath" /* column */, itemImage/* value */)
                    //updateBuilder!!.updateColumnValue("servicesId" /* column */, serviceId/* value */)
                    //updateBuilder!!.updateColumnValue("servicesName" /* column */, serviceName/* value */)
                    updateBuilder!!.updateColumnValue("serviceRate" /* column */, serviceRate/* value */)
                    updateBuilder.updateColumnValue("servicesItemQuantity" /* column */, serviceQuantity/* value */)
                    //updateBuilder!!.updateColumnValue("servicesImagePath" /* column */, servicesImagePath/* value */)
                                    val mWhere =updateBuilder.where()
                    mWhere.eq("itemId", itemId)
                    mWhere.and()
                    mWhere.eq("servicesId", serviceId)
                    updateBuilder.prepare()

                    updateBuilder.update()


                }

            }
        } catch (e: java.lang.Exception) {
            Log.v("dip", "insert or update error :" + e.message)

        }
        //for going to again category page
        //mDialog.dialog.dismiss()
        //startActivity(Intent(this@SelectServicesActivity, MyBasketActivity::class.java))
        //finish()
        ItemManagement(this@SelectServicesActivity,this@SelectServicesActivity,mDialog).gettingItemList()

    }


    override fun onClick(v: View?) {
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.select_type, menu)
        return true

    }

    //For Database
    private fun gethelper(): OrmLiteDb {

        var ormLiteDb: OrmLiteDb? = null
        if (ormLiteDb == null) {
            ormLiteDb =
                    OpenHelperManager.getHelper(this, OrmLiteDb::class.java)
        }
        return ormLiteDb!!
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        val mDialog= AppProgressDialog(this@SelectServicesActivity)
        mDialog.show()
        ItemManagement(this@SelectServicesActivity,this@SelectServicesActivity,mDialog).gettingItemList()
    }

}