package com.natrajinfotech.aboardlaundry.ui

import android.os.Bundle
import android.widget.ImageView
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.RetroFitHelper.OrderManagement
import com.natrajinfotech.aboardlaundry.model.GetOrderHistoryResponseModel
import com.natrajinfotech.aboardlaundry.utils.BaseActivity
import com.natrajinfotech.aboardlaundry.utils.NetworkHandler
import com.natrajinfotechindia.brainiton.utils.AppAlerts
import com.natrajinfotech.aboardlaundry.utils.AppProgressDialog
import kotlinx.android.synthetic.main.activity_track_order.*

class TrackOrderActivity : BaseActivity() {
    private var orderStatusArray= arrayOf("Order Placed","Out for Pickup","In Process","Laundry is Cleaned","Out for Delivery","Order Delivered","Order Cancelled")
    lateinit var orderStatusImageViewArray:Array<ImageView>
    private var orderStatusImageArray= arrayOf(R.drawable.ab_1_order_placed,R.drawable.ab_2_on_way,R.drawable.ab_3_in_process,R.drawable.ab_4_laundry_cleaned,R.drawable.ab_5_out_for_delivery)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_track_order)
showHomeBackOnToolbar(toolBar)
        this.title="Track Order"
        initViews()

    }

    private fun initViews() {
orderStatusImageViewArray= arrayOf(iv1OrderPlaced,iv2OnTheWay,iv3InProcess,iv4Cleaned,iv5Delivery)
        val orderData:GetOrderHistoryResponseModel.OrderHistoryData= intent.getSerializableExtra("orderData") as GetOrderHistoryResponseModel.OrderHistoryData
        var servicesData=""
        for (i in orderData.item_details!!.indices){
            if (servicesData.isEmpty()){
                servicesData= orderData.item_details!![i].services_name
            }else{
                servicesData=servicesData+"/"+ orderData.item_details!![i].services_name
            }

        }

        tvMyOrder.text=servicesData
        tvPlacedOn.text="Place On : "+orderData.order_date
        tvPrice.text="Price :"+orderData.total
        tvOrderId.text="Order Id :"+orderData.order_number
        tvStatus.text="Status :"+orderData.order_status
for (i in orderStatusImageArray.indices){
            if (orderData.status.equals(orderStatusArray[i])){
                orderStatusImageViewArray[i].setImageResource(R.drawable.icf_tick_activated)
                imgBigStatus.setImageResource(orderStatusImageArray[i])

            }
        }




    }

    override fun onBackPressed() {
        //super.onBackPressed()
        if (NetworkHandler(this@TrackOrderActivity).isNetworkAvailable()){
        val mDialog= AppProgressDialog(this@TrackOrderActivity)
        mDialog.show()
        OrderManagement(this@TrackOrderActivity,this@TrackOrderActivity,mDialog).getOrderHistory()
        }else{
            AppAlerts().showAlertMessage(this@TrackOrderActivity,"Info",resources.getString(R.string.networkError))

        }
    }
}
