package com.natrajinfotech.aboardlaundry.ui.fragment


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import com.google.gson.Gson
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.RetroFitHelper.OrderManagement
import com.natrajinfotech.aboardlaundry.model.GetUserAddressListResponceModel
import com.natrajinfotech.aboardlaundry.ui.ScheduleMainActivity
import com.natrajinfotech.aboardlaundry.utils.AppProgressDialog
import com.natrajinfotech.aboardlaundry.utils.AppSharedPreference
import com.natrajinfotech.aboardlaundry.utils.NetworkHandler
import com.natrajinfotechindia.brainiton.utils.AppAlerts
import kotlinx.android.synthetic.main.fragment_select_payment_method.*

class SelectPaymentMethodFragment : Fragment(), View.OnClickListener {
    private var mContext: Context? = null
    var paymentType:String=""
    var orderType=""
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        ScheduleMainActivity.backFragmentName="dateTime"
        return inflater.inflate(R.layout.fragment_select_payment_method, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mContext = this.activity!!
        initViews()

        activity!!.title = "Schedule Date/Time"
    }

    private fun initViews() {
        val ivSchPayment = activity!!.findViewById<ImageView>(R.id.ivSchPayment)
        val progressCalender1 = activity!!.findViewById<View>(R.id.progressCalender1)
        val progressCalender2 = activity!!.findViewById<View>(R.id.progressCalender2)

        progressCalender1.setBackgroundResource(R.color.colorAccent)
        progressCalender2.setBackgroundResource(R.color.colorAccent)
        ivSchPayment.setImageResource(R.drawable.icf_payment_activated)
        ivEdiTime.setOnClickListener(this@SelectPaymentMethodFragment)
        ivEditAddress.setOnClickListener(this@SelectPaymentMethodFragment)
btnConfirmOrder.setOnClickListener(this@SelectPaymentMethodFragment)
        setSelectedData()



    }

    private fun setSelectedData() {
        val mGson=Gson()
        val addressDataStr=AppSharedPreference(requireContext()).getString("address")
        val addressData=mGson.fromJson<GetUserAddressListResponceModel.AddressListData>(addressDataStr,GetUserAddressListResponceModel.AddressListData::class.java)
        //here we will get order type
        // if order type is quick then we will submit order from here & default payment type will be COD.
        orderType=AppSharedPreference(requireContext()).getString("orderType")
        if (orderType.equals("quick")){
            paymentType="COD"
            imgPayKnet.background=ContextCompat.getDrawable(requireContext(),R.drawable.circle_stroke_bg)
            imgCOD.background=ContextCompat.getDrawable(requireContext(),R.drawable.icf_tick_acccent)

        }else{
            rlKnet.setOnClickListener(this@SelectPaymentMethodFragment)
            rlCOD.setOnClickListener(this@SelectPaymentMethodFragment)


        }

        tvPayName.text=addressData.name
        val address=addressData.block_no+" ,"+addressData.floor_no+" ,"+addressData.building_no+" ,"+addressData.street_no+" ,"+addressData.area_name
                tvPayAddress.text=address
        tvPayMobile.text=addressData.phone_number
        val getvalue=AppSharedPreference(requireContext())
        tvPickupDate.text=getvalue.getString("pickupDate")+ " & "+getvalue.getString("pickupTime")
        tvDateDelivery.text=getvalue.getString("deliveryDate")+ " & "+getvalue.getString("deliveryTime")

    }


    private fun showFragment(frag: Fragment) {
        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.flSchedulePickup, frag)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.rlKnet -> {
                paymentType="KNET"
                imgPayKnet.background=ContextCompat.getDrawable(requireContext(),R.drawable.icf_tick_acccent)
                imgCOD.background=ContextCompat.getDrawable(requireContext(),R.drawable.circle_stroke_bg)

            }
            R.id.rlCOD -> {
                paymentType="COD"
                imgPayKnet.background=ContextCompat.getDrawable(requireContext(),R.drawable.circle_stroke_bg)
                imgCOD.background=ContextCompat.getDrawable(requireContext(),R.drawable.icf_tick_acccent)

            }
            R.id.ivEditAddress -> {
                showFragment(ChangeAddAddressFragment())

            }
            R.id.ivEdiTime -> {
                showFragment(ScheduleDateTimeFragment())

            }
            R.id.btnConfirmOrder -> {
if (paymentType.isEmpty()){
    Toast.makeText(requireContext(),"Please select payment type.",Toast.LENGTH_LONG).show()

}else{
    AppSharedPreference(requireContext()).saveString("paymentMode",paymentType)
    //here we will check order type
    //if order type will be quick then we will submit order from here otherwise we will go on next page.
    if (orderType.equals("quick")){
doConfirmOrder()

    }else {
        showFragment(ConfirmOrderFragment())
    }
}

            }

        }
    }private fun doConfirmOrder() {
        if (NetworkHandler(requireContext()).isNetworkAvailable()) {

                val mDialog = AppProgressDialog(requireContext())
                mDialog.show()
                val finalRequestData = HashMap<String, Any>()

                finalRequestData["total_price"] = "0"
                finalRequestData["total_qty"] = "0"
                val getvalue = AppSharedPreference(requireContext())
                finalRequestData["delivery_type"] = getvalue.getString("deliveryType")

                val pickupDate = getvalue.getString("pickupDate").split(" ")
                val pickTime = getvalue.getString("pickupTime").split("-")

                val deliveryDate = getvalue.getString("deliveryDate").split(" ")
                val deliveryTime = getvalue.getString("deliveryTime").split("-")
                finalRequestData["pickup_day"] = pickupDate[0].trim()
                finalRequestData["pickup_month"] = pickupDate[1].trim()
                finalRequestData["pickup_year"] = pickupDate[2].trim()
                finalRequestData["delivery_day"] = deliveryDate[0].trim()
                finalRequestData["delivery_month"] = deliveryDate[1].trim()
                finalRequestData["delivery_year"] = deliveryDate[2].trim()
                finalRequestData["pickup_from_time"] = pickTime[0].trim()
                finalRequestData["pickup_to_time"] = pickTime[1].trim()
                finalRequestData["delivery_from_time"] = deliveryTime[0].trim()
                finalRequestData["delivery_to_time"] = deliveryTime[1].trim()
                finalRequestData["address_id"] = getvalue.getInt("addressId").toString()
                finalRequestData["payment_mode"] = getvalue.getString("paymentMode")
                finalRequestData["notes"]=""
            finalRequestData["order_type"]="quickorder"
                OrderManagement(requireContext(), requireActivity(), mDialog).doConfirmOrder(finalRequestData)



        } else {
            AppAlerts().showAlertMessage(requireContext(), "Info", resources.getString(R.string.networkError))

        }
    }




}