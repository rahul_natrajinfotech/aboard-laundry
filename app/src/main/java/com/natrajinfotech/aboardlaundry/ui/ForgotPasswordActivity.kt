package com.natrajinfotech.aboardlaundry.ui

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.widget.Toast
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.RetroFitHelper.UserManagement
import com.natrajinfotech.aboardlaundry.utils.BaseActivity
import com.natrajinfotech.aboardlaundry.utils.NetworkHandler
import com.natrajinfotechindia.brainiton.utils.AppAlerts
import com.natrajinfotech.aboardlaundry.utils.AppProgressDialog
import kotlinx.android.synthetic.main.activity_forgot_password2.*

class ForgotPasswordActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password2)
        showHomeBackOnToolbar(toolBar)
        this.title="Forgot Password"
        inItView()
    }

    private fun inItView() {
btnSubmit.setOnClickListener {
    val emailStr=etEmailId.text.toString().trim()
if (!emailStr.isEmpty() || isValidInputEmail(emailStr)){
    sendForgotEmail(emailStr)

}else{
    Toast.makeText(this@ForgotPasswordActivity,resources.getString(R.string.emailError),Toast.LENGTH_LONG).show()

}

}
    }

    private fun sendForgotEmail(emailStr: String) {
        if (NetworkHandler(this@ForgotPasswordActivity).isNetworkAvailable()){
val mDialog= AppProgressDialog(this@ForgotPasswordActivity)
            mDialog.show()
            var requestData=HashMap<String,String>()
            requestData["email"]=emailStr
            UserManagement(this@ForgotPasswordActivity,this@ForgotPasswordActivity,mDialog,requestData).sendForgotPassword()

        }else{
            AppAlerts().showAlertMessage(this@ForgotPasswordActivity,"Info",resources.getString(R.string.networkError))

        }
    }

    //For Email Validation
    private fun isValidInputEmail(email: CharSequence): Boolean {
        return if (!TextUtils.isEmpty(email)) {
            Patterns.EMAIL_ADDRESS.matcher(email).matches()
        } else false
    }


    override fun onBackPressed() {
        //super.onBackPressed()
        startActivity(Intent(this@ForgotPasswordActivity,SignInActivity::class.java))
        finish()
    }
}
