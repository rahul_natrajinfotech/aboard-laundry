package com.natrajinfotech.aboardlaundry.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.RetroFitHelper.OrderManagement
import com.natrajinfotech.aboardlaundry.RetroFitHelper.UserAddressManagement
import com.natrajinfotech.aboardlaundry.model.GetUserDataResponseModel
import com.natrajinfotech.aboardlaundry.utils.BaseActivity
import com.natrajinfotech.aboardlaundry.utils.NetworkHandler
import com.natrajinfotechindia.brainiton.utils.AppAlerts
import com.natrajinfotech.aboardlaundry.utils.AppProgressDialog
import kotlinx.android.synthetic.main.activity_my_account_settings.*

class AccountSettingActivity:BaseActivity(), View.OnClickListener {
lateinit var userData:GetUserDataResponseModel.UserDetail
var isSettingExpand:Boolean=false
    var isPaymentExpand:Boolean=false
    private val INITIAL_POSITION = 0.0f
    private val ROTATED_POSITION = 90f
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_account_settings)
        showHomeBackOnToolbar(toolBar)
        this.title ="My Account"
initView()
    }

    private fun initView() {

        userData= intent.getSerializableExtra("userData") as GetUserDataResponseModel.UserDetail

        tvHello.text="Hello, "+userData.first_name+" "+userData.last_name

llASetting.setOnClickListener(this@AccountSettingActivity)
        llPayment.setOnClickListener(this@AccountSettingActivity)
tvProfileInformation.setOnClickListener(this@AccountSettingActivity)
        tvManageAddress.setOnClickListener(this@AccountSettingActivity)
    }

    override fun onClick(v: View?) {
when (v?.id) {
    R.id.llASetting -> {
if (isSettingExpand){
    isSettingExpand=false
    llSettingOptions.visibility=View.GONE
imgSNext.rotation=INITIAL_POSITION
}else{
    isSettingExpand=true
    llSettingOptions.visibility=View.VISIBLE
    imgSNext.rotation=ROTATED_POSITION
}

    }
    R.id.llPayment -> {
        if (isPaymentExpand){
isPaymentExpand=false
            llPaymentOptions.visibility=View.GONE
            imgPayNext.rotation=INITIAL_POSITION
        }else{
            isPaymentExpand=true
            llPaymentOptions.visibility=View.VISIBLE
            imgPayNext.rotation=ROTATED_POSITION
        }

    }
R.id.tvProfileInformation -> {
    startActivity(Intent(this@AccountSettingActivity,PersonalInformationActivity::class.java)
        .putExtra("userData",userData))
    finish()

}
    R.id.tvManageAddress -> {
        getAddressList()

    }
}
    }

    private fun getAddressList() {
        if (NetworkHandler(this@AccountSettingActivity).isNetworkAvailable()){
val mDialog= AppProgressDialog(this@AccountSettingActivity)
            mDialog.show()
            UserAddressManagement(this@AccountSettingActivity,this@AccountSettingActivity,mDialog).getAddressList(Intent(this@AccountSettingActivity,UserAddressListActivity::class.java))

        }else{
            AppAlerts().showAlertMessage(this@AccountSettingActivity,"Info",resources.getString(R.string.networkError))

        }
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        val mDialog= AppProgressDialog(this@AccountSettingActivity)
        mDialog.show()
        OrderManagement(this@AccountSettingActivity,this@AccountSettingActivity,mDialog).getLatestOrder()
    }

}