package com.natrajinfotech.aboardlaundry.ui


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Patterns
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.RetroFitHelper.UserManagement
import com.natrajinfotech.aboardlaundry.model.GetAreaListResponceModel
import com.natrajinfotech.aboardlaundry.utils.AppProgressDialog
import com.natrajinfotech.aboardlaundry.utils.NetworkHandler
import com.natrajinfotechindia.brainiton.utils.AppAlerts
import kotlinx.android.synthetic.main.activity_register.*

class SignupActivity:AppCompatActivity(), View.OnClickListener{
var name:String=""
    var email:String=""
    var mobile:String=""
    var password:String=""
    var confirmPassword:String=""
    var areaId :String=""
    lateinit var areaList:ArrayList<GetAreaListResponceModel.AreaListData>
    lateinit var mAdaptor: ArrayAdapter<String>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
inItView()
            }


    private fun inItView() {
        //here we will get list of area as arraylist fromn intent
        areaList= ArrayList()
        @Suppress("UNCHECKED_CAST")
        areaList= intent.getSerializableExtra("areaList") as ArrayList<GetAreaListResponceModel.AreaListData>
        val mAreaList = ArrayList<String>()
        for (i in areaList.indices) {
            mAreaList.add(areaList[i].area_name)
        }
        mAdaptor = ArrayAdapter(this@SignupActivity, android.R.layout.simple_list_item_1, mAreaList)
        etArea.setAdapter(mAdaptor)
        mAdaptor.notifyDataSetChanged()
        setClickOnEtArea()
        next.setOnClickListener(this@SignupActivity)
        tvLogin.setOnClickListener {
            startActivity(Intent(this@SignupActivity, SignInActivity::class.java))
        }

    }

    private fun setClickOnEtArea() {
        etArea.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                areaId = ""
                val userInput = p0.toString()

                val mAreaList = ArrayList<String>()
                for (i in areaList.indices) {
                    if (areaList[i].area_name.toLowerCase().contains(userInput.toLowerCase())) {
                        mAreaList.add(areaList[i].area_name)
                    }
                }


                mAdaptor =
                    ArrayAdapter(this@SignupActivity, android.R.layout.simple_list_item_1, mAreaList)
                etArea.setAdapter(mAdaptor)
                mAdaptor.notifyDataSetChanged()
            }


        })
        etArea.setOnItemClickListener(object : AdapterView.OnItemClickListener {
            override fun onItemClick(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                val inputManger: InputMethodManager =
                    this@SignupActivity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputManger.hideSoftInputFromWindow(etArea.windowToken, 0)

                val areaTv: TextView = p1 as TextView
                val areaName = areaTv.text.toString().trim()
                for (i in areaList.indices) {
                    if (areaList[i].area_name.equals(areaName)) {
                        areaId = areaList[i].id.toString()

                    }

                }
            }


        })


    }


    override fun onClick(v: View?) {
when (v?.id) {
    R.id.next -> {
        getData()
        if (isValidInput()){
if (NetworkHandler(this@SignupActivity).isNetworkAvailable()) {
    doRegistration()

}else{
    AppAlerts().showAlertMessage(this@SignupActivity,"Info",resources.getString(R.string.networkError))

}

        }
    }

}
    }

    private fun doRegistration() {
        val mDialog= AppProgressDialog(this@SignupActivity)
        mDialog.show()
        val requestData=HashMap<String,String>()
        requestData["first_name"]=name
        requestData["email"]=email
        requestData["phone"]=mobile
        requestData["city_id"]=areaId
        requestData["password"]=password
        requestData["confirm_password"]=confirmPassword
        UserManagement(this@SignupActivity,this@SignupActivity,mDialog,requestData).doRegistration()
    }

    private fun isValidInput(): Boolean {
if (name.isEmpty()&&email.isEmpty()&&mobile.isEmpty()&&password.isEmpty()&&areaId.isEmpty()){
    Toast.makeText(this@SignupActivity,"Please Enter all Valid Detail.",Toast.LENGTH_LONG).show()
    return false
}else if (name.isEmpty() || name.matches("[a-zA-z .?]*".toRegex()) == false) {
    etName.setError(resources.getString(R.string.nameError))
    return false
}else if (mobile.isEmpty() || mobile.length < 10 || mobile.matches("[0-9.?]*".toRegex()) == false){
    etPhone.setError(resources.getString(R.string.mobileError))
    return false
}else if (email.isEmpty() ||!isValidEmail(email)){
etEmail.setError(resources.getString(R.string.emailError))
    return false

}else if (password.isEmpty()){
etPassword.setError(resources.getString(R.string.passwordError))
    return false
}else if (confirmPassword.isEmpty() || !confirmPassword.equals(password)){
    Toast.makeText(this@SignupActivity,resources.getString(R.string.confirmPasswordError),Toast.LENGTH_LONG).show()
    return false

}else{
    return true

}
    }

    private fun getData() {
        name=etName.text.toString().trim()
        email=etEmail.text.toString().trim()
        mobile=etPhone.text.toString().trim()
        password=etPassword.text.toString().trim()
        confirmPassword=etConfirmPassword.text.toString().trim()

    }
    //For Email Validation
    private fun isValidEmail(email: CharSequence): Boolean {
        return if (!TextUtils.isEmpty(email)) {
            Patterns.EMAIL_ADDRESS.matcher(email).matches()
        } else false
    }
}