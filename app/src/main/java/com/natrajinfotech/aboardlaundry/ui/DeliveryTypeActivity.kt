package com.natrajinfotech.aboardlaundry.ui

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.text.style.SuperscriptSpan
import android.view.View
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.RetroFitHelper.OrderManagement
import com.natrajinfotech.aboardlaundry.utils.AppProgressDialog
import com.natrajinfotech.aboardlaundry.utils.AppSharedPreference
import com.natrajinfotech.aboardlaundry.utils.BaseActivity
import com.natrajinfotech.aboardlaundry.utils.NetworkHandler
import com.natrajinfotechindia.brainiton.utils.AppAlerts
import kotlinx.android.synthetic.main.activity_delivery_type.*





class DeliveryTypeActivity: BaseActivity(),View.OnClickListener{

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delivery_type)

        showHomeBackOnToolbar(toolBar)
        this.title="Delivery Type"
inItView()
         }

    private fun inItView() {
        btnSchedulePickup.visibility=View.GONE

        val cs = SpannableStringBuilder("*Extra charges will be applied")
        cs.setSpan(SuperscriptSpan(), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        cs.setSpan(RelativeSizeSpan(1.50f), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        val fcs = ForegroundColorSpan(Color.rgb(178,34,34))
        cs.setSpan(fcs, 0, 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        @Suppress("DEPRECATION")
        tvExpressConditionText.text=cs

        cardNormalDelivery.setOnClickListener(this@DeliveryTypeActivity)
        cardExpressDelivery.setOnClickListener(this@DeliveryTypeActivity)

    }

    override fun onClick(v: View?) {
when (v?.id) {
    R.id.cardExpressDelivery -> {
        getDeliveryCharges("express")

    }
    R.id.cardNormalDelivery -> {
        getDeliveryCharges("normal")

    }


}
    }

    private fun getDeliveryCharges(deliveryType: String) {
if (NetworkHandler(mContext).isNetworkAvailable()) {
    val mDialog = AppProgressDialog(this@DeliveryTypeActivity)
    mDialog.show()
    OrderManagement(this@DeliveryTypeActivity,this@DeliveryTypeActivity,mDialog).getDeliveryCharges(deliveryType)
}else{
    AppAlerts().showAlertMessage(mContext,"Info",resources.getString(R.string.networkError))

}
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        val orderType=AppSharedPreference(this@DeliveryTypeActivity).getString("orderType")
        if(orderType.equals("quick")){
            val mDialog= AppProgressDialog(this@DeliveryTypeActivity)
            mDialog.show()
            OrderManagement(this@DeliveryTypeActivity,this@DeliveryTypeActivity,mDialog).getLatestOrder()

        }else{
        startActivity(Intent(this@DeliveryTypeActivity,MyBasketActivity::class.java)
            .putExtra("from","home"))
        finish()
        }
    }
}