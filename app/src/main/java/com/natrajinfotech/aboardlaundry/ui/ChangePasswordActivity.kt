package com.natrajinfotech.aboardlaundry.ui

import android.content.Intent
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.widget.Toast
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.RetroFitHelper.UserManagement
import com.natrajinfotech.aboardlaundry.model.GetUserDataResponseModel
import com.natrajinfotech.aboardlaundry.utils.BaseActivity
import com.natrajinfotech.aboardlaundry.utils.NetworkHandler
import com.natrajinfotechindia.brainiton.utils.AppAlerts
import com.natrajinfotech.aboardlaundry.utils.AppProgressDialog
import kotlinx.android.synthetic.main.activity_change_password.*

class ChangePasswordActivity : BaseActivity(),View.OnClickListener {
    private var isShowPassword:Boolean=false
    private var oldPassword:String=""
    private var newPassword:String=""
    private var confirmPassword:String=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
showHomeBackOnToolbar(toolBar)
        this.title ="Change Password"
        initViews()

    }

    private fun initViews() {
        etOldPassword
        etNewPassword
        etConfirmPassword
        cbShowPassword.setOnClickListener(this@ChangePasswordActivity)

        btnSubmit.setOnClickListener(this@ChangePasswordActivity)
    }
    override fun onClick(p0: View?) {
when (p0?.id) {
    R.id.cbShowPassword -> {
        if (isShowPassword){
cbShowPassword.isChecked=false
                isShowPassword=false
etOldPassword.transformationMethod=PasswordTransformationMethod.getInstance()
etNewPassword.transformationMethod=PasswordTransformationMethod.getInstance()
etConfirmPassword.transformationMethod=PasswordTransformationMethod.getInstance()

        }else{
            cbShowPassword.isChecked=true
            isShowPassword=true
            etOldPassword.transformationMethod=HideReturnsTransformationMethod.getInstance()
            etNewPassword.transformationMethod=HideReturnsTransformationMethod.getInstance()
            etConfirmPassword.transformationMethod=HideReturnsTransformationMethod.getInstance()


        }

    }
R.id.btnSubmit -> {
    getData()
    if (isValidInput()){
updatePassword()

    }

}

}

    }

    private fun updatePassword() {
        if (NetworkHandler(this@ChangePasswordActivity).isNetworkAvailable()){
            val mDialog= AppProgressDialog(this@ChangePasswordActivity)
            mDialog.show()
            val requestData=HashMap<String,String>()
            requestData["type"]="password"
            requestData["new_password"]=newPassword
            requestData["old_password"]=oldPassword
UserManagement(this@ChangePasswordActivity,this@ChangePasswordActivity,mDialog,requestData).doUpdateProfileData()
        }else{
            AppAlerts().showAlertMessage(this@ChangePasswordActivity,"Info",resources.getString(R.string.networkError))

        }
    }

    private fun getData() {
        oldPassword=etOldPassword.text.toString().trim()
        newPassword=etNewPassword.text.toString().trim()
        confirmPassword=etConfirmPassword.text.toString().trim()
    }

    private fun isValidInput(): Boolean {
if (oldPassword.isEmpty() && newPassword.isEmpty() && confirmPassword.isEmpty()){
    Toast.makeText(this@ChangePasswordActivity,resources.getString(R.string.allFieldError),Toast.LENGTH_LONG).show()
    return false
}else if (oldPassword.isEmpty()){
    Toast.makeText(this@ChangePasswordActivity,"Please enter valid old password",Toast.LENGTH_LONG).show()
    return false

}else if (newPassword.isEmpty()){
    Toast.makeText(this@ChangePasswordActivity,"Please enter valid new password.",Toast.LENGTH_LONG).show()
    return false
}else if (confirmPassword.isEmpty()){
    Toast.makeText(this@ChangePasswordActivity,"Please enter valid confirm password.",Toast.LENGTH_LONG).show()
    return false
}else if (!newPassword.equals(confirmPassword)){
    Toast.makeText(this@ChangePasswordActivity,"Please enter confirm password as same new password.",Toast.LENGTH_LONG).show()
    return false
}else{
    return true

}
    }


    override fun onBackPressed() {
        //super.onBackPressed()
        val userData:GetUserDataResponseModel.UserDetail= intent.getSerializableExtra("userData") as GetUserDataResponseModel.UserDetail
        startActivity(Intent(this@ChangePasswordActivity,PersonalInformationActivity::class.java)
            .putExtra("userData",userData))
        finish()
    }
}
