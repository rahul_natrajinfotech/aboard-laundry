package com.natrajinfotech.aboardlaundry.ui

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.RetroFitHelper.Interface.OrderManagementInterface
import com.natrajinfotech.aboardlaundry.RetroFitHelper.OrderManagement
import com.natrajinfotech.aboardlaundry.adapter.RvOrderHistoryAdaptor
import com.natrajinfotech.aboardlaundry.model.GetOrderHistoryResponseModel
import com.natrajinfotech.aboardlaundry.utils.*
import com.natrajinfotechindia.brainiton.RetroFitHelper.ApiClient
import com.natrajinfotechindia.brainiton.utils.AppAlerts
import kotlinx.android.synthetic.main.activity_my_orders.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MyOrdersActivity : BaseActivity(),View.OnClickListener {

    private lateinit var orderList:ArrayList<GetOrderHistoryResponseModel.OrderHistoryData>
private lateinit var rvOrderHistoryLayoutManager:LinearLayoutManager
private lateinit var mHistoryAdaptor:RvOrderHistoryAdaptor
    var totalPages:Int=0
    var isLastPage:Boolean=false
    var isLoading:Boolean=false
    var currentPage:Int=1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_orders)
        showHomeBackOnToolbar(toolBar)
        this.title="My Order"
inItView()
            }

    private fun inItView() {
orderList= ArrayList()
        @Suppress("UNCHECKED_CAST")
        orderList= intent.getSerializableExtra("orderList") as ArrayList<GetOrderHistoryResponseModel.OrderHistoryData>
        totalPages=intent.getIntExtra("totalPages",0)
        rvOrderHistoryLayoutManager=LinearLayoutManager(this@MyOrdersActivity)
        rvOrderHistory.layoutManager=rvOrderHistoryLayoutManager
        mHistoryAdaptor=RvOrderHistoryAdaptor(this@MyOrdersActivity,this@MyOrdersActivity,orderList,this@MyOrdersActivity)
        rvOrderHistory.adapter=mHistoryAdaptor
        rvOrderHistory.addOnScrollListener(object: PaginationScrollListener(rvOrderHistoryLayoutManager){
            override fun totalPageCount(): Int {
return totalPages
            }

            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return  isLoading
            }

            override fun loadMoreItems() {
                currentPage = currentPage + 1
                isLoading=true
                Handler().postDelayed({ getNextPageOrderHistory() }, 1000)
            }


        } )


        btnHome.setOnClickListener(this@MyOrdersActivity)
    }

    override fun onBackPressed() {
            val mDialog= AppProgressDialog(this@MyOrdersActivity)
            mDialog.show()
            OrderManagement(this@MyOrdersActivity,this@MyOrdersActivity,mDialog).getLatestOrder()

    }


    override fun onClick(p0: View?) {
when (p0?.id) {
    R.id.btnHome -> {
        onBackPressed()

    }
    R.id.imgCancelOrder -> {
        val selectedPosition:Int= p0.tag as Int
        doCancelOrder(selectedPosition)


    }
    R.id.tvViewOrder -> {
        val selectedPosition:Int= p0.tag as Int
        startActivity(Intent(this@MyOrdersActivity,ViewOrderActivity::class.java)
            .putExtra("orderData",mHistoryAdaptor.orderList[selectedPosition]))
        finish()

    }
    R.id.imgTrackOrder -> {
        val selectedPosition:Int= p0.tag as Int
        if (mHistoryAdaptor.orderList[selectedPosition].status.equals("Order Cancelled")){
Toast.makeText(this@MyOrdersActivity,"You can not track cancel order.",Toast.LENGTH_LONG).show()

        }else{
        startActivity(Intent(this@MyOrdersActivity,TrackOrderActivity::class.java)
            .putExtra("orderData",mHistoryAdaptor.orderList[selectedPosition]))
        finish()
        }

    }

}
    }

    private fun doCancelOrder(selectedPosition: Int) {
        if (NetworkHandler(this@MyOrdersActivity).isNetworkAvailable()){
            val mDialog= AppProgressDialog(this@MyOrdersActivity)
            mDialog.show()
            OrderManagement(this@MyOrdersActivity,this@MyOrdersActivity,mDialog).getCancelReasonListing(mHistoryAdaptor.orderList[selectedPosition].id.toString())

        }else{
            AppAlerts().showAlertMessage(this@MyOrdersActivity,"Info",resources.getString(R.string.networkError))


        }
    }


    fun getNextPageOrderHistory(){
        val mProgressDialog= AppProgressDialog(this@MyOrdersActivity)
        mProgressDialog.show()
        val requestInterface= ApiClient.getClient().create(OrderManagementInterface::class.java)
        val token= AppSharedPreference(mContext).getString("apiToken")
        val getOrderCall=requestInterface!!.getAllOrderHistory(token,currentPage)
        getOrderCall.enqueue(object : Callback<GetOrderHistoryResponseModel> {
            override fun onFailure(call: Call<GetOrderHistoryResponseModel>, t: Throwable) {
                mProgressDialog.dialog.dismiss()
                AppAlerts().showAlertMessage(mContext,"Error", t.message!!)
            }

            override fun onResponse(
                call: Call<GetOrderHistoryResponseModel>,
                response: Response<GetOrderHistoryResponseModel>
            ) {
                mProgressDialog.dialog.dismiss()
                if (response.isSuccessful){
                    val responseCode= response.body()!!.ResponseCode
                    if (responseCode.equals("200")){
                        val mOrderList= response.body()!!.Data
for (i in mOrderList!!.indices){
    mHistoryAdaptor.orderList.add(mOrderList[i])
}
mHistoryAdaptor.notifyDataSetChanged()
                        isLoading=false
if (currentPage== response.body()!!.total_page){
isLastPage=true

}
                    }else if (responseCode.equals("400")){
                        Toast.makeText(mContext, response.body()!!.ResponseMessage,Toast.LENGTH_LONG).show()

                    }
                }else{
                    AppAlerts().showAlertMessage(mContext,"Error",response.message())

                }
            }


        })

    }}
