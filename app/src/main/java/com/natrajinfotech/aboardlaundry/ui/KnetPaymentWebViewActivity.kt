package com.natrajinfotech.aboardlaundry.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.RetroFitHelper.OrderManagement
import com.natrajinfotech.aboardlaundry.utils.AppProgressDialog
import com.natrajinfotech.aboardlaundry.utils.NetworkHandler
import com.natrajinfotechindia.brainiton.utils.AppAlerts
import kotlinx.android.synthetic.main.activity_web_view.*




private var currentUrl: String=""
class KnetPaymentWebViewActivity : AppCompatActivity() {
    lateinit var mDialog: AppProgressDialog
    private var webUrl = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)

        // showHomeBackOnActionbar()
        mDialog= AppProgressDialog(this@KnetPaymentWebViewActivity)


        webUrl =intent.getStringExtra("webUrl")


        initViews()
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initViews() {
mDialog.show()
        webView.settings.javaScriptEnabled = true
        val customWebViewClient = CustomWebViewClient()
        val customWebChromeClient = CustomWebChromeClient()
        webView.webViewClient = customWebViewClient
        webView.webChromeClient = customWebChromeClient
        webView.loadUrl(webUrl)
        webView.setOnTouchListener(object :View.OnTouchListener{
            override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {
                val hr = (p0 as WebView).hitTestResult
                Log.v("dip","Click Type :"+hr.extra)
Log.v("dip","click type int :"+hr.type)
                goBackToActivity(hr)
                return false
            }


        })
    }

    private fun goBackToActivity(hr: WebView.HitTestResult?) {
//the final url after payment status will be like that : https://www.metrolaundry.online/knet/result.php?paymentid=100201908868476406&result=CAPTURED&auth=000000&avr=N&ref=908810000034&tranid=201908831613814&postdate=0330&trackid=345638008&udf1=182&udf2=Test2&udf3=Test3&udf4=Test4&udf5=Test5&amt=400.0&authRespCode=91&
// here we will check which url is open on webview it is for payment status or not.
        if (currentUrl.contains("paymentid")){
//here we will check web touch event when user click on go back my order then event will return 7
            if (hr!!.type==7){
    //here we will split url for getting status of payment.
            val paymentStatusResult= currentUrl.split("&")
    val paymentStatus=paymentStatusResult[1].split("=")
    val kNetPaymentStatus=paymentStatus[1]
    if (kNetPaymentStatus.equals("CAPTURED")){
getOrderHistoryList()

    }else{
        Toast.makeText(this@KnetPaymentWebViewActivity,"Payment Faild. Please do order process again.",Toast.LENGTH_LONG).show()
        startActivity(
            Intent(this@KnetPaymentWebViewActivity,MyBasketActivity::class.java)
                .putExtra("from","home"))
        finish()

    }
            }

}
    }

    private fun getOrderHistoryList() {
        if (NetworkHandler(this@KnetPaymentWebViewActivity).isNetworkAvailable()){
            val mDialog= AppProgressDialog(this@KnetPaymentWebViewActivity)
            mDialog.show()
            OrderManagement(this@KnetPaymentWebViewActivity,this@KnetPaymentWebViewActivity,mDialog).getOrderHistory()

        }else{
            AppAlerts().showAlertMessage(this@KnetPaymentWebViewActivity,"Info",resources.getString(R.string.networkError))

        }
    }

    private inner class CustomWebViewClient : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            if (!NetworkHandler(this@KnetPaymentWebViewActivity).isNetworkAvailable()) {
                //showToast(mContext, "No Internet!")
            } else {
                Log.v("dip","current url :"+url)
                currentUrl=url
                view.loadUrl(url)
            }

            return true
        }

    }



    private inner class CustomWebChromeClient : WebChromeClient() {
  override fun onProgressChanged(View: WebView, progress: Int) {
Log.v("dip","progress :"+progress)
            if (progress == 100) {
                mDialog.dialog.dismiss()
            } else {
                //mDialog.show()
                //mDialog.dialog.dismiss()
            }
        }
    }

    override fun onBackPressed() {
        val res = webView.canGoBack()
        if (res && NetworkHandler(this@KnetPaymentWebViewActivity).isNetworkAvailable()) {
            webView.goBack()
        }else {
            mDialog.show()
            OrderManagement(this@KnetPaymentWebViewActivity,this@KnetPaymentWebViewActivity,mDialog).getLatestOrder()
        }
    }
}