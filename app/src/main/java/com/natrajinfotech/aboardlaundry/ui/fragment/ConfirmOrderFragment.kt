package com.natrajinfotech.aboardlaundry.ui.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.j256.ormlite.android.apptools.OpenHelperManager
import com.natrajinfotech.aboardlaundry.Database.OrmLiteDb
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.RetroFitHelper.OrderManagement
import com.natrajinfotech.aboardlaundry.adapter.RvConfirmOrderItem
import com.natrajinfotech.aboardlaundry.model.OrderBasketListModel
import com.natrajinfotech.aboardlaundry.ui.ScheduleMainActivity
import com.natrajinfotech.aboardlaundry.utils.AppProgressDialog
import com.natrajinfotech.aboardlaundry.utils.AppSharedPreference
import com.natrajinfotech.aboardlaundry.utils.BaseActivity.Companion.showToast
import com.natrajinfotech.aboardlaundry.utils.NetworkHandler
import com.natrajinfotechindia.brainiton.utils.AppAlerts
import kotlinx.android.synthetic.main.activity_confirm_order.*

/**
 * Created by Dipendra Sharma  on 22-01-2019.
 */
class ConfirmOrderFragment : Fragment(), View.OnClickListener {
    private var mContext: Context? = null
    private var userNotes = ""
    private lateinit var basketList: ArrayList<OrderBasketListModel>
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        ScheduleMainActivity.backFragmentName="payment"
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.activity_confirm_order, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mContext = this.activity!!
        tvCount = view.findViewById<TextView>(R.id.tvSubNumber)
        tvRate = view.findViewById<TextView>(R.id.tvPriceNumber)
        tvRatePayable = view.findViewById<TextView>(R.id.tvTotalAmount)
        initViews()

        activity!!.title = "Confirm Order"
    }

    private fun initViews() {
        val progressPayment1 = activity!!.findViewById<View>(R.id.progressPayment1)
        val progressPayment2 = activity!!.findViewById<View>(R.id.progressPayment2)
        val ivSchComplete = activity!!.findViewById<ImageView>(R.id.ivSchComplete)

        progressPayment1.setBackgroundResource(R.color.colorAccent)
        progressPayment2.setBackgroundResource(R.color.colorAccent)
        ivSchComplete.setImageResource(R.drawable.icf_tick_activated)
        inItSetView()

    }

    private fun inItSetView() {
        basketList = ArrayList()
        basketList = getBasketData()
        mAdaptor = RvConfirmOrderItem(requireContext(), requireActivity(), basketList)
        rvItemDetail.layoutManager = LinearLayoutManager(requireContext())
        rvItemDetail.adapter = mAdaptor
        val getvalue = AppSharedPreference(requireContext())
        val pickDateTime= getvalue.getString("pickupDate") + " & " + getvalue.getString("pickupTime")
        tvPickDateTime.text =pickDateTime
        val deliveryDateTime=getvalue.getString("deliveryDate") + " & " + getvalue.getString("deliveryTime")
        tvDeliveryDateTime.text =deliveryDateTime



        tvType.text = getvalue.getString("deliveryType")
if (getvalue.getString("deliveryType").equals("Normal")){
    imgDeliveryType.setImageResource(R.drawable.normal_delivery)
}else{
    imgDeliveryType.setImageResource(R.drawable.express_delivery)
}
        val deliveryTypeCharges="Kwt" + getvalue.getString("deliveryCharges")
        tvTypePrice.text =deliveryTypeCharges
        deliveryCharges = getvalue.getString("deliveryCharges")
        tvPaymentMode.text=getvalue.getString("paymentMode")
        updateBottumData()

        btnConfirm.setOnClickListener(this@ConfirmOrderFragment)

        tvNote.setOnClickListener { openCreateNoteDialog() }

    }


    //for getting data
    private fun getBasketData(): ArrayList<OrderBasketListModel> {
        val basketServiceData =
            gethelper().getBucketDao().queryBuilder().distinct().selectColumns("servicesName").query()

        val basketServiceIdData =
            gethelper().getBucketDao().queryBuilder().distinct().selectColumns("servicesId").query()
        val finalBasketList = ArrayList<OrderBasketListModel>()

        for (i in basketServiceData.indices) {
            Log.v("dip", "services id :" + basketServiceIdData[i].servicesId)
            val basketItemList = gethelper().getBucketDao().queryForEq("servicesId", basketServiceIdData[i].servicesId)
            finalBasketList.add(
                OrderBasketListModel(
                    basketItemList!![0].servicesId,
                    basketItemList[0].servicesName,
                    basketItemList[0].servicesImagePath,
                    basketItemList
                )
            )
            for (j in basketItemList.indices) {
                basketItemList[j].servicesRate.toFloat()
                basketItemList[j].servicesItemQuantity.toInt()
                //basketTotalAmount=basketTotalAmount+currentItemPrice*currentItemQuantity
                //basketTotalCount=basketTotalCount+currentItemQuantity
            }
        }
        return finalBasketList
    }


    //For Database
    private fun gethelper(): OrmLiteDb {

        var ormLiteDb: OrmLiteDb? = null
        if (ormLiteDb == null) {
            ormLiteDb =
                OpenHelperManager.getHelper(requireContext(), OrmLiteDb::class.java)
        }
        return ormLiteDb!!
    }


    companion object {
        var basketTotalAmount = 0f
        var basketTotalCount = 0
        var mAdaptor: RvConfirmOrderItem?=null
        lateinit var tvCount: TextView
        lateinit var tvRate: TextView
        lateinit var tvRatePayable: TextView
        lateinit var deliveryCharges: String
        fun updateBottumData() {
            basketTotalAmount = 0f
            basketTotalCount = 0
            for (i in mAdaptor!!.mServicesList.indices) {


                for (j in mAdaptor!!.mServicesList[i].itemData!!.indices) {
                    val currentItemPrice = mAdaptor!!.mServicesList[i].itemData!![j].servicesRate.toFloat()
                    val currentItemQuantity = mAdaptor!!.mServicesList[i].itemData!![j].servicesItemQuantity.toInt()
                    basketTotalAmount = basketTotalAmount + currentItemPrice * currentItemQuantity
                    basketTotalCount = basketTotalCount + currentItemQuantity
                }
            }
            tvCount.text = basketTotalCount.toString()
            val rate= "KWD" + basketTotalAmount.toString()
            tvRate.text =rate
            basketTotalAmount = basketTotalAmount + deliveryCharges.toFloat()
            val paybleRate="KWD" + basketTotalAmount.toString()
            tvRatePayable.text =paybleRate

        }

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnConfirm -> {
                doConfirmOrder()

            }

        }
    }

    private fun doConfirmOrder() {
        if (NetworkHandler(requireContext()).isNetworkAvailable()) {

            basketList = ArrayList()
            basketList = getBasketData()
            if (basketList.size != 0) {
                val mDialog = AppProgressDialog(requireContext())
                mDialog.show()
                val itemData = ArrayList<HashMap<String, String>>()
                for (i in basketList.indices) {
                    val itemDataList = basketList[i].itemData
                    val itemMap = HashMap<String, String>()
                    for (j in itemDataList!!.indices) {
                        itemMap["services_id"] = itemDataList[j].getServicesId()
                        itemMap["item_id"] = itemDataList[j].getItemId()
                        itemMap["item_name"] = itemDataList[j].getItemName()
                        itemMap["services_name"] = itemDataList[j].getServicesName()
                        itemMap["quantity"] = itemDataList[j].getServicesItemQuantity()
                        itemMap["rate"] = itemDataList[j].getServicesRate()
                        val subTotal =
                            itemDataList[j].getServicesRate().toFloat() * itemDataList[j].getServicesItemQuantity().toInt()
                        itemMap["sub_total"] = subTotal.toString()
                        itemData.add(itemMap)
                    }

                }
                val finalRequestData = HashMap<String, Any>()
                finalRequestData["item_details"] = itemData
                finalRequestData["total_price"] = basketTotalAmount.toString()
                finalRequestData["total_qty"] = basketTotalCount.toString()
                val getvalue = AppSharedPreference(requireContext())
                finalRequestData["delivery_type"] = getvalue.getString("deliveryType")
                val pickupDate = getvalue.getString("pickupDate").split(" ")
                val pickTime = getvalue.getString("pickupTime").split("-")

                val deliveryDate = getvalue.getString("deliveryDate").split(" ")
                val deliveryTime = getvalue.getString("deliveryTime").split("-")
                finalRequestData["pickup_day"] = pickupDate[0].trim()
                finalRequestData["pickup_month"] = pickupDate[1].trim()
                finalRequestData["pickup_year"] = pickupDate[2].trim()
                finalRequestData["delivery_day"] = deliveryDate[0].trim()
                finalRequestData["delivery_month"] = deliveryDate[1].trim()
                finalRequestData["delivery_year"] = deliveryDate[2].trim()
                finalRequestData["pickup_from_time"] = pickTime[0].trim()
                finalRequestData["pickup_to_time"] = pickTime[1].trim()
                finalRequestData["delivery_from_time"] = deliveryTime[0].trim()
                finalRequestData["delivery_to_time"] = deliveryTime[1].trim()
                finalRequestData["address_id"] = getvalue.getInt("addressId").toString()
                finalRequestData["payment_mode"] = getvalue.getString("paymentMode")
finalRequestData["notes"]=userNotes
                finalRequestData["order_type"]="normal"
                OrderManagement(requireContext(), requireActivity(), mDialog).doConfirmOrder(finalRequestData)


            } else {
                Toast.makeText(requireContext(), "There is no item in your basket", Toast.LENGTH_LONG).show()

            }

        } else {
            AppAlerts().showAlertMessage(requireContext(), "Info", resources.getString(R.string.networkError))

        }
    }


    private fun openCreateNoteDialog() {
        val dialogBuilder = AlertDialog.Builder(mContext!!)
        //dialogBuilder.tit
        dialogBuilder.setCancelable(false)

        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.dialog_add_note, null)
        dialogBuilder.setView(dialogView)
        val alertDialog = dialogBuilder.create()

        val btnDialogCreate = dialogView.findViewById<Button>(R.id.btnDialogCreate)
        val btnDialogCancel = dialogView.findViewById<Button>(R.id.btnDialogCancel)
        val etNote = dialogView.findViewById<EditText>(R.id.etAddNote)

        btnDialogCreate.setOnClickListener {
            val mNote = etNote.text.toString()
            when (mNote) {
                "" -> showToast(mContext!!, "Please add some note to create.")
                else -> {
                    userNotes = mNote
                    tvNote.text = "Note"
                    alertDialog.dismiss()
                }
            }
        }

        btnDialogCancel.setOnClickListener{ alertDialog.dismiss() }

        alertDialog.show()

    }

    override fun onResume() {
        super.onResume()
        ScheduleMainActivity.backFragmentName="payment"
    }

}