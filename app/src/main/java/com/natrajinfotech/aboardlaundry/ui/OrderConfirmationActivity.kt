package com.natrajinfotech.aboardlaundry.ui

import android.os.Bundle
import android.view.Menu
import android.view.View
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.RetroFitHelper.OrderManagement
import com.natrajinfotech.aboardlaundry.model.ConfirmOrderResponseModel
import com.natrajinfotech.aboardlaundry.utils.BaseActivity
import com.natrajinfotech.aboardlaundry.utils.NetworkHandler
import com.natrajinfotechindia.brainiton.utils.AppAlerts
import com.natrajinfotech.aboardlaundry.utils.AppProgressDialog
import kotlinx.android.synthetic.main.activity_done.*


class OrderConfirmationActivity: BaseActivity(), View.OnClickListener{
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_done)

        showHomeBackOnToolbar(toolBar)
        this.title="Done"
initView()


    }

    private fun initView() {
val orderData:ConfirmOrderResponseModel.OrderDetail= intent.getSerializableExtra("orderData") as ConfirmOrderResponseModel.OrderDetail
        tvId.text=orderData.order_id.toString()
        tvPickDate.text=orderData.pickup_date
        tvPickTime.text=orderData.pickup_time
        tvDeliveryDate.text=orderData.delivery_date
        tvDeliveryTime.text=orderData.delivery_time
        btnMyOrders.setOnClickListener(this@OrderConfirmationActivity)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.select_type,menu)
        return true
    }
    override fun onClick(v: View?) {
when (v?.id) {
    R.id.btnMyOrders -> {
        openOrderList()

    }

}
    }

    private fun openOrderList() {
        if (NetworkHandler(this@OrderConfirmationActivity).isNetworkAvailable()){
            val mDialog= AppProgressDialog(this@OrderConfirmationActivity)
            mDialog.show()
            OrderManagement(this@OrderConfirmationActivity,this@OrderConfirmationActivity,mDialog).getOrderHistory()

        }else{
            AppAlerts().showAlertMessage(this@OrderConfirmationActivity,"Info",resources.getString(R.string.networkError))

        }
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        val mDialog= AppProgressDialog(this@OrderConfirmationActivity)
        mDialog.show()
        OrderManagement(this@OrderConfirmationActivity,this@OrderConfirmationActivity,mDialog).getLatestOrder()
    }
}