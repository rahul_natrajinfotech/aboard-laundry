package com.natrajinfotech.aboardlaundry.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.View
import com.natrajinfotech.aboardlaundry.R
import kotlinx.android.synthetic.main.fragment_payment.*


class PaymentActivity:AppCompatActivity(),View.OnClickListener{
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_payment)
        setSupportActionBar(toolBar)
        val actionBar=supportActionBar
        actionBar!!.title="Payment"
        actionBar.setDisplayHomeAsUpEnabled(true)
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.select_type,menu)
        return true

    }
    override fun onClick(v: View?) {

    }
}