package com.natrajinfotech.aboardlaundry.ui.fragment

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.RetroFitHelper.UserAddressManagement
import com.natrajinfotech.aboardlaundry.adapter.RvSelectAddressAdaptor
import com.natrajinfotech.aboardlaundry.model.GetUserAddressListResponceModel
import com.natrajinfotech.aboardlaundry.ui.ScheduleMainActivity
import com.natrajinfotech.aboardlaundry.utils.AppProgressDialog
import com.natrajinfotech.aboardlaundry.utils.AppSharedPreference
import com.natrajinfotechindia.brainiton.utils.AppAlerts
import kotlinx.android.synthetic.main.fragment_change_add_address.*

class ChangeAddAddressFragment : Fragment(),View.OnClickListener {
    private var mContext : Context? = null
lateinit var mAdaptor:RvSelectAddressAdaptor
    var isDataLoaded=false
    var isOnCreate=false
    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        isDataLoaded=isVisibleToUser
        if (isDataLoaded &&isOnCreate){
            getAddressList()

        }
    }

    private fun getAddressList() {
        val mDialog= AppProgressDialog(requireContext())
        mDialog.show()
        val addressList=ArrayList<GetUserAddressListResponceModel.AddressListData>()
        mAdaptor= RvSelectAddressAdaptor(requireContext(),requireActivity(),addressList,this,"order")
        UserAddressManagement(requireContext(),requireActivity(),mDialog).getAddressList(rvSelectAddress,mAdaptor,tvAddNew)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        ScheduleMainActivity.backFragmentName="location"
        return inflater.inflate(R.layout.fragment_change_add_address, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mContext = this.activity!!
        initViews()
isOnCreate=true
        activity!!.title = "Select Address"
    }

    private fun initViews() {




        tvAddNew.setOnClickListener(this@ChangeAddAddressFragment)
        btnContinue.setOnClickListener(this@ChangeAddAddressFragment)
        rvSelectAddress

    }

    private fun showFragment(frag : Fragment) {
        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.flSchedulePickup, frag)
        //fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }
    override fun onClick(p0: View?) {
when
    (p0?.id) {
    R.id.tvAddNew -> {
        val fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
        val mDialog= AppProgressDialog(requireContext())
        mDialog.show()
        UserAddressManagement(requireContext(),requireActivity(),mDialog).getAreaList(fragmentTransaction)

    }
    R.id.btnContinue -> {
var isAddressSelected=false
        for (i in mAdaptor.addressList.indices){
            if (mAdaptor.addressList[i].isSelectedAddress){
                AppSharedPreference(requireContext()).saveBoolean("isAddressSelected",true)
                AppSharedPreference(requireContext()).saveInt("addressId",mAdaptor.addressList[i].id)
            isAddressSelected=true
            }

        }
        if (isAddressSelected) {
            showFragment(SelectLocationFragment(mAdaptor.addressList))
        }else{
            Toast.makeText(requireContext(),"Please Selecte atleast one address.",Toast.LENGTH_LONG).show()

        }
    }

    R.id.rdAddress -> {
        val selectedPosition:Int= p0.tag as Int

        for (i in mAdaptor.addressList.indices){
            val addressData=mAdaptor.addressList[i]
            if (selectedPosition==i) {
    val updateData: GetUserAddressListResponceModel.AddressListData
    if (addressData.floor_no == null) {

        updateData = GetUserAddressListResponceModel.AddressListData(
            addressData.id,
            addressData.name,
            addressData.building_no,
            "",
            addressData.block_no,
            addressData.street_no,
            addressData.area_name,
            addressData.phone_number,addressData.latitude,addressData.longitude,
            true
        )
    } else {
        updateData = GetUserAddressListResponceModel.AddressListData(
            addressData.id,
            addressData.name,
            addressData.building_no,
            addressData.floor_no,
            addressData.block_no,
            addressData.street_no,
            addressData.area_name,
            addressData.phone_number,addressData.latitude,addressData.longitude,
            true
        )
    }
    mAdaptor.addressList.set(i, updateData)
}else {
                val updateData: GetUserAddressListResponceModel.AddressListData
                if (addressData.floor_no == null) {

                    updateData = GetUserAddressListResponceModel.AddressListData(
                        addressData.id,
                        addressData.name,
                        addressData.building_no,
                        "",
                        addressData.block_no,
                        addressData.street_no,
                        addressData.area_name,
                        addressData.phone_number,addressData.latitude,addressData.longitude,
                        false
                    )
                } else {
                    updateData = GetUserAddressListResponceModel.AddressListData(
                        addressData.id,
                        addressData.name,
                        addressData.building_no,
                        addressData.floor_no,
                        addressData.block_no,
                        addressData.street_no,
                        addressData.area_name,
                        addressData.phone_number,addressData.latitude,addressData.longitude,
                        false
                    )
                }
                mAdaptor.addressList.set(i, updateData)
            }


            mAdaptor.notifyItemChanged(i)
        }

    }
    R.id.btnEditAddress -> {
val selectedPosition:Int= p0.tag as Int
        val addressData=mAdaptor.addressList[selectedPosition]
        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        val mDialog= AppProgressDialog(requireContext())
        mDialog.show()
        UserAddressManagement(requireContext(),requireActivity(),mDialog).getAreaList(fragmentTransaction,addressData)


    }
    R.id.btnDeleteAddress -> {
        val selectedPosition:Int= p0.tag as Int
        doDeleteAddress(selectedPosition)

    }

}
    }

    private fun doDeleteAddress(selectedPosition: Int) {
        AppAlerts().showAlertWithAction(requireContext(),"Confirm","Do you want to delete this address?","Yes","No",object :DialogInterface.OnClickListener{
            override fun onClick(p0: DialogInterface?, p1: Int) {
                val addressData=mAdaptor.addressList[selectedPosition]
                val mDialog= AppProgressDialog(requireContext())
                mDialog.show()
                UserAddressManagement(requireContext(),requireActivity(),mDialog).deleteUserAddress(addressData.id.toString(),mAdaptor,selectedPosition)
            }


        },true)}


    override fun onResume() {
        super.onResume()
        ScheduleMainActivity.backFragmentName="location"
        if (!isDataLoaded){
            getAddressList()

        }
    }

}