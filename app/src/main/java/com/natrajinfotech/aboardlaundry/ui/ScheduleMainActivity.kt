package com.natrajinfotech.aboardlaundry.ui

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.Menu
import android.view.View
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.RetroFitHelper.UserAddressManagement
import com.natrajinfotech.aboardlaundry.model.GetUserAddressListResponceModel
import com.natrajinfotech.aboardlaundry.ui.fragment.ChangeAddAddressFragment
import com.natrajinfotech.aboardlaundry.ui.fragment.ScheduleDateTimeFragment
import com.natrajinfotech.aboardlaundry.ui.fragment.SelectLocationFragment
import com.natrajinfotech.aboardlaundry.ui.fragment.SelectPaymentMethodFragment
import com.natrajinfotech.aboardlaundry.utils.AppSharedPreference
import com.natrajinfotech.aboardlaundry.utils.BaseActivity
import com.natrajinfotech.aboardlaundry.utils.AppProgressDialog
import kotlinx.android.synthetic.main.activity_schedule_main.*

class ScheduleMainActivity: BaseActivity(),View.OnClickListener{
    lateinit var addressList:ArrayList<GetUserAddressListResponceModel.AddressListData>
    companion object {
    var backFragmentName:String=""


}
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_schedule_main)

        showHomeBackOnToolbar(toolbar)
        this.title = "Schedule Pickup"
val isData=intent.getStringExtra("isData")
//here isdata value will be define for 0 = there is no addres save by user 1= there is save address & user selected address in past.
        if (isData.equals("1")) {

        val isaddressSelected=AppSharedPreference(this@ScheduleMainActivity).getBoolean("isAddressSelected")
            @Suppress("UNCHECKED_CAST")
            addressList= (intent.getSerializableExtra("addressList") as ArrayList<GetUserAddressListResponceModel.AddressListData>?)!!
            if (isaddressSelected){

                showFragment(SelectLocationFragment(addressList),"")

            }else{
                showFragment(ChangeAddAddressFragment(),"addressList")


            }
        }else if (isData.equals("0")){
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            val mDialog= AppProgressDialog(this@ScheduleMainActivity)
            mDialog.show()
            UserAddressManagement(this@ScheduleMainActivity,this@ScheduleMainActivity,mDialog).getAreaList(fragmentTransaction)

        }
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.select_type,menu)
        return true

    }
    override fun onClick(v: View?) {

    }

    private fun showFragment(frag : Fragment,lastFragment:String) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.flSchedulePickup, frag)
        fragmentTransaction.addToBackStack(lastFragment)
        fragmentTransaction.commit()
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        val count=supportFragmentManager.backStackEntryCount
Log.v("dip","count :"+count)
        if (count<=-1){
startActivity(Intent(this@ScheduleMainActivity,DeliveryTypeActivity::class.java))
    finish()

}else{
Log.v("dipLocation", backFragmentName)
if (backFragmentName.equals("location")){
    val isaddressSelected=AppSharedPreference(this@ScheduleMainActivity).getBoolean("isAddressSelected")
    if (isaddressSelected){
    showFragment(SelectLocationFragment(addressList),"")
    }else{
        startActivity(Intent(this@ScheduleMainActivity,DeliveryTypeActivity::class.java))
        finish()

    }

}else if (backFragmentName.equals("add")){
showFragment(ChangeAddAddressFragment(),"")

}else if (backFragmentName.equals("dateTime")){
    showFragment(ScheduleDateTimeFragment(),"")

}else if (backFragmentName.equals("payment")){
    showFragment(SelectPaymentMethodFragment(),"")

}else if (backFragmentName.equals("main")){
    startActivity(Intent(this@ScheduleMainActivity,DeliveryTypeActivity::class.java))
    finish()

} else{
            supportFragmentManager.popBackStackImmediate(backFragmentName,0)
}

}
    }


}