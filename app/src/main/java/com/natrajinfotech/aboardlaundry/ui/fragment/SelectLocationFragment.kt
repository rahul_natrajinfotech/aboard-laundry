package com.natrajinfotech.aboardlaundry.ui.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.Gson
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.model.GetUserAddressListResponceModel
import com.natrajinfotech.aboardlaundry.ui.ScheduleMainActivity
import com.natrajinfotech.aboardlaundry.utils.AppSharedPreference
import kotlinx.android.synthetic.main.fragment_select_location.*
import kotlinx.android.synthetic.main.fragment_select_location.view.*

@SuppressLint("ValidFragment")
class SelectLocationFragment(private var addressList: ArrayList<GetUserAddressListResponceModel.AddressListData>?) : Fragment() {
    private lateinit var addressData:GetUserAddressListResponceModel.AddressListData
    private var mContext : Context? = null
    lateinit var mGoogleMap:GoogleMap
    var lat:Double=0.0
    var lng:Double=0.0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val mView=inflater.inflate(R.layout.fragment_select_location, container, false)
        mView.fragmentMap.onCreate(savedInstanceState)
        ScheduleMainActivity.backFragmentName="main"
        return mView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mContext = this.activity!!
        activity!!.title = "Schedule Pickup"
        initViews()
    }

    private fun initViews() {

        btnChangeAddress.setOnClickListener { showFragment(ChangeAddAddressFragment(),"location") }
        btnDateTime.setOnClickListener {             saveAddress()
             }
        val addressId=AppSharedPreference(requireContext()).getInt("addressId")
        for (i in addressList!!.indices){

    if (addressList!![i].id==addressId){
        addressData= addressList!![i]
        tvUserName.text= addressList!![i].name
        tvUserPhone.text= addressList!![i].phone_number
        val address=addressData.block_no+" ,"+addressData.building_no+" ,"+addressData.street_no+" ,"+addressData.area_name
        tvUserAddress.text=address
        lat=addressData.latitude.toDouble()
        lng=addressData.longitude.toDouble()
        Log.v("dip","lat of selected address :"+lat)
        Log.v("dip","Lng  of selected address :"+lng)
    }

}
   setMap()
    }

    private fun saveAddress() {
        val mGson=Gson()
        val addressDataStr=mGson.toJson(addressData)
        AppSharedPreference(requireContext()).saveString("address",addressDataStr)
        showFragment(ScheduleDateTimeFragment(),"location")
    }

    private fun setMap() {
        try {
             Log.v("dip","gps lat :"+lat)
            fragmentMap.onResume()
            MapsInitializer.initialize(requireContext())
            fragmentMap.getMapAsync(object : OnMapReadyCallback {
                override fun onMapReady(p0: GoogleMap?) {
                    mGoogleMap= p0!!
                    MarkerOptions().position(LatLng(lat,lng))
                        .title("Your Location")
                        //.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.blue_pin))
                    //mGoogleMap.addMarker(marker)
                    val cameraPosition= CameraPosition.Builder().target(LatLng(lat,lng)).zoom(17F).build()
                    mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
                                    }


            })
        }catch (e:Exception){
            Log.v("dip","map error :"+e.message)

        }
    }


    override fun onResume() {
        super.onResume()
ScheduleMainActivity.backFragmentName="main"
        activity!!.findViewById<ImageView>(R.id.ivSchMap)
        val ivSchCalender = activity!!.findViewById<ImageView>(R.id.ivSchCalender)
        val progressMap1 = activity!!.findViewById<View>(R.id.progressMap1)
        val progressMap2 = activity!!.findViewById<View>(R.id.progressMap2)

        progressMap1.setBackgroundResource(R.color.colorProgressDeactivated)
        progressMap2.setBackgroundResource(R.color.colorProgressDeactivated)
        ivSchCalender.setImageResource(R.drawable.icf_calander_d_activated)
    }

    private fun showFragment(frag : Fragment,fragmentName:String) {
        val fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.flSchedulePickup, frag)
        fragmentTransaction.addToBackStack(fragmentName)
        fragmentTransaction.commit()
    }

}