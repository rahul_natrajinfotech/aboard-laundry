package com.natrajinfotech.aboardlaundry.ui

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.j256.ormlite.android.apptools.OpenHelperManager
import com.j256.ormlite.table.TableUtils
import com.natrajinfotech.aboardlaundry.Database.OrmLiteDb
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.RetroFitHelper.ItemManagement
import com.natrajinfotech.aboardlaundry.RetroFitHelper.OrderManagement
import com.natrajinfotech.aboardlaundry.RetroFitHelper.UserManagement
import com.natrajinfotech.aboardlaundry.adapter.HomePageSliderAdapter
import com.natrajinfotech.aboardlaundry.adapter.RvLatestOrderAdaptor
import com.natrajinfotech.aboardlaundry.model.GetLatestOrderResponseModel
import com.natrajinfotech.aboardlaundry.model.HomeSliderModel
import com.natrajinfotech.aboardlaundry.model.OrderBasketDbModel
import com.natrajinfotech.aboardlaundry.utils.AppProgressDialog
import com.natrajinfotech.aboardlaundry.utils.AppSharedPreference
import com.natrajinfotech.aboardlaundry.utils.NetworkHandler
import com.natrajinfotechindia.brainiton.utils.AppAlerts
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.app_bar_home.*
import kotlinx.android.synthetic.main.content_home.*
import java.util.*
import kotlin.collections.ArrayList

class HomeActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {
    private var colorName = ArrayList<String>()
    private var mContext: Context? = null

    private var count = 0

    private lateinit var latestOrderList: ArrayList<GetLatestOrderResponseModel.LatestOrderData>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setSupportActionBar(toolbar)

        mContext = this
        this.title = ""


        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        val headerView: View = nav_view.inflateHeaderView(R.layout.nav_header)
        val tvUserName: TextView = headerView.findViewById(R.id.tvNavigationName)
        val tvUserEmail: TextView = headerView.findViewById(R.id.tvNavigationEmail)
        val getValue = AppSharedPreference(this@HomeActivity)
        tvUserName.text = getValue.getString("userName")
        tvUserEmail.text = getValue.getString("userEmail")
        nav_view.setNavigationItemSelectedListener(this)



        initSlider()

        initViews()
    }

    private fun initViews() {
        tvOrderNow.setOnClickListener(this@HomeActivity)
        llService1.setOnClickListener(this@HomeActivity)
        llService2.setOnClickListener(this@HomeActivity)
        llService3.setOnClickListener(this@HomeActivity)
        llService4.setOnClickListener(this@HomeActivity)
        btnQuickOrder.setOnClickListener(this@HomeActivity)
        latestOrderList = ArrayList()
        @Suppress("UNCHECKED_CAST")
        latestOrderList = intent.getSerializableExtra("latestData") as ArrayList<GetLatestOrderResponseModel.LatestOrderData>
        rvLatestOrder.layoutManager = LinearLayoutManager(this@HomeActivity)
        rvLatestOrder.adapter = RvLatestOrderAdaptor(this@HomeActivity, this@HomeActivity, latestOrderList, this@HomeActivity)




        tvOrderHistory.setOnClickListener(this@HomeActivity)
    }

    private fun getItemList() {
        val mDialog = AppProgressDialog(this@HomeActivity)
        mDialog.show()
        ItemManagement(this@HomeActivity, this@HomeActivity, mDialog).gettingItemList()
    }

    private fun initSlider() {
        colorName.add("First")
        colorName.add("Second")
        colorName.add("Third")
        viewPager
        layoutDots
        indicator
        worm_dots_indicator

        viewPager.adapter = HomePageSliderAdapter(this, colorName, sliderModelList(), this@HomeActivity)
        //indicator.setupWithViewPager(viewPager, true)
        worm_dots_indicator.setViewPager(viewPager)

        val timer = Timer()
        timer.scheduleAtFixedRate(SliderTimer(), 4000, 6000)

    }

    private fun sliderModelList(): ArrayList<HomeSliderModel> {
        val sliderList: ArrayList<HomeSliderModel> = ArrayList()

        val slider1 = HomeSliderModel()
        val slider2 = HomeSliderModel()
        val slider3 = HomeSliderModel()

        slider1.sliderTitle = "Get Top Washing Facilities"
        slider1.sliderMsg = resources.getString(R.string.washingFacilityDescription)
        slider1.sliderAction = "Order Now"
        slider1.sliderImage = R.drawable.slider_machine

        slider2.sliderTitle = "Instant Delivery"
        slider2.sliderMsg = resources.getString(R.string.getOnTimeSliderDescription)
        slider2.sliderAction = "More Info"
        slider2.sliderImage = R.drawable.delivery_van

        slider3.sliderTitle = "Anywhere in Kuwait"
        slider3.sliderMsg = resources.getString(R.string.anyWhereInKuwait)
        slider3.sliderAction = "More Info"
        slider3.sliderImage = R.drawable.skyline

        sliderList.add(0, slider1)
        sliderList.add(1, slider2)
        sliderList.add(2, slider3)

        return sliderList

    }

    private inner class SliderTimer : TimerTask() {

        override fun run() {
            this@HomeActivity.runOnUiThread {
                if (viewPager.currentItem < colorName.size - 1) {
                    viewPager.currentItem = viewPager.currentItem + 1
                } else {
                    viewPager.currentItem = 0
                }
            }
        }
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.home, menu)

        val actionCart = menu.findItem(R.id.action_notification).actionView

        val txtViewCount = actionCart.findViewById(R.id.txtCount) as TextView
        updateHotCount(count++, txtViewCount)
        actionCart.setOnClickListener {
            //updateHotCount(count++, txtViewCount)
            //showFragment(NotificationFragment())
            getNotificationList()
        }
        //actionCart.setOnClickListener { menuCart() }

        return true
    }

    private fun getNotificationList() {
        val mDialog = AppProgressDialog(this@HomeActivity)
        mDialog.show()
        UserManagement(this@HomeActivity, this@HomeActivity, mDialog).getNotificationList()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_notification -> {
                getNotificationList()
            }
            R.id.action_complain -> {
                startActivity(Intent(this@HomeActivity, UserComplainActivity::class.java))
                finish()

            }

        }
        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_orders -> {
                getOrderHistoryList()
            }
            R.id.nav_basket -> {
                openBasketPage()

            }

            R.id.nav_setting -> {
                getUserData()

            }

            R.id.nav_about -> {
                val getValue = AppSharedPreference(this@HomeActivity)

                startActivity(
                    Intent(this@HomeActivity, KnetPaymentWebViewActivity::class.java)
                        .putExtra("webUrl", getValue.getString("aboutUs"))
                )
                finish()
            }
            R.id.nav_logout -> {
                doLogout()
            }
        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun openBasketPage() {
        val basketList: ArrayList<OrderBasketDbModel>? =
            gethelper().getBucketDao().queryForAll() as ArrayList<OrderBasketDbModel>?
        if (basketList.isNullOrEmpty()) {
            Toast.makeText(this@HomeActivity, "No item in your basket.", Toast.LENGTH_LONG).show()

        } else {
            startActivity(
                Intent(this@HomeActivity, MyBasketActivity::class.java)
                    .putExtra("from", "home")
            )
            finish()

        }
    }

    private fun getUserData() {
        val mDialog = AppProgressDialog(this@HomeActivity)
        mDialog.show()
        UserManagement(this@HomeActivity, this@HomeActivity, mDialog).getUserData()
    }

    private fun doLogout() {
        AppAlerts().showAlertWithAction(this@HomeActivity, "Info", "Do you want to logout ?", "Yes", "No",
            DialogInterface.OnClickListener { _, _ ->
                TableUtils.clearTable(gethelper().connectionSource, OrderBasketDbModel::class.java)
                AppSharedPreference(this@HomeActivity).DeleteSharedPreference()
                startActivity(Intent(this@HomeActivity, SignInActivity::class.java))
                finish()
            }, true
        )
    }


    //For Database
    private fun gethelper(): OrmLiteDb {

        var ormLiteDb: OrmLiteDb? = null
        if (ormLiteDb == null) {
            ormLiteDb =
                OpenHelperManager.getHelper(this@HomeActivity, OrmLiteDb::class.java)
        }
        return ormLiteDb!!
    }


    private fun updateHotCount(new_hot_number: Int, txtViewCount: TextView) {
        //BaseActivity.showToast(this@HomeActivity, "Menu Clicked")
        count = new_hot_number
        if (count < 0) return
        runOnUiThread {
            try {
                if (count == 0)
                    txtViewCount.visibility = View.GONE
                else {
                    txtViewCount.visibility = View.VISIBLE
                    txtViewCount.text = count.toString()
                    // supportInvalidateOptionsMenu();
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }


    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.btnQuickOrder -> {
                AppSharedPreference(this@HomeActivity).saveString("orderType", "quick")
                startActivity(Intent(this@HomeActivity, DeliveryTypeActivity::class.java))
                finish()

            }


            R.id.tvQuickOrder -> {
                AppSharedPreference(this@HomeActivity).saveString("orderType", "quick")
                startActivity(Intent(this@HomeActivity, DeliveryTypeActivity::class.java))
                finish()

            }
            R.id.tvOrderNow -> {
                getItemList()

            }
            R.id.llService1 -> {
                getItemList()
            }
            R.id.llService2 -> {
                getItemList()
            }
            R.id.llService3 -> {
                getItemList()
            }
            R.id.llService4 -> {
                getItemList()
            }
            R.id.tvOrderHistory -> {
                getOrderHistoryList()

            }
            R.id.tvSelectCloths -> {
                getItemList()

            }

            R.id.cvLatestMain -> {
                val selectedPosition: Int = p0.tag as Int
                val orderId = latestOrderList[selectedPosition].id.toString()
                getOrderDetail(orderId)
            }
        }
    }

    private fun getOrderDetail(orderId: String) {
        if (NetworkHandler(this@HomeActivity).isNetworkAvailable()) {
            val mDialog = AppProgressDialog(this@HomeActivity)
            mDialog.show()
            OrderManagement(this@HomeActivity, this@HomeActivity, mDialog).getOrderDetailById(orderId)

        } else {
            AppAlerts().showAlertMessage(this@HomeActivity, "Info", resources.getString(R.string.networkError))

        }
    }


    private fun getOrderHistoryList() {
        if (NetworkHandler(this@HomeActivity).isNetworkAvailable()) {
            val mDialog = AppProgressDialog(this@HomeActivity)
            mDialog.show()
            OrderManagement(this@HomeActivity, this@HomeActivity, mDialog).getOrderHistory()

        } else {
            AppAlerts().showAlertMessage(this@HomeActivity, "Info", resources.getString(R.string.networkError))

        }
    }

}
