package com.natrajinfotech.aboardlaundry.ui


import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.RetroFitHelper.UserAddressManagement
import com.natrajinfotech.aboardlaundry.RetroFitHelper.UserManagement
import com.natrajinfotech.aboardlaundry.adapter.RvSelectAddressAdaptor
import com.natrajinfotech.aboardlaundry.model.GetUserAddressListResponceModel
import com.natrajinfotech.aboardlaundry.utils.AppProgressDialog
import com.natrajinfotech.aboardlaundry.utils.BaseActivity
import com.natrajinfotech.aboardlaundry.utils.NetworkHandler
import com.natrajinfotechindia.brainiton.utils.AppAlerts
import kotlinx.android.synthetic.main.activity_user_address_list.*

class UserAddressListActivity : BaseActivity(),View.OnClickListener {
    lateinit var addressList:ArrayList<GetUserAddressListResponceModel.AddressListData>
    lateinit var mAdaptor: RvSelectAddressAdaptor
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_address_list)
        showHomeBackOnToolbar(toolBar)
        this.title="Address List"
        inItView()
    }

    private fun inItView() {
        addressList= ArrayList()
        var isData:String=intent.getStringExtra("isData")
        tvAddNew.setOnClickListener(this@UserAddressListActivity)
        if (isData.equals("1")) {
            @Suppress("UNCHECKED_CAST")
            addressList =intent.getSerializableExtra("addressList") as ArrayList<GetUserAddressListResponceModel.AddressListData>
            mAdaptor = RvSelectAddressAdaptor(
                this@UserAddressListActivity,
                this@UserAddressListActivity,
                addressList,
                this@UserAddressListActivity,
                "profile"
            )
            rvAddressList.layoutManager = LinearLayoutManager(this@UserAddressListActivity)
            rvAddressList.adapter = mAdaptor
        }else {

            AppAlerts().showAlertWithAction(
                this@UserAddressListActivity,
                "Info",
                "You have no save address.\n Do you want to add new address?",
                "Yes",
                "Not Now",
                object : DialogInterface.OnClickListener {
                    override fun onClick(p0: DialogInterface?, p1: Int) {
                        tvAddNew.performClick()
                    }


                },
                true
            )


        }  }

    override fun onClick(p0: View?) {
        when
            (p0?.id) {
            R.id.tvAddNew -> {

                val mDialog= AppProgressDialog(this@UserAddressListActivity)
                mDialog.show()
                UserAddressManagement(this@UserAddressListActivity,this@UserAddressListActivity,mDialog).getAreaList(
                    Intent(this@UserAddressListActivity,AddUpdateUserAddressActivity::class.java))

            }
            R.id.btnEditAddress -> {
                val selectedPosition:Int= p0.tag as Int
                val addressData=mAdaptor.addressList[selectedPosition]

                val mDialog= AppProgressDialog(this@UserAddressListActivity)
                mDialog.show()
                UserAddressManagement(this@UserAddressListActivity,this@UserAddressListActivity,mDialog).getAreaList(
                    Intent(this@UserAddressListActivity,AddUpdateUserAddressActivity::class.java),addressData)


            }
            R.id.btnDeleteAddress -> {
                val selectedPosition:Int= p0.tag as Int
                doDeleteAddress(selectedPosition)

            }

        }
    }

    private fun doDeleteAddress(selectedPosition: Int) {
        AppAlerts().showAlertWithAction(this@UserAddressListActivity,"Confirm","Do you want to delete this address?","Yes","No",object :
            DialogInterface.OnClickListener{
            override fun onClick(p0: DialogInterface?, p1: Int) {
                val addressData=mAdaptor.addressList[selectedPosition]
                val mDialog= AppProgressDialog(this@UserAddressListActivity)
                mDialog.show()
                UserAddressManagement(this@UserAddressListActivity,this@UserAddressListActivity,mDialog).deleteUserAddress(addressData.id.toString(),mAdaptor,selectedPosition)
            }


        },true)}




    private fun getUserData() {
        if (NetworkHandler(this@UserAddressListActivity).isNetworkAvailable()){
        val mDialog= AppProgressDialog(this@UserAddressListActivity)
        mDialog.show()
        UserManagement(this@UserAddressListActivity,this@UserAddressListActivity,mDialog).getUserData()
        }else{
            AppAlerts().showAlertMessage(this@UserAddressListActivity,"Info",resources.getString(R.string.networkError))

        }
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        getUserData()
    }

}
