package com.natrajinfotech.aboardlaundry.ui

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.j256.ormlite.android.apptools.OpenHelperManager
import com.natrajinfotech.aboardlaundry.Database.OrmLiteDb
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.RetroFitHelper.OrderManagement
import com.natrajinfotech.aboardlaundry.adapter.RvCategoryExpandebleAdaptor
import com.natrajinfotech.aboardlaundry.model.GetItemListResponseModel
import com.natrajinfotech.aboardlaundry.utils.AppProgressDialog
import com.natrajinfotech.aboardlaundry.utils.ExpandableRecyclerAdapter
import kotlinx.android.synthetic.main.activity_select_category.*


class SelectItemByCategoryActivity:AppCompatActivity() ,View.OnClickListener,TextWatcher{
    lateinit var mAdaptor:RvCategoryExpandebleAdaptor
    private lateinit var categoryList:ArrayList<GetItemListResponseModel.GroupListData>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_category)
        initViews()
    }

    private fun initViews() {
        /*val v = findViewById<LinearLayout>(android.support.v7.appcompat.R.id.search_plate)
        findViewById<ImageView>(android.support.v7.appcompat.R.id.search_mag_icon)
        val sText = findViewById<TextView>(android.support.v7.appcompat.R.id.search_src_text)
        @Suppress("DEPRECATION")
        v.setBackgroundDrawable(ContextCompat.getDrawable(this@SelectItemByCategoryActivity,R.drawable.search_view_background))
        sText.setTextColor(resources.getColor(R.color.colorWhite))
        @Suppress("DEPRECATION")
        sText.setHintTextColor(resources.getColor(R.color.colorWhite))


        */backArrow.setOnClickListener {
                onBackPressed()
            }
        rvCategoryExpandable.layoutManager=LinearLayoutManager(this@SelectItemByCategoryActivity)
        categoryList=ArrayList<GetItemListResponseModel.GroupListData>()
        @Suppress("UNCHECKED_CAST")
        categoryList= (intent.getSerializableExtra("itemList") as ArrayList<GetItemListResponseModel.GroupListData>?)!!
        mAdaptor=RvCategoryExpandebleAdaptor(this@SelectItemByCategoryActivity,this@SelectItemByCategoryActivity, categoryList)

        mAdaptor.setExpandCollapseListener(object :ExpandableRecyclerAdapter.ExpandCollapseListener{
            override fun onParentExpanded(parentPosition: Int) {

            }

            override fun onParentCollapsed(parentPosition: Int) {

            }


        })
        val itemDecorator = DividerItemDecoration(this@SelectItemByCategoryActivity, DividerItemDecoration.VERTICAL)
        itemDecorator.setDrawable(ContextCompat.getDrawable(this@SelectItemByCategoryActivity, R.drawable.category_row_devider)!!)
        rvCategoryExpandable.addItemDecoration(itemDecorator)
        rvCategoryExpandable.adapter=mAdaptor
        val basketItemList=gethelper().getBucketDao().queryForAll()
        if (basketItemList.size>0){
relativeBasket.visibility=View.VISIBLE
var basketTotalPrice=0f
            var basketItemQuantity=0
            for (i in basketItemList.indices){
                val currentItemPrice=basketItemList[i].servicesRate.toFloat()
                val currentItemQuantity=basketItemList[i].servicesItemQuantity.toInt()
                basketTotalPrice=basketTotalPrice+currentItemPrice*currentItemQuantity
                basketItemQuantity=basketItemQuantity+currentItemQuantity

            }
val basketCount=resources.getString(R.string.your_basket)+basketItemQuantity+")"
            tvBasketCount.text=basketCount
            val basketPrice="KWD"+basketTotalPrice.toString()
            tvPriceBasket.text=basketPrice
        }else {
            relativeBasket.visibility=View.GONE

        }
        relativeBasket.setOnClickListener(this@SelectItemByCategoryActivity)

        imgNext.setOnClickListener(this@SelectItemByCategoryActivity)
        //ivSearchView.setOnClickListener(this@SelectItemByCategoryActivity)
                //ivSearchView.setIconifiedByDefault(false)


        getSystemService(Context.SEARCH_SERVICE) as SearchManager
        backArrow.isFocusable=true
        backArrow.requestFocus()
        backArrow.isFocusable=true
etSearchView.clearFocus()
        val inputManger: InputMethodManager = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManger.hideSoftInputFromWindow(etSearchView.windowToken, 0)
        etSearchView.addTextChangedListener(this@SelectItemByCategoryActivity)


    }


    private fun filterMethod(newText: String) {


        val filteredModelList = searchFilter(categoryList, newText)
Log.v("dip","size :"+filteredModelList.size)


        //array_list_count = filteredModelList.size

        mAdaptor.setParentList(filteredModelList, false)

        mAdaptor.notifyParentDataSetChanged(true)
        mAdaptor.expandAllParents()


    }

    private fun searchFilter(models: List<GetItemListResponseModel.GroupListData>, userQuery: String): List<GetItemListResponseModel.GroupListData> {
        var query = userQuery
        query = query.toLowerCase()


        val filteredModelList = ArrayList<GetItemListResponseModel.GroupListData>()



        for (model in models) {

            val productList_datas = model.item_data
            val newList = ArrayList<GetItemListResponseModel.GroupItemListData>()
            for (productList_data in productList_datas!!) {
                if (productList_data.item_name.toLowerCase().contains(query)) {
                    Log.v("dip","data :"+productList_data.item_name)
                    newList.add(productList_data)
                }
            }

            if (newList.size > 0) {




Log.v("dip","category Name "+model.group_name   )
                val categoryItem =
                    GetItemListResponseModel.GroupListData(model.id,model.group_name, model.icons,model.icon_path,  newList)
                filteredModelList.add(categoryItem)



            }
            // else{
            //
            // home_search_empty_txt_view.setVisibility(View.VISIBLE);
            // Log.e("emptylist","No data found");
            // }
        }
        return filteredModelList
    }
    override fun onClick(v: View?) {
when (v?.id) {
    R.id.imgNext -> {
        startActivity(Intent(this@SelectItemByCategoryActivity,MyBasketActivity::class.java)
            .putExtra("from","item"))
        finish()

    }R.id.relativeBasket -> {
    startActivity(Intent(this@SelectItemByCategoryActivity,MyBasketActivity::class.java)
        .putExtra("from","item"))
    finish()


}
    R.id.etSearchView -> {
        //etSearchView.setIconifiedByDefault(false)

    }

}
    }    //For Database
    fun gethelper(): OrmLiteDb {

        var ormLiteDb: OrmLiteDb? = null
        if (ormLiteDb == null) {
            ormLiteDb =
                    OpenHelperManager.getHelper(this, OrmLiteDb::class.java)
        }
        return ormLiteDb!!
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        val mDialog= AppProgressDialog(this@SelectItemByCategoryActivity)
        mDialog.show()
        OrderManagement(this@SelectItemByCategoryActivity,this@SelectItemByCategoryActivity,mDialog).getLatestOrder()
    }

    override fun afterTextChanged(p0: Editable?) {

    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
val query:String= p0.toString()
        if (!query.isEmpty()){
        filterMethod(query)
        }else{
            mAdaptor.setParentList(categoryList, false)

            mAdaptor.notifyParentDataSetChanged(true)
            mAdaptor.collapseAllParents()

        }
    }


}