package com.natrajinfotech.aboardlaundry.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.RetroFitHelper.OrderManagement
import com.natrajinfotech.aboardlaundry.utils.AppSharedPreference
import com.natrajinfotech.aboardlaundry.utils.BaseActivity
import com.natrajinfotech.aboardlaundry.utils.AppProgressDialog
import kotlinx.android.synthetic.main.activity_user_complain.*

class UserComplainActivity : BaseActivity(),View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_complain)

        showHomeBackOnToolbar(toolBar)
        initViews()
    }

    private fun initViews() {
        val getValue=AppSharedPreference(this@UserComplainActivity)
        tvEmail.text=getValue.getString("companyEmail")
        tvTelPaid.text=getValue.getString("phoneNumber")
        tvTelTolFree.text=getValue.getString("tollFree")
tvEmail.setOnClickListener(this@UserComplainActivity)

    }
    override fun onClick(p0: View?) {
when (p0?.id) {
    R.id.tvEmail -> {
        sendEmail()
    }



}
    }
    private fun sendEmail() {
        val supportId= AppSharedPreference(this@UserComplainActivity).getString("companyEmail")
        startActivity(
            Intent.createChooser(
                Intent("android.intent.action.SENDTO", Uri.parse("mailto:"+supportId)),
                "Chooser Title"
            )
        )
    }


    override fun onBackPressed() {
        //super.onBackPressed()
        val mDialog= AppProgressDialog(this@UserComplainActivity)
        mDialog.show()
        OrderManagement(this@UserComplainActivity,this@UserComplainActivity,mDialog).getLatestOrder()
    }

}
