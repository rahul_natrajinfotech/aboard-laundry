package com.natrajinfotech.aboardlaundry.ui

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.Toast
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.RetroFitHelper.UserManagement
import com.natrajinfotech.aboardlaundry.model.GetUserDataResponseModel
import com.natrajinfotech.aboardlaundry.utils.AppProgressDialog
import com.natrajinfotech.aboardlaundry.utils.BaseActivity
import kotlinx.android.synthetic.main.activity_personal_information.*

class PersonalInformationActivity : BaseActivity(),View.OnClickListener {
var genderStr:String=""
    lateinit var userData:GetUserDataResponseModel.UserDetail
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_personal_information)

        showHomeBackOnToolbar(toolBar)
        this.title ="Personal Detail"
        initViews()

    }

    private fun initViews() {
try {


        userData= intent.getSerializableExtra("userData") as GetUserDataResponseModel.UserDetail
        tvEditName.setOnClickListener(this@PersonalInformationActivity)
        etFirstName.isClickable=false
if (!userData.first_name.isEmpty()) {
    etFirstName.setHint(userData.first_name)
}
            etLastName.setHint(userData.last_name)


    radioGroup.isClickable=false
    rbFemale.isClickable=false
    rbMale.isClickable=false
    btnSubmit.visibility=View.GONE

    btnSubmit.setOnClickListener(this@PersonalInformationActivity)
    tvEditEmail.setOnClickListener(this@PersonalInformationActivity)
    tvChangePass.setOnClickListener(this@PersonalInformationActivity)
    etEmailAddress.setHint(userData.email)
    etEmailAddress.isClickable=false
    tvEditMobile.setOnClickListener(this@PersonalInformationActivity)
    etMobile.setHint(userData.phone)
        if (!userData.gender.isEmpty()){
        genderStr=userData.gender
    }
        if (genderStr.equals("Male")){

            rbMale.isChecked=true
        }else{
            rbFemale.isChecked=true

        }

}catch (e:Exception){
    Log.v("dip","Error :"+e.message)

}
    }
    override fun onClick(p0: View?) {
when (p0?.id) {
    R.id.tvEditName -> {
        etFirstName.isClickable=true
etFirstName.isFocusable=true
        etFirstName.isFocusableInTouchMode=true
etFirstName.setText(userData.first_name)
        etLastName.isClickable=true
        etLastName.isFocusable=true
        etLastName.isFocusableInTouchMode=true
        etLastName.setText(userData.last_name)
        btnSubmit.visibility=View.VISIBLE
radioGroup.isClickable=true
        rbMale.isClickable=true
        rbFemale.isClickable=true
    }
    R.id.tvEditEmail -> {
        if (etEmailAddress.isClickable){
saveEmail()

        }else{
etEmailAddress.isClickable=true
            etEmailAddress.isFocusable=true
            etEmailAddress.isFocusableInTouchMode=true
            etEmailAddress.setText(userData.email)
            tvEditEmail.text="Save"
        }

    }
R.id.tvEditMobile -> {
    if (etMobile.isClickable){
        saveMobile()

    }else{
        etMobile.isClickable=true
        etMobile.isFocusable=true
        etMobile.isFocusableInTouchMode=true
        etMobile.setText(userData.phone)
        tvEditMobile.text="Save"

    }

}
R.id.btnSubmit -> {
            savePersonalData()

        }
    R.id.rbMale -> {
        genderStr="Male"
        rbMale.isChecked=true
        rbFemale.isChecked=false

    }
    R.id.rbFemale -> {
        genderStr="Female"
        rbMale.isChecked=false
        rbFemale.isChecked=true

    }
    R.id.tvChangePass -> {
        startActivity(Intent(this@PersonalInformationActivity,ChangePasswordActivity::class.java)
            .putExtra("userData",userData))
        finish()

    }
        }
}

    private fun savePersonalData() {
if (isValidPersonalDetail()){
    val firstName=etFirstName.text.toString().trim()
    val lastName=etLastName.text.toString().trim()
    val mDialog= AppProgressDialog(this@PersonalInformationActivity)
    mDialog.show()
    val requestData=HashMap<String,String>()
    requestData["type"] =""
    requestData["first_name"]=firstName
    requestData["last_name"]=lastName
    requestData["gender"]=genderStr
    UserManagement(this@PersonalInformationActivity,this@PersonalInformationActivity,mDialog,requestData).doUpdateProfileData()
    etFirstName.isClickable=false
    etFirstName.isFocusable=false
    etFirstName.isFocusableInTouchMode=false

    etFirstName.setText("")
    etFirstName.setHint(firstName)
    etLastName.isClickable=false
    etLastName.isFocusable=false
    etLastName.isFocusableInTouchMode=false
    etLastName.setText("")
    etLastName.setHint(lastName)

    radioGroup.isClickable=false
    rbMale.isClickable=false
    rbFemale.isClickable=false
    btnSubmit.visibility=View.GONE

}
    }

    private fun isValidPersonalDetail(): Boolean {
val firstName=etFirstName.text.toString().trim()
        val lastName=etLastName.text.toString().trim()

        if (firstName.isEmpty() && lastName.isEmpty() &&genderStr.isEmpty() ){
  Toast.makeText(this@PersonalInformationActivity,resources.getString(R.string.allFieldError),Toast.LENGTH_LONG).show()
    return false
}else if (firstName.isEmpty() || firstName.matches("[a-zA-z .?]*".toRegex()) == false) {
            Toast.makeText(this@PersonalInformationActivity,"Please enter valid first name.",Toast.LENGTH_LONG).show()
            return false

        }else if (lastName.isEmpty() || lastName.matches("[a-zA-z .?]*".toRegex()) == false){
            Toast.makeText(this@PersonalInformationActivity,"Please enter valid last name.",Toast.LENGTH_LONG).show()
            return false
        }else if (genderStr.isEmpty()){
            Toast.makeText(this@PersonalInformationActivity,"Please select gender.",Toast.LENGTH_LONG).show()
            return false

        }else{
            return true


    }
}

    private fun saveMobile() {
        val mobileStr = etMobile.text.toString().trim()
        if (mobileStr.isEmpty()) {
            Toast.makeText(
                this@PersonalInformationActivity,
                resources.getString(R.string.mobileError),
                Toast.LENGTH_LONG
            ).show()

        } else {
            val requestData = HashMap<String, String>()
            requestData["type"] = "phone"
            requestData["phone"] = mobileStr
            val mDialog = AppProgressDialog(this@PersonalInformationActivity)
            mDialog.show()
            UserManagement(
                this@PersonalInformationActivity,
                this@PersonalInformationActivity,
                mDialog,
                requestData
            ).doUpdateProfileData()
            etMobile.isClickable=false
            etMobile.isFocusable=false
            etMobile.isFocusableInTouchMode=false
etMobile.setText("")
            etMobile.setHint(mobileStr)
            tvEditMobile.text="Edit"
        }


    }    private fun saveEmail() {
    val emailStr=etEmailAddress.text.toString().trim()
        if (emailStr.isEmpty() ||!isValidInputEmail(emailStr)){
            Toast.makeText(this@PersonalInformationActivity,resources.getString(R.string.emailError),Toast.LENGTH_LONG).show()

        }else{
            val requestData=HashMap<String,String>()
requestData["type"]="email"
requestData["email"]=emailStr
            val mDialog= AppProgressDialog(this@PersonalInformationActivity)
            mDialog.show()
            UserManagement(this@PersonalInformationActivity,this@PersonalInformationActivity,mDialog,requestData).doUpdateProfileData()
            etEmailAddress.isClickable=false
            etEmailAddress.isFocusable=false
            etEmailAddress.isFocusableInTouchMode=false
etEmailAddress.setText("")
            etEmailAddress.setHint(emailStr)
            tvEditEmail.text="Edit"
        }
    }


    //For Email Validation
    private fun isValidInputEmail(email: CharSequence): Boolean {
        return if (!TextUtils.isEmpty(email)) {
            Patterns.EMAIL_ADDRESS.matcher(email).matches()
        } else false
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        val mDialog= AppProgressDialog(this@PersonalInformationActivity)
        mDialog.show()
UserManagement(this@PersonalInformationActivity,this@PersonalInformationActivity,mDialog).getUserData()
    }
}
