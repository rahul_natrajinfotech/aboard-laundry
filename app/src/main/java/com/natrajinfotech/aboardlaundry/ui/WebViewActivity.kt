package com.natrajinfotech.aboardlaundry.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.RetroFitHelper.OrderManagement
import com.natrajinfotech.aboardlaundry.utils.NetworkHandler
import com.natrajinfotech.aboardlaundry.utils.AppProgressDialog
import kotlinx.android.synthetic.main.activity_web_view.*


class WebViewActivity : AppCompatActivity() {
lateinit var mDialog: AppProgressDialog
    private var webUrl = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)

       // showHomeBackOnActionbar()
mDialog= AppProgressDialog(this@WebViewActivity)


            webUrl =intent.getStringExtra("webUrl")


        initViews()
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initViews() {

        webView.settings.javaScriptEnabled = true
        val customWebViewClient = CustomWebViewClient()
        val customWebChromeClient = CustomWebChromeClient()
        webView.webViewClient = customWebViewClient
        webView.webChromeClient = customWebChromeClient
        webView.loadUrl(webUrl)
    }

    private inner class CustomWebViewClient : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            if (!NetworkHandler(this@WebViewActivity).isNetworkAvailable()) {
                //showToast(mContext, "No Internet!")
            } else {
                view.loadUrl(url)
            }

            return true
        }

    }

    private inner class CustomWebChromeClient : WebChromeClient() {
        override fun onProgressChanged(View: WebView, progress: Int) {
            if (progress == 100) {
                mDialog.dialog.dismiss()
            } else {
                mDialog.show()
            }
        }
    }

    override fun onBackPressed() {
        val res = webView.canGoBack()
        if (res && NetworkHandler(this@WebViewActivity).isNetworkAvailable()) {
            webView.goBack()
        }else {
            mDialog.show()
            OrderManagement(this@WebViewActivity,this@WebViewActivity,mDialog).getLatestOrder()
        }
    }
}