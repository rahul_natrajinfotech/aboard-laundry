package com.natrajinfotech.aboardlaundry.ui

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.RetroFitHelper.OrderManagement
import com.natrajinfotech.aboardlaundry.adapter.RvCancelReasonAdaptor
import com.natrajinfotech.aboardlaundry.model.GetCancelReasonResponseModel
import com.natrajinfotech.aboardlaundry.utils.BaseActivity
import com.natrajinfotech.aboardlaundry.utils.NetworkHandler
import com.natrajinfotechindia.brainiton.utils.AppAlerts
import com.natrajinfotech.aboardlaundry.utils.AppProgressDialog
import kotlinx.android.synthetic.main.activity_cancel_order.*

class CancelOrderActivity : BaseActivity(),View.OnClickListener {
    lateinit var reasonList:ArrayList<GetCancelReasonResponseModel.CancelReasonData>
    lateinit var mAdaptor:RvCancelReasonAdaptor
    var cancelReason:String=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cancel_order)
showHomeBackOnToolbar(toolBar)
        this.title="Cancel Order"
        initViews()

    }

    private fun initViews() {
        reasonList= ArrayList()
        @Suppress("UNCHECKED_CAST")
        reasonList= intent.getSerializableExtra("reasonList") as ArrayList<GetCancelReasonResponseModel.CancelReasonData>
        mAdaptor= RvCancelReasonAdaptor(this@CancelOrderActivity,reasonList,this@CancelOrderActivity)

        rvReason.layoutManager=LinearLayoutManager(this@CancelOrderActivity)
        rvReason.adapter=mAdaptor
        etAnyComment
        btnSubmitReason.setOnClickListener(this@CancelOrderActivity)
        /*********************************************************
        * row_reason_list ==> rbReason tvReason
        *********************************************************/
    }
    override fun onClick(p0: View?) {
when (p0?.id) {
    R.id.rbReason -> {
        val selectedPosition:Int= p0.tag as Int
updateClick(selectedPosition)
        }
    R.id.btnSubmitReason -> {
        if (isValidInput()){
            doCancelOrder()

        }

    }

}
    }

    private fun isValidInput(): Boolean {
if (!isReasonSelected()){
    Toast.makeText(this@CancelOrderActivity,"Please select atleast one valid reason for cancellation.",Toast.LENGTH_LONG).show()
    return false

}else{
    return true

}
    }

    private fun isReasonSelected(): Boolean {
var isSelected=false
        for (i in mAdaptor.reasonList.indices){
    if (mAdaptor.reasonList[i].isCheck){
        cancelReason=mAdaptor.reasonList[i].reason
        isSelected=true
        break
    }
}
    return isSelected
    }

    private fun doCancelOrder() {
if (NetworkHandler(this@CancelOrderActivity).isNetworkAvailable()){
    val mDialog= AppProgressDialog(this@CancelOrderActivity)
    mDialog.show()
val requestData=HashMap<String,String>()
    requestData["order_id"]=intent.getStringExtra("orderId")
    requestData["cancel_notes"]=etAnyComment.text.toString().trim()
    requestData["cancel_reason"]=cancelReason
    OrderManagement(this@CancelOrderActivity,this@CancelOrderActivity,mDialog).doCancelOrder(requestData)
}else{
    AppAlerts().showAlertMessage(this@CancelOrderActivity,"Info",resources.getString(R.string.networkError))

}
    }

    private fun updateClick(selectedPosition: Int) {
        if (mAdaptor.reasonList[selectedPosition].isCheck){
            val updateModel=GetCancelReasonResponseModel.CancelReasonData()
            updateModel.id=mAdaptor.reasonList[selectedPosition].id
            updateModel.reason=mAdaptor.reasonList[selectedPosition].reason
            updateModel.isCheck=false
            mAdaptor.reasonList.set(selectedPosition,updateModel)
            mAdaptor.notifyItemChanged(selectedPosition)
        }else{
            for (i in mAdaptor.reasonList.indices){
                if (i==selectedPosition){
                    val updateModel=GetCancelReasonResponseModel.CancelReasonData()
                    updateModel.id=mAdaptor.reasonList[i].id
                    updateModel.reason=mAdaptor.reasonList[i].reason
                    updateModel.isCheck=true
                    mAdaptor.reasonList.set(i,updateModel)

                }else{
                    val updateModel=GetCancelReasonResponseModel.CancelReasonData()
                    updateModel.id=mAdaptor.reasonList[i].id
                    updateModel.reason=mAdaptor.reasonList[i].reason
                    updateModel.isCheck=false
                    mAdaptor.reasonList.set(i,updateModel)

                }

            }
            mAdaptor.notifyDataSetChanged()
        }


    }

    override fun onBackPressed() {
        //super.onBackPressed()
        getOrderList()
    }

    private fun getOrderList() {
        if (NetworkHandler(this@CancelOrderActivity).isNetworkAvailable()){
val mDialog= AppProgressDialog(this@CancelOrderActivity)
            mDialog.show()
            OrderManagement(this@CancelOrderActivity,this@CancelOrderActivity,mDialog).getOrderHistory()

        }else{
            AppAlerts().showAlertMessage(this@CancelOrderActivity,"Info",resources.getString(R.string.networkError))

        }


    }
}
