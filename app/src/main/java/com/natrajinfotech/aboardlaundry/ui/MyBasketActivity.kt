package com.natrajinfotech.aboardlaundry.ui

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.j256.ormlite.android.apptools.OpenHelperManager
import com.natrajinfotech.aboardlaundry.Database.OrmLiteDb
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.RetroFitHelper.ItemManagement
import com.natrajinfotech.aboardlaundry.RetroFitHelper.OrderManagement
import com.natrajinfotech.aboardlaundry.adapter.RvBasketAdaptor
import com.natrajinfotech.aboardlaundry.model.OrderBasketListModel
import com.natrajinfotech.aboardlaundry.utils.AppProgressDialog
import com.natrajinfotech.aboardlaundry.utils.BaseActivity
import kotlinx.android.synthetic.main.activity_mybasket_main.*
import java.util.*


class MyBasketActivity:BaseActivity(),View.OnClickListener {
private lateinit var basketList:ArrayList<OrderBasketListModel>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mybasket_main)
        inItActionBar()
        initView()
            }

    private fun initView() {
        tvCount=findViewById<TextView>(R.id.tvItemCount)
        tvRate=findViewById<TextView>(R.id.tvPrice)
        tvRatePayable=findViewById<TextView>(R.id.tvAmountPayable)
        basketList= ArrayList()
        basketList=getBasketData()
rvBasket.layoutManager=LinearLayoutManager(this@MyBasketActivity)
        mAdaptor=RvBasketAdaptor(this@MyBasketActivity,this@MyBasketActivity,basketList)
        rvBasket.adapter=mAdaptor
//tvItemCount.text=basketTotalCount.toString()
        //tvPrice.text="KWD"+basketTotalAmount.toString()
        //tvAmountPayable.text="KWD"+basketTotalAmount.toString()
        updateBottumData()
        btnDeliveryType.setOnClickListener(this@MyBasketActivity)
    }

    private fun getBasketData(): ArrayList<OrderBasketListModel> {
val basketServiceData=gethelper().getBucketDao().queryBuilder().distinct().selectColumns("servicesName").query()

val basketServiceIdData=gethelper().getBucketDao().queryBuilder().distinct().selectColumns("servicesId").query()
        val finalBasketList= ArrayList<OrderBasketListModel>()

        for (i in basketServiceData.indices){
        Log.v("dip","services id :"+basketServiceIdData[i].servicesId)
            val basketItemList=gethelper().getBucketDao().queryForEq("servicesId",basketServiceIdData[i].servicesId)
            finalBasketList.add(  OrderBasketListModel(basketItemList!![0].servicesId,basketItemList[0].servicesName,basketItemList[0].servicesImagePath,basketItemList))
for (j in basketItemList.indices){
    basketItemList[j].servicesRate.toFloat()
    basketItemList[j].servicesItemQuantity.toInt()
            //basketTotalAmount=basketTotalAmount+currentItemPrice*currentItemQuantity
            //basketTotalCount=basketTotalCount+currentItemQuantity
    }
    }
    return finalBasketList
    }

    private fun inItActionBar() {
        showHomeBackOnToolbar(toolBar)
        this.title = "My Basket"



    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.select_type,menu)
        return true

    }
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnDeliveryType -> {
val basketCount=tvItemCount.text.toString().trim().toInt()
                if (basketCount>0){
                    startActivity(Intent(this@MyBasketActivity,DeliveryTypeActivity::class.java))
                    finish()

                }else{
                    Toast.makeText(this@MyBasketActivity,"No item in your basket. Please add item into basket first",Toast.LENGTH_LONG).show()
                    onBackPressed()

                }

            }

        }


    }//For Database
    private fun gethelper(): OrmLiteDb {

        var ormLiteDb: OrmLiteDb? = null
        if (ormLiteDb == null) {
            ormLiteDb =
                    OpenHelperManager.getHelper(this, OrmLiteDb::class.java)
        }
        return ormLiteDb!!
    }


    companion object {
        private var basketTotalAmount=0f
        private var basketTotalCount=0
        lateinit var mAdaptor:RvBasketAdaptor
        lateinit var tvCount:TextView
        lateinit var tvRate:TextView
        lateinit var tvRatePayable:TextView
        fun updateBottumData(){
            basketTotalAmount=0f
            basketTotalCount=0
            for (i in mAdaptor.mServicesList.indices){



                for (j in mAdaptor.mServicesList[i].itemData!!.indices){
                    var currentItemPrice= mAdaptor.mServicesList[i].itemData!![j].servicesRate.toFloat()
                    var currentItemQuantity= mAdaptor.mServicesList[i].itemData!![j].servicesItemQuantity.toInt()
                    basketTotalAmount=basketTotalAmount+currentItemPrice*currentItemQuantity
                    basketTotalCount=basketTotalCount+currentItemQuantity
                }
            }
            tvCount.text=basketTotalCount.toString()
            tvRate.text="KWD"+basketTotalAmount.toString()
            tvRatePayable.text="KWD"+basketTotalAmount.toString()

        }

    }

    override fun onBackPressed() {
        //super.onBackPressed()
val from=intent.getStringExtra("from")

        val mDialog= AppProgressDialog(this@MyBasketActivity)
        mDialog.show()
        if (from.equals("item")) {
            ItemManagement(this@MyBasketActivity, this@MyBasketActivity, mDialog).gettingItemList()
        }else if (from.equals("home")){
            OrderManagement(this@MyBasketActivity,this@MyBasketActivity,mDialog).getLatestOrder()

        }
    }

}