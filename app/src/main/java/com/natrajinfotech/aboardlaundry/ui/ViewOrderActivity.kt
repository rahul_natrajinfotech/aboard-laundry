package com.natrajinfotech.aboardlaundry.ui

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.RetroFitHelper.OrderManagement
import com.natrajinfotech.aboardlaundry.adapter.RvConfirmOrderItem
import com.natrajinfotech.aboardlaundry.model.GetOrderHistoryResponseModel
import com.natrajinfotech.aboardlaundry.model.OrderBasketDbModel
import com.natrajinfotech.aboardlaundry.model.OrderBasketListModel
import com.natrajinfotech.aboardlaundry.utils.BaseActivity
import com.natrajinfotech.aboardlaundry.utils.AppProgressDialog
import kotlinx.android.synthetic.main.activity_view_order.*


class ViewOrderActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_order)
        showHomeBackOnToolbar(toolBar)
        this.title = "View Order"

        initViews()

    }

    private fun initViews() {
        val orderData: GetOrderHistoryResponseModel.OrderHistoryData =
            intent.getSerializableExtra("orderData") as GetOrderHistoryResponseModel.OrderHistoryData
        var servicesData = ""
        for (i in orderData.item_details!!.indices) {
            if (servicesData.isEmpty()) {
                servicesData = orderData.item_details!![i].services_name
            } else {
                servicesData = servicesData + "/" + orderData.item_details!![i].services_name
            }

        }

        tvMyOrder.text = servicesData
        tvPlacedOn.text = "Place On : " + orderData.order_date
        tvPrice.text = "Price :" + orderData.total
        tvOrderId.text = "Order Id :" + orderData.order_number
        tvStatus.text = "Status :" + orderData.order_status
        val itemList: ArrayList<OrderBasketListModel>? = getItemList(orderData.item_details!!)
        rvOrderItem.layoutManager = LinearLayoutManager(this@ViewOrderActivity)
        rvOrderItem.adapter = RvConfirmOrderItem(this@ViewOrderActivity, this@ViewOrderActivity, itemList!!)
        tvNote.text = orderData.notes
        var pickupDateTime =
            orderData.pickup_day + orderData.pickup_month + orderData.pickup_year + " " + orderData.pickup_from_time + " " + orderData.pickup_to_time
        val deliveryDateTime =
            orderData.delivery_day + orderData.delivery_month + orderData.delivery_year + " " + orderData.delivery_from_time + "-" + orderData.delivery_to_time
        tvPickupDateNTime.text = pickupDateTime
        tvDeliveryDateNTime.text = deliveryDateTime
        tvItems.text = orderData.total_qty
        tvPriceCount.text = "KWD${orderData.sub_total}"
        tvPaidVia.text=orderData.payment_mode
    }

    private fun getItemList(item_details: ArrayList<GetOrderHistoryResponseModel.OrderItemDetail>): ArrayList<OrderBasketListModel>? {

        val serviceId = java.util.ArrayList<String>()
        for (j in item_details.indices) {
            if (!serviceId.contains(item_details[j].services_id.toString())) {
                serviceId.add(item_details[j].services_id.toString())

            }

        }
        val finalItemList = ArrayList<OrderBasketListModel>()
        for (i in serviceId.indices) {
            val itemArrayList = ArrayList<OrderBasketDbModel>()
            for (k in item_details.indices) {
                if (serviceId[i].equals(item_details[k].services_id.toString())) {
                    val addModel = OrderBasketDbModel(
                        item_details[k].item_id.toString(),
                        item_details[k].item_name,
                        "",
                        item_details[k].services_id.toString(),
                        item_details[k].services_name,
                        item_details[k].rate.toString(),
                        item_details[k].quantity.toString(),
                        ""
                    )
                    itemArrayList.add(addModel)

                }
            }
            val addItm = OrderBasketListModel(
                itemArrayList[0].servicesId,
                itemArrayList[0].servicesName,
                itemArrayList[0].servicesImagePath,
                itemArrayList
            )
            finalItemList.add(addItm)

        }

        return finalItemList
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        val mDialog = AppProgressDialog(this@ViewOrderActivity)
        mDialog.show()
        OrderManagement(this@ViewOrderActivity, this@ViewOrderActivity, mDialog).getOrderHistory()
    }
}
