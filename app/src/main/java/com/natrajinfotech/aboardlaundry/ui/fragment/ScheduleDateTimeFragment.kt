package com.natrajinfotech.aboardlaundry.ui.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.adapter.RvDeliveryDateAdaptor
import com.natrajinfotech.aboardlaundry.adapter.RvDeliveryTimeSlotAdaptor
import com.natrajinfotech.aboardlaundry.adapter.RvPickupDateAdaptor
import com.natrajinfotech.aboardlaundry.adapter.RvPickupTimeAdaptor
import com.natrajinfotech.aboardlaundry.model.CalenderDataModel
import com.natrajinfotech.aboardlaundry.ui.ScheduleMainActivity
import com.natrajinfotech.aboardlaundry.utils.AppSharedPreference
import com.natrajinfotech.aboardlaundry.utils.cal.FindDate


import kotlinx.android.synthetic.main.fragment_schedule_date_time.*
import java.text.SimpleDateFormat
import java.util.*

 class ScheduleDateTimeFragment : Fragment(),View.OnClickListener {
     private var mContext : Context? = null
var pickupCurrentYear:Int=0
    var pickupCurrentMonth:Int=0
    var deliveryCurrentYear:Int=0
     var deliveryCurrentMonth:Int=0
     var monthsArray= arrayOf("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")
     lateinit var currentDate:Date
     var todayMonth:Int=0
     var todayYear:Int=0
     lateinit var mPickupDateAdaptor:RvPickupDateAdaptor
     lateinit var pickupDateLayoutManager:LinearLayoutManager
    lateinit var mDeliveryDateAdaptor: RvDeliveryDateAdaptor
     lateinit var deliveryDateLayoutManager:LinearLayoutManager
     var timeSlot= arrayOf("08:00 AM - 10:00 AM","10:00 AM - 12:00 PM","12:00 PM - 02:00 PM","02:00 PM - 04:00PM","04:00 PM - 06:00 PM","06:00 PM - 08:00 PM")
     lateinit var mPickupTimeAdaptor: RvPickupTimeAdaptor
     lateinit var mDeliveryTimeAdaptor: RvDeliveryTimeSlotAdaptor
     var selectedPickupDate:String=""
     var selectedDeliveryDate:String=""
     var selectedPickupTime:String=""
     var selectedDeliveryTime:String=""

     override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        ScheduleMainActivity.backFragmentName="location"
         return inflater.inflate(R.layout.fragment_schedule_date_time, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mContext = this.activity!!
        ScheduleMainActivity.backFragmentName="location"
        initViews()

        activity!!.title = "Schedule Date/Time"
    }

    private fun initViews() {
        val ivSchCalender = activity!!.findViewById<ImageView>(R.id.ivSchCalender)
        val progressMap1 = activity!!.findViewById<View>(R.id.progressMap1)
        val progressMap2 = activity!!.findViewById<View>(R.id.progressMap2)

        progressMap1.setBackgroundResource(R.color.colorAccent)
        progressMap2.setBackgroundResource(R.color.colorAccent)
        ivSchCalender.setImageResource(R.drawable.icf_calander_activated)

       /* tvAddNew.setOnClickListener { showFragment(AddNewAddressFragment()) }
        btnContinue
        rvSelectAddress*/
        //tvAddNew.setOnClickListener { showFragment(AddNewAddressFragment()) }
        btnContinue.setOnClickListener(this@ScheduleDateTimeFragment)


        val calendar     = Calendar.getInstance()
        val dateFormat = SimpleDateFormat("MM/yyyy", Locale.ENGLISH)
        val currentDateFormat = SimpleDateFormat("d MMM yyyy", Locale.ENGLISH)
        val currentDateStr=currentDateFormat.format(calendar.time)
        currentDate= currentDateFormat.parse(currentDateStr)
        val currentMonthYear=dateFormat.format(calendar.time)
val dateDetail=currentMonthYear.split("/")
todayMonth=dateDetail[0].toInt()
        todayYear=dateDetail[1].toInt()
        pickupCurrentMonth=dateDetail[0].toInt()
        pickupCurrentYear=dateDetail[1].toInt()
        deliveryCurrentMonth=dateDetail[0].toInt()
        deliveryCurrentYear=dateDetail[1].toInt()
        setPickupCalenderData()
setDeliveryCalenderData()
        imgNextMonth.setOnClickListener(this@ScheduleDateTimeFragment)

        imgPreviousMonth.setOnClickListener(this@ScheduleDateTimeFragment)
        rvTimePickup.layoutManager=LinearLayoutManager(requireContext(),LinearLayoutManager.HORIZONTAL,false)
        mPickupTimeAdaptor= RvPickupTimeAdaptor(requireContext(),requireActivity(),timeSlot,this@ScheduleDateTimeFragment)
        rvTimePickup.adapter=mPickupTimeAdaptor

        imgNextMonthD.setOnClickListener(this@ScheduleDateTimeFragment)

        imgPreviousMonthD.setOnClickListener(this@ScheduleDateTimeFragment)

        rvTimeDelivery.layoutManager=LinearLayoutManager(requireContext(),LinearLayoutManager.HORIZONTAL,false)
        mDeliveryTimeAdaptor= RvDeliveryTimeSlotAdaptor(requireContext(),requireActivity(),timeSlot,this@ScheduleDateTimeFragment)
        rvTimeDelivery.adapter=mDeliveryTimeAdaptor

        /***************************************************************
        * row_grid_select_time(tvTimeSlot),
        * bg_cal_time_gray && bg_cal_time_gray_orange_stroke
        *
        * row_grid_select_date(tvDate, tvDate)
        * bg_cal_time_gray && bg_cal_time_gray_orange_stroke
        *****************************************************************/

    }

     @SuppressLint("SetTextI18n")
     private fun setDeliveryCalenderData() {
         tvMonthD.text=monthsArray[deliveryCurrentMonth-1]+" "+deliveryCurrentYear
         val dateList=FindDate.getDate(monthsArray[deliveryCurrentMonth-1],deliveryCurrentYear,currentDate)
         deliveryDateLayoutManager=LinearLayoutManager(requireContext(),LinearLayoutManager.HORIZONTAL,false)
         rvDayDelivery.layoutManager=deliveryDateLayoutManager
         mDeliveryDateAdaptor= RvDeliveryDateAdaptor(requireContext(),requireActivity(),dateList,this@ScheduleDateTimeFragment)
         rvDayDelivery.adapter=mDeliveryDateAdaptor
         for (i in mDeliveryDateAdaptor.dateList.indices){
             if (mDeliveryDateAdaptor.dateList[i].isCurrentDate){
                 deliveryDateLayoutManager.scrollToPosition(i)

             }

         }

     }

     private fun setPickupCalenderData() {
        tvMonth.text=monthsArray[pickupCurrentMonth-1]+" "+pickupCurrentYear
        val dateList=FindDate.getDate(monthsArray[pickupCurrentMonth-1],pickupCurrentYear,currentDate)
        pickupDateLayoutManager=LinearLayoutManager(requireContext(),LinearLayoutManager.HORIZONTAL,false)
        rvDayPickup.layoutManager=pickupDateLayoutManager
        mPickupDateAdaptor=RvPickupDateAdaptor(requireContext(),requireActivity(),dateList,this@ScheduleDateTimeFragment)
        rvDayPickup.adapter=mPickupDateAdaptor
        for (i in mPickupDateAdaptor.dateList.indices){
            if (mPickupDateAdaptor.dateList[i].isCurrentDate){
                pickupDateLayoutManager.scrollToPosition(i)

            }

        }
    }

    private fun showFragment(frag : Fragment) {
        val fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.flSchedulePickup, frag)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }
     override fun onClick(p0: View?) {
         when (p0?.id) {
             R.id.imgNextMonth -> {

                 if (pickupCurrentMonth==12){
                     pickupCurrentMonth=1
                     pickupCurrentYear=pickupCurrentYear+1

                 }else{
                     pickupCurrentMonth=pickupCurrentMonth+1

                 }
                 setPickupCalenderData()
             }
             R.id.imgPreviousMonth -> {
if (pickupCurrentMonth==todayMonth && pickupCurrentYear==todayYear){
    Toast.makeText(requireContext(),"Pickup date can not be select from previous month.",Toast.LENGTH_LONG).show()

}
                 else {
    if (pickupCurrentMonth == 1) {
        pickupCurrentMonth = 12
        pickupCurrentYear = pickupCurrentYear - 1
    } else {
        pickupCurrentMonth = pickupCurrentMonth - 1

    }
    setPickupCalenderData()
}
         }
         R.id.llPickupDateContener -> {
             val selectedPosition:Int= p0.tag as Int
val mData=mPickupDateAdaptor.dateList[selectedPosition]
             selectedPickupDate=mData.date+" "+mData.month+" "+mData.year
             val currentDateFormat = SimpleDateFormat("d MMM yyyy", Locale.ENGLISH)
             val mSelectedDate=currentDateFormat.parse(selectedPickupDate)
             if (mSelectedDate.before(currentDate)){
             selectedPickupDate=""
                 Toast.makeText(requireContext(),"You can not select past date for order.",Toast.LENGTH_LONG).show()

             }else{
                 selectedPickupDate=mData.date+" "+mData.month+" "+mData.year
                 setPickupSelectedDateView(selectedPosition)

             }
         }
             R.id.imgNextMonthD -> {
                 if (deliveryCurrentMonth==12){
                     deliveryCurrentMonth=1
                     deliveryCurrentYear=deliveryCurrentYear+1

                 }else{
                     deliveryCurrentMonth=deliveryCurrentMonth+1

                 }
                 setDeliveryCalenderData()
             }
             R.id.imgPreviousMonthD -> {
if (deliveryCurrentMonth==todayMonth && deliveryCurrentYear==todayYear){
 Toast.makeText(requireContext(),"You can not select delivery date from previous month.",Toast.LENGTH_LONG).show()

}else {
    if (deliveryCurrentMonth == 1) {
        deliveryCurrentMonth = 12
        deliveryCurrentYear = deliveryCurrentYear - 1
    } else {
        deliveryCurrentMonth = deliveryCurrentMonth - 1

    }
    setDeliveryCalenderData()
}
             }
             R.id.llDeliveryDateContener -> {
                 val selectedPosition: Int = p0.tag as Int
                 val mData = mDeliveryDateAdaptor.dateList[selectedPosition]
                 selectedDeliveryDate = mData.date + " " + mData.month + " " + mData.year
                 val currentDateFormat = SimpleDateFormat("d MMM yyyy", Locale.ENGLISH)
                 val mSelectedDate = currentDateFormat.parse(selectedDeliveryDate)
                 if (!selectedPickupDate.isEmpty()) {
                     val formatPickUpDate=currentDateFormat.parse(selectedPickupDate)
                     if (mSelectedDate.before(currentDate)) {
                         selectedDeliveryDate = ""
                         Toast.makeText(requireContext(), "You can not select past date for order.", Toast.LENGTH_LONG)
                             .show()

                     }else if (mSelectedDate.before(formatPickUpDate)) {
                         selectedDeliveryDate = ""
                         Toast.makeText(requireContext(), "You can not select past date from pickup date for order.", Toast.LENGTH_LONG)
                             .show()

                     }else {
                         selectedDeliveryDate = mData.date + " " + mData.month + " " + mData.year
                         setDeliverySelectedDateView(selectedPosition)


                 }
             }else{
             Toast.makeText(requireContext(),"Please first select pickup date.",Toast.LENGTH_LONG).show()
                 }

         }
             R.id.llTimeContainer -> {

                 val selectedPosition:Int=p0.tag as Int
                 selectedPickupTime=timeSlot[selectedPosition]
                 val lastTime=selectedPickupTime.split("-")
                 if (!selectedPickupDate.isEmpty()) {

                     val selectedDateTime = selectedPickupDate + " " + lastTime[1].trim()
                     val currentDateFormat = SimpleDateFormat("d MMM yyyy hh:mm a", Locale.ENGLISH)

                     var selectedTime = currentDateFormat.parse(selectedDateTime)
                     val currentDateTimeStr = currentDateFormat.format(Calendar.getInstance().time)
                     val currentDateTime = currentDateFormat.parse(currentDateTimeStr)
                     if (selectedTime.before(currentDateTime)) {
                         selectedPickupTime = ""
                         Toast.makeText(requireContext(), "You can not select past time slot.", Toast.LENGTH_LONG)
                             .show()

                     } else {
                         mPickupTimeAdaptor.selecteSlot(selectedPosition)
                     }

                 }else{
                     Toast.makeText(requireContext(),"Please select pickup date first.",Toast.LENGTH_LONG).show()

                 }

             }
             R.id.llDeliveryTimeContainer -> {
                 if (!selectedDeliveryDate.isEmpty()){
                 val selectedPosition:Int=p0.tag as Int
                 selectedDeliveryTime=timeSlot[selectedPosition]
                 mDeliveryTimeAdaptor.selecteSlot(selectedPosition)
                 }else{
                     Toast.makeText(requireContext(),"Please select delivery date first.",Toast.LENGTH_LONG).show()

                 }

             }
             R.id.btnContinue -> {
                 goNext()

             }

         }
     }

     private fun goNext() {
         if (selectedPickupDate.isEmpty()&& selectedPickupTime.isEmpty() && selectedDeliveryDate.isEmpty() && selectedDeliveryTime.isEmpty()){
             Toast.makeText(requireContext(),"Please select date & time for Pickup & Delivery.",Toast.LENGTH_LONG).show()
         }else if (selectedPickupDate.isEmpty()){
             Toast.makeText(requireContext(),"Please select pickup date.",Toast.LENGTH_LONG).show()

         }else if (selectedPickupTime.isEmpty()){
             Toast.makeText(requireContext(),"Please select pickup time.",Toast.LENGTH_LONG).show()

         }else if (selectedDeliveryDate.isEmpty()){
             Toast.makeText(requireContext(),"Please select delivery date.",Toast.LENGTH_LONG).show()
         }else if (selectedDeliveryTime.isEmpty()){
             Toast.makeText(requireContext(),"Please select delivery time.",Toast.LENGTH_LONG).show()
         }else {
             val saveValue=AppSharedPreference(requireContext())
             saveValue.saveString("pickupDate",selectedPickupDate)
             saveValue.saveString("pickupTime",selectedPickupTime)
             saveValue.saveString("deliveryDate",selectedDeliveryDate)
             saveValue.saveString("deliveryTime",selectedDeliveryTime)
             showFragment(SelectPaymentMethodFragment())


         }
     }

     private fun setPickupSelectedDateView(selectedPosition: Int) {
         //selectedPickupDate=""
         for (i in mPickupDateAdaptor.dateList.indices){
             val dateData=mPickupDateAdaptor.dateList[i]
             if (selectedPosition==i) {
                 val updateDateModle = CalenderDataModel(
                     dateData.date,
                     dateData.month,
                     dateData.year,
                     dateData.day,
                     dateData.numbricMonth,
                     dateData.isCurrentDate,
                     true
                 )
                 mPickupDateAdaptor.dateList.set(i,updateDateModle)
             }else{
                 val updateDateModle = CalenderDataModel(
                     dateData.date,
                     dateData.month,
                     dateData.year,
                     dateData.day,
                     dateData.numbricMonth,
                     dateData.isCurrentDate,
                     false
                 )
                 mPickupDateAdaptor.dateList.set(i,updateDateModle)

             }
         }
         mPickupDateAdaptor.notifyDataSetChanged()
         pickupDateLayoutManager.scrollToPosition(selectedPosition)
     }


     private fun setDeliverySelectedDateView(selectedPosition: Int) {
         //selectedDeliveryDate=""
         for (i in mDeliveryDateAdaptor.dateList.indices){
             val dateData=mDeliveryDateAdaptor.dateList[i]
             if (selectedPosition==i) {
                 val updateDateModle = CalenderDataModel(
                     dateData.date,
                     dateData.month,
                     dateData.year,
                     dateData.day,
                     dateData.numbricMonth,
                     dateData.isCurrentDate,
                     true
                 )
                 mDeliveryDateAdaptor.dateList.set(i,updateDateModle)
             }else{
                 val updateDateModle = CalenderDataModel(
                     dateData.date,
                     dateData.month,
                     dateData.year,
                     dateData.day,
                     dateData.numbricMonth,
                     dateData.isCurrentDate,
                     false
                 )
                 mDeliveryDateAdaptor.dateList.set(i,updateDateModle)

             }
         }
         mDeliveryDateAdaptor.notifyDataSetChanged()
         deliveryDateLayoutManager.scrollToPosition(selectedPosition)
     }

     override fun onResume() {
         super.onResume()
         ScheduleMainActivity.backFragmentName="location"
     }
 }