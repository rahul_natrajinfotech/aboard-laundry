package com.natrajinfotech.aboardlaundry.ui

import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.View
import android.widget.Toast
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.RetroFitHelper.Interface.UserManagementInterface
import com.natrajinfotech.aboardlaundry.RetroFitHelper.OrderManagement
import com.natrajinfotech.aboardlaundry.RetroFitHelper.UserManagement
import com.natrajinfotech.aboardlaundry.adapter.RvNotificationAdaptor
import com.natrajinfotech.aboardlaundry.model.GetNotificationResponseModel
import com.natrajinfotech.aboardlaundry.utils.*
import com.natrajinfotechindia.brainiton.RetroFitHelper.ApiClient
import com.natrajinfotechindia.brainiton.utils.AppAlerts
import kotlinx.android.synthetic.main.activity_notification_page.*
import kotlinx.android.synthetic.main.content_notification.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class NotificationActivity : BaseActivity(), View.OnClickListener,
    RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {
    lateinit var mAdaptor: RvNotificationAdaptor
    private lateinit var notificationList: ArrayList<GetNotificationResponseModel.NotificationData>
    private lateinit var rvNotificationLayoutManager:LinearLayoutManager
    lateinit var deleteHandler: Handler
    private val deleteDelayTime: Long = 2000
    var totalPages:Int=0
    var isLastPage:Boolean=false
    var isLoading:Boolean=false
    var currentPage:Int=1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification_page)
        showHomeBackOnToolbar(toolBar)
        this.title = "Notification"
        inItView()
    }

    private fun inItView() {
        notificationList = ArrayList()
        @Suppress("UNCHECKED_CAST")
        notificationList =intent.getSerializableExtra("notificationList") as ArrayList<GetNotificationResponseModel.NotificationData>
        rvNotificationLayoutManager= LinearLayoutManager((this@NotificationActivity))
        mAdaptor = RvNotificationAdaptor(this@NotificationActivity, this@NotificationActivity, notificationList)
        rvNotification.layoutManager = rvNotificationLayoutManager
        rvNotification.adapter = mAdaptor
        rvNotification.itemAnimator = DefaultItemAnimator()
        val itemTouchHelperCallback = RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this)
        ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(rvNotification)

    rvNotification.addOnScrollListener(object: PaginationScrollListener(rvNotificationLayoutManager){
        override fun totalPageCount(): Int {
            return totalPages
        }

        override fun isLastPage(): Boolean {
            return isLastPage
        }

        override fun isLoading(): Boolean {
            return  isLoading
        }

        override fun loadMoreItems() {
            currentPage = currentPage + 1
            isLoading=true
            Handler().postDelayed({ getNextPageNotificationList() }, 1000)
        }


    } )



    }

    override fun onClick(v: View?) {
        when (v?.id) {

        }
    }

    override fun onBackPressed() {
        //super.onBackPressed()L̥
        val mDialog = AppProgressDialog(this@NotificationActivity)
        mDialog.show()
        OrderManagement(this@NotificationActivity, this@NotificationActivity, mDialog).getLatestOrder()

    }

    /**
     * callback when recycler view is swiped
     * item will be removed on swiped
     * undo option will be provided in snackbar to restore the item
     */
    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int) {
        if (viewHolder is RvNotificationAdaptor.NotificationViewHolder) {
            // get the removed item name to display it in snack bar
            val name = mAdaptor.notificationList[viewHolder.adapterPosition].title

            // backup of removed item for undo purpose
            val deletedItem = mAdaptor.notificationList[viewHolder.adapterPosition]
            val deletedIndex = viewHolder.adapterPosition

            // remove the item from recycler view
            mAdaptor.removeItem(viewHolder.adapterPosition)

            // showing snack bar with Undo option
            val snackbar = Snackbar
                .make(coordinator_layout, name + " removed from cart!", deleteDelayTime.toInt())
            snackbar.setAction("Undo", object : View.OnClickListener {
                override fun onClick(p0: View?) {
                    // undo is selected, restore the deleted item & cancel the delete api call Handelr

                    mAdaptor.restoreItem(deletedItem, deletedIndex)
                    deleteHandler.removeCallbacksAndMessages(null)
                }

            })
            snackbar.setActionTextColor(Color.YELLOW)
            snackbar.show()
            deleteHandler = Handler()
            deleteHandler.postDelayed(object : Runnable {
                override fun run() {
                    val notificationId = deletedItem.id.toString()
                    val mDialog = AppProgressDialog(this@NotificationActivity)
                    mDialog.show()
                    UserManagement(this@NotificationActivity, this@NotificationActivity, mDialog).deleteNotification(
                        notificationId
                    )
                }


            }, deleteDelayTime)
        }
    }

    fun getNextPageNotificationList() {
        val mProgressDialog=AppProgressDialog(this@NotificationActivity)
        mProgressDialog.show()
        val requestInterface = ApiClient.getClient().create(UserManagementInterface::class.java)
        val token = AppSharedPreference(mContext).getString("apiToken")
        val getNotificationCall = requestInterface!!.getNotificationList(token,currentPage)
        getNotificationCall.enqueue(object : Callback<GetNotificationResponseModel> {
            override fun onFailure(call: Call<GetNotificationResponseModel>, t: Throwable) {
                mProgressDialog.dialog.dismiss()
                AppAlerts().showAlertMessage(mContext, "Error", t.message!!)
            }

            override fun onResponse(
                call: Call<GetNotificationResponseModel>,
                response: Response<GetNotificationResponseModel>
            ) {
                mProgressDialog.dialog.dismiss()
                if (response.isSuccessful) {
                    val responseCode = response.body()!!.ResponseCode
                    when {
                        responseCode == "200" -> {
                            val mNotificationList = response.body()!!.Data
                            for (i in mNotificationList!!.indices) {
                                mAdaptor.notificationList.add(mNotificationList[i])
                            }
                            mAdaptor.notifyDataSetChanged()
                            isLoading = false
                            if (currentPage == response.body()!!.total_page) {
                                isLastPage = true

                            }


                        }
                        responseCode == "400" -> Toast.makeText(
                            mContext,
                            response.body()!!.ResponseMessage,
                            Toast.LENGTH_LONG
                        ).show()
                    }

                } else {
                    AppAlerts().showAlertMessage(mContext, "Error", response.message())

                }


            }


        })

    }


}