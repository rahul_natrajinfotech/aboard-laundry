package com.natrajinfotech.aboardlaundry.ui

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.places.AutocompleteFilter
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.Places
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.*
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.RetroFitHelper.UserAddressManagement
import com.natrajinfotech.aboardlaundry.adapter.PlaceArrayAdapter
import com.natrajinfotech.aboardlaundry.model.GetAreaListResponceModel
import com.natrajinfotech.aboardlaundry.model.GetUserAddressListResponceModel
import com.natrajinfotech.aboardlaundry.utils.*
import com.natrajinfotechindia.brainiton.utils.AppAlerts
import kotlinx.android.synthetic.main.activity_add_update_user_address.*


class AddUpdateUserAddressActivity : BaseActivity(), View.OnClickListener, OnMapReadyCallback,
    GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener,
    com.google.android.gms.location.LocationListener {


    var name: String = ""
    var areaId: String = ""
    var florrNo: String = """"""
    var block: String = ""
    var street: String = ""
    var buildingName: String = ""
    var phoneNumber: String = ""
    var lat: Double = 0.0
    var lng: Double = 0.0
    var addressId: String = ""
    var from: String = ""
    lateinit var mGoogleMap: GoogleMap
    lateinit var mGoogleApiClient: GoogleApiClient
    lateinit var areaList: ArrayList<GetAreaListResponceModel.AreaListData>
    lateinit var mAdaptor: ArrayAdapter<String>
    var request: LocationRequest = LocationRequest.create()
        .setInterval(5000)
        .setFastestInterval(16)
        .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
    lateinit var mPlaceArrayAdapter: PlaceArrayAdapter
    val GOOGLE_API_CLIENT_ID: Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_update_user_address)
        //mContext=this@AddUpdateUserAddressActivity
        showHomeBackOnToolbar(toolBar)
        fragmentMap.onCreate(savedInstanceState)
        initViews()

    }

    private fun initViews() {
        etArea.threshold = 1
        from = intent.getStringExtra("from")
        if (from.equals("edit")) {
            setEditData()

        } else {
            val gpsTracker = GPSTracker(this@AddUpdateUserAddressActivity)
            lat = gpsTracker.latitude
            lng = gpsTracker.longitude
            this.title = "Add New Address"
            btnSave.text = resources.getString(R.string.save)
            @Suppress("UNCHECKED_CAST")
            areaList = intent.getSerializableExtra("area") as ArrayList<GetAreaListResponceModel.AreaListData>
            val mAreaList = ArrayList<String>()
            for (i in areaList.indices) {
                mAreaList.add(areaList[i].area_name)
            }
            mAdaptor = ArrayAdapter(this@AddUpdateUserAddressActivity, android.R.layout.simple_list_item_1, mAreaList)
            etArea.setAdapter(mAdaptor)
            mAdaptor.notifyDataSetChanged()

        }
        setMap()
        etName
        etArea
        etBlock
        btnSave.setOnClickListener(this)
        etArea.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                areaId = ""
                val userInput = p0.toString()

                val mAreaList = ArrayList<String>()
                for (i in areaList.indices) {
                    if (areaList[i].area_name.toLowerCase().contains(userInput.toLowerCase())) {
                        mAreaList.add(areaList[i].area_name)
                    }
                }


                mAdaptor =
                    ArrayAdapter(this@AddUpdateUserAddressActivity, android.R.layout.simple_list_item_1, mAreaList)
                etArea.setAdapter(mAdaptor)
                mAdaptor.notifyDataSetChanged()
            }


        })
        etArea.setOnItemClickListener(object : AdapterView.OnItemClickListener {
            override fun onItemClick(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                val inputManger: InputMethodManager =
                    this@AddUpdateUserAddressActivity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputManger.hideSoftInputFromWindow(etArea.windowToken, 0)

                val areaTv: TextView = p1 as TextView
                val areaName = areaTv.text.toString().trim()
                for (i in areaList.indices) {
                    if (areaList[i].area_name.equals(areaName)) {
                        areaId = areaList[i].id.toString()

                    }

                }
            }


        })

    }


    private fun setEditData() {
        this.title = "Update Address"
        btnSave.text = resources.getString(R.string.update)
        val addressData: GetUserAddressListResponceModel.AddressListData =
            intent.getSerializableExtra("data") as GetUserAddressListResponceModel.AddressListData
        addressId = addressData.id.toString()
        @Suppress("UNCHECKED_CAST")
        areaList = intent.getSerializableExtra("area") as ArrayList<GetAreaListResponceModel.AreaListData>
        val mAreaList = ArrayList<String>()
        for (i in areaList.indices) {
            mAreaList.add(areaList[i].area_name)
            if (areaList[i].area_name.equals(addressData.area_name)) {
                areaId = areaList[i].id.toString()

            }
        }
        mAdaptor = ArrayAdapter(this@AddUpdateUserAddressActivity, android.R.layout.simple_list_item_1, mAreaList)
        etArea.setAdapter(mAdaptor)
        mAdaptor.notifyDataSetChanged()
        etArea.threshold = 1

        lat = addressData.latitude.toDouble()
        lng = addressData.longitude.toDouble()
        etName.setText(addressData.name)
        etMobile.setText(addressData.phone_number)
        etBlock.setText(addressData.block_no)
        etBuilding.setText(addressData.building_no)
        etStreet.setText(addressData.street_no)
        etArea.setText(addressData.area_name)
        etFloor.setText(addressData.floor_no)
    }

    private fun setMap() {
        try {

            fragmentMap.onResume()
            MapsInitializer.initialize(this@AddUpdateUserAddressActivity)

            fragmentMap.getMapAsync(object : OnMapReadyCallback {
                @SuppressLint("MissingPermission")
                override fun onMapReady(p0: GoogleMap?) {
                    mGoogleMap = p0!!
                    mGoogleMap.isMyLocationEnabled = true
                    mGoogleMap.uiSettings.isMyLocationButtonEnabled = true

                    // val locationButton = (mMapView.findViewById<View>(Integer.parseInt("1")).getParent()).findViewById<View>(Integer.parseInt("2") as android.view.View)
                    val locationButton = fragmentMap!!.rootView.findViewById<View>(Integer.parseInt("2")) as View
                    // and next place it, for exemple, on bottom right (as Google Maps app)
                    val rlp = locationButton.getLayoutParams() as RelativeLayout.LayoutParams
                    // position on right bottom
                    rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
                    rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
                    mGoogleMap.setPadding(0, 0, 0, 30)

                    MarkerOptions().position(LatLng(lat, lng))
                        .title("Your Location")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE))
                    //mGoogleMap.addMarker(marker)
                    val cameraPosition = CameraPosition.Builder().target(LatLng(lat, lng)).zoom(17F).build()
                    mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))

                    @Suppress("DEPRECATION")
                    mGoogleMap.setOnCameraChangeListener(object : GoogleMap.OnCameraChangeListener {
                        override fun onCameraChange(p0: CameraPosition?) {
                            lat = p0!!.target.latitude
                            lng = p0.target.longitude


                        }

                    })
                }


            })
            setAutoAddressView()
        } catch (e: Exception) {
            Log.v("dip", "map error :" + e.message)

        }
    }

    private fun setAutoAddressView() {
        val latRadian: Double = Math.toRadians(lat)
        val degLatKm = 1.574235
        val degLongKm: Double = 1.572833 * Math.cos(latRadian)
        val mDistanceInMeters = 500.0
        val deltaLat: Double = mDistanceInMeters / 1000.0 / degLatKm
        val deltaLong: Double = mDistanceInMeters / 1000.0 / degLongKm
        val minLat = lat - deltaLat
        val minLong = lng - deltaLong
        val maxLat = lat + deltaLat
        val maxLong = lng + deltaLong
        val mLatLngBounds = LatLngBounds(LatLng(minLat, minLong), LatLng(maxLat, maxLong))
        //for setting country wise filter
        val autocompleteFilter = AutocompleteFilter.Builder()
            .setTypeFilter(Place.TYPE_SUBLOCALITY_LEVEL_1)
            .setCountry("IN")
            .build()
        mPlaceArrayAdapter = PlaceArrayAdapter(
            this@AddUpdateUserAddressActivity, R.layout.layout_place_autocomplete,
            mLatLngBounds, autocompleteFilter
        )

        mGoogleApiClient = GoogleApiClient.Builder(this@AddUpdateUserAddressActivity)
            .addApi(LocationServices.API)
            .addApi(Places.GEO_DATA_API)
            .enableAutoManage(this@AddUpdateUserAddressActivity, GOOGLE_API_CLIENT_ID, this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .build()
        locationEdTxt.threshold = 0
        locationEdTxt.setAdapter(mPlaceArrayAdapter)
        locationEdTxt.onItemClickListener = object : AdapterView.OnItemClickListener {
            override fun onItemClick(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                @Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
                Places.GeoDataApi.getPlaceById(mGoogleApiClient, mPlaceArrayAdapter.getItem(p2).placeId.toString())
                    .setResultCallback {

                            p0 ->
                        if (p0.status.isSuccess) {
                            val myPlace: Place = p0.get(0)
                            val queriedLocation: LatLng = myPlace.latLng
                            AppSharedPreference(this@AddUpdateUserAddressActivity).saveString(
                                "lat",
                                "" + queriedLocation.latitude
                            )
                            AppSharedPreference(this@AddUpdateUserAddressActivity).saveString(
                                "lng",
                                "" + queriedLocation.longitude
                            )
                            lat = queriedLocation.latitude
                            lng = queriedLocation.longitude
                            val cameraPosition = CameraPosition.Builder().target(LatLng(lat, lng)).zoom(17F).build()
                            mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))


                        } else {
                            Toast.makeText(
                                this@AddUpdateUserAddressActivity,
                                "Sorry Try Again. Some thing went Wrong.",
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }


            }
        }

    }


    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.btnSave -> {
                getData()
                if (isValidInput()) {
                    addUpdateAddress()

                }
            }


        }
    }

    private fun addUpdateAddress() {
        if (NetworkHandler(this@AddUpdateUserAddressActivity).isNetworkAvailable()) {
            val mDialog = AppProgressDialog(this@AddUpdateUserAddressActivity)
            mDialog.show()
            val token = AppSharedPreference(this@AddUpdateUserAddressActivity).getString("apiToken")
            val requestMap = HashMap<String, String>()
            requestMap["token"] = token
            if (from.equals("edit")) {
                requestMap["id"] = addressId
            }
            requestMap["name"] = name
            requestMap["block_no"] = block
            requestMap["floor_no"] = florrNo
            requestMap["area_id"] = areaId
            requestMap["phone_number"] = phoneNumber
            requestMap["street_no"] = street
            requestMap["building_no"] = buildingName
            requestMap["latitude"] = "" + lat
            requestMap["longitude"] = "" + lng

            UserAddressManagement(this,this@AddUpdateUserAddressActivity,mDialog,requestMap).addUpdateAddress()

        } else {
            AppAlerts().showAlertMessage(
                this@AddUpdateUserAddressActivity,
                "Info",
                this@AddUpdateUserAddressActivity.resources.getString(R.string.networkError)
            )

        }
    }

    private fun isValidInput(): Boolean {
        if (name.isEmpty() && block.isEmpty() && areaId.isEmpty() && street.isEmpty() && buildingName.isEmpty() && phoneNumber.isEmpty() && lat == 0.0 && lng == 0.0 && florrNo.isEmpty()) {
            showToast(this@AddUpdateUserAddressActivity, resources.getString(R.string.allFieldError))
            return false
        } else if (name.isEmpty() || name.matches("[a-zA-z .?]*".toRegex()) == false) {
            showToast(this@AddUpdateUserAddressActivity, resources.getString(R.string.nameError))
            return false
        } else if (phoneNumber.isEmpty()) {
            showToast(
                this@AddUpdateUserAddressActivity,
                this@AddUpdateUserAddressActivity.resources.getString(R.string.mobileError)
            )
            return false
        } else if (buildingName.isEmpty()) {
            showToast(
                this@AddUpdateUserAddressActivity,
                this@AddUpdateUserAddressActivity.resources.getString(R.string.buildingError)
            )
            return false
        } else if (florrNo.isEmpty()) {
            showToast(this@AddUpdateUserAddressActivity, resources.getString(R.string.floorError))
            return false
        } else if (block.isEmpty()) {
            showToast(
                this@AddUpdateUserAddressActivity,
                this@AddUpdateUserAddressActivity.resources.getString(R.string.blockError)
            )
            return false
        } else if (street.isEmpty()) {
            showToast(this, this@AddUpdateUserAddressActivity.resources.getString(R.string.streetError))
            return false
        } else if (areaId.isEmpty()) {
            showToast(this@AddUpdateUserAddressActivity, resources.getString(R.string.areaError))
            return false
        } else if (lat == 0.0 || lng == 0.0) {
            showToast(
                this@AddUpdateUserAddressActivity,
                this@AddUpdateUserAddressActivity.resources.getString(R.string.selectMapLocationError)
            )
            return false
        } else {
            return true

        }
    }

    private fun getData() {
        name = etName.text.toString().trim()
        //areaId=etArea.text.toString().trim()

        florrNo = etFloor.text.toString().trim()
        block = etBlock.text.toString().trim()
        street = etStreet.text.toString().trim()
        buildingName = etBuilding.text.toString().trim()
        phoneNumber = etMobile.text.toString().trim()


    }

    override fun onMapReady(p0: GoogleMap?) {

    }

    override fun onConnected(p0: Bundle?) {
//var mLocation=LocationServices.FusedLocationApi.getLastLocation(mgoa)
        if (ActivityCompat.checkSelfPermission(
                this@AddUpdateUserAddressActivity,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this@AddUpdateUserAddressActivity,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            @Suppress("DEPRECATION")
            LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, request, this
            )
            mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient)
        }
        @Suppress("DEPRECATION")
        LocationServices.FusedLocationApi.requestLocationUpdates(
            mGoogleApiClient, request, this
        )
        mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient)


    }

    override fun onConnectionSuspended(p0: Int) {

    }

    override fun onConnectionFailed(p0: ConnectionResult) {

    }

    override fun onLocationChanged(p0: Location?) {

    }

    private fun getAddressList() {
        if (NetworkHandler(this@AddUpdateUserAddressActivity).isNetworkAvailable()) {
            val mDialog = AppProgressDialog(this@AddUpdateUserAddressActivity)
            mDialog.show()
            UserAddressManagement(
                this@AddUpdateUserAddressActivity,
                this@AddUpdateUserAddressActivity,
                mDialog
            ).getAddressList(Intent(this@AddUpdateUserAddressActivity, UserAddressListActivity::class.java))

        } else {
            AppAlerts().showAlertMessage(
                this@AddUpdateUserAddressActivity,
                "Info",
                resources.getString(R.string.networkError)
            )

        }
    }


    override fun onBackPressed() {
        //super.onBackPressed()
        getAddressList()
    }
}
