package com.natrajinfotech.aboardlaundry.utils

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by Ronak on 3/21/2016.
 */
class AppSharedPreference(mContext: Context) {
    var mContext:Context
    init {
        this.mContext=mContext
    }

    fun saveString(key: String, value: String): Boolean {
        try {


           var sharedPreference:SharedPreferences=mContext.getSharedPreferences("atm",0)

            val editor = sharedPreference.edit()
            editor.putString(key, value)
            editor.commit()
            return true
        } catch (e: Exception) {
            return false

        }

    }

    fun getString(key: String): String {
            var value: String? = ""
        try {

            val sharedPreferences =  mContext.getSharedPreferences("atm", 0)

            value = sharedPreferences.getString(key, "")
            if (value == "") {
                value = ""
                return value

            } else {
                return value
            }

        } catch (e: Exception) {
            e.printStackTrace()
            return value!!
        }

    }

    fun saveInt(key: String, value: Int): Boolean {
        try {


            val sharedPreferences = mContext.getSharedPreferences("atm", 0)

            val editor = sharedPreferences.edit()
            editor.putInt(key, value)
            editor.commit()
            return true
        } catch (e: Exception) {
            return false

        }

    }

    fun getInt(key: String): Int {
        var value = 0
        try {

            val sharedPreferences = mContext.getSharedPreferences("atm", 0)

            value = sharedPreferences.getInt(key, 0)
            if (value == 0) {
                value = 0
                return value

            } else {
                return value
            }

        } catch (e: Exception) {
            e.printStackTrace()
            return value
        }

    }

    /*public boolean saveDouble(String key, double value){
        try {


            SharedPreferences sharedPreferences = mContext.getSharedPreferences("cashCollection", mContext.MODE_PRIVATE);

            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.put(key, value);
            editor.commit();
            return true;
        }catch (Exception e){
            return false;

        }
    }*/

    fun saveBoolean(key: String, value: Boolean): Boolean {
        try {
            val sharedPreferences = mContext.getSharedPreferences("atm", 0)

            val editor = sharedPreferences.edit()
            editor.putBoolean(key, value)
            editor.commit()
            return true
        } catch (e: Exception) {
            return false

        }

    }

    fun getBoolean(key: String): Boolean {
        var value = false
        try {

            val sharedPreferences = mContext.getSharedPreferences("atm", 0)

            value = sharedPreferences.getBoolean(key, false)

            return value

        } catch (e: Exception) {
            e.printStackTrace()
            return value
        }

    }


    fun saveLong(key: String, value: Long): Boolean {
        try {
            val sharedPreferences = mContext.getSharedPreferences("atm", 0)

            val editor = sharedPreferences.edit()
            editor.putLong(key, value)
            editor.commit()
            return true
        } catch (e: Exception) {
            return false

        }

    }

    fun getLong(key: String): Long {
        var value: Long = 0
        try {

            val sharedPreferences = mContext.getSharedPreferences("atm", 0)

            value = sharedPreferences.getLong(key, 0)

            return value

        } catch (e: Exception) {
            e.printStackTrace()
            return value
        }

    }


    fun DeleteSharedPreference() {

        val sharedPreferences = mContext.getSharedPreferences("atm", 0)
        val editor = sharedPreferences.edit()
        editor.clear()
        editor.commit()
    }

    fun saveBooleanDatabase(key: String, value: Boolean): Boolean {
        try {
            val sharedPreferences = mContext.getSharedPreferences("ormLite", 0)

            val editor = sharedPreferences.edit()
            editor.putBoolean(key, value)
            editor.commit()
            return true
        } catch (e: Exception) {
            return false

        }

    }

    fun getBooleanDatabase(key: String): Boolean {
        var value = false
        try {

            val sharedPreferences = mContext.getSharedPreferences("ormLite", 0)

            value = sharedPreferences.getBoolean(key, false)

            return value

        } catch (e: Exception) {
            e.printStackTrace()
            return value
        }

    }


    //for database string like date & other
    fun saveDatabaseString(key: String, value: String): Boolean {
        try {


            val sharedPreferences = mContext.getSharedPreferences("ormLite", 0)

            val editor = sharedPreferences.edit()
            editor.putString(key, value)
            editor.commit()
            return true
        } catch (e: Exception) {
            return false

        }

    }

    fun getDatabaseString(key: String): String {
        var value: String? = ""
        try {

            val sharedPreferences = mContext.getSharedPreferences("ormLite", 0)

            value = sharedPreferences.getString(key, "")
            if (value == "") {
                value = ""
                return value

            } else {
                return value
            }

        } catch (e: Exception) {
            e.printStackTrace()
            return value!!
        }

    }


}
