package com.natrajinfotech.aboardlaundry.utils;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.natrajinfotech.aboardlaundry.R;
import com.natrajinfotech.aboardlaundry.RetroFitHelper.ItemManagement;
import com.natrajinfotech.aboardlaundry.model.GetItemListResponseModel;


public class CategoryItemDetailViewHolder extends ChildViewHolder {
    private Context mContext;
    private Activity mActivity;
    private TextView tvCategoryItemName;
    private ImageView imgCategoryItemImage;
    private LinearLayout llMain;

    public CategoryItemDetailViewHolder(@NonNull View itemView, Context mContext, Activity mActivity) {
        super(itemView);
        this.mContext = mContext;
        this.mActivity = mActivity;
        tvCategoryItemName = (TextView) itemView.findViewById(R.id.tvCategoryItemName);
        imgCategoryItemImage = (ImageView) itemView.findViewById(R.id.imgCategoryItem);
        llMain = (LinearLayout) itemView.findViewById(R.id.llMain);
    }

    public void bind(@NonNull final GetItemListResponseModel.GroupItemListData categoryItem) {
        tvCategoryItemName.setText(categoryItem.getItem_name());
        Glide.with(mContext)
                .load(categoryItem.getIcon_path())
                .placeholder(R.mipmap.ic_launcher)
                .into(imgCategoryItemImage);
        llMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppProgressDialog mDialog = new AppProgressDialog(mContext);
                mDialog.show();
                new ItemManagement(mContext, mActivity, mDialog).getItemServiceRate(categoryItem.getId(), categoryItem.getItem_name(), categoryItem.getIcon_path());

            }
        });
    }
}

/*L̥*/
