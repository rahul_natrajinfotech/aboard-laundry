package com.natrajinfotech.aboardlaundry.utils

import android.app.Dialog
import android.content.Context
import android.util.Log
import android.view.Window
import com.natrajinfotech.aboardlaundry.R
import dmax.dialog.SpotsDialog

class AppProgressDialog(mContext:Context) {
    private var mContext:Context
    init {
    this.mContext=mContext
}

lateinit var dialog: Dialog

        fun show() {
            try {
dialog=Dialog(mContext)
                if (dialog.isShowing())
                    return
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialog.setContentView(R.layout.layout_progress_bar)
                   dialog.setCancelable(false)
                if (dialog.getWindow() != null)
                    dialog.getWindow()!!.setBackgroundDrawableResource(android.R.color.transparent)
                dialog.show()

            } catch (e: Exception) {
                e.printStackTrace()
                Log.v("dip",e.message)
            }


        }

        fun hide(dialog: SpotsDialog?) {
            if (dialog != null) {
                if (dialog.isShowing) {
                    dialog.dismiss()
                }
            }
        }

} 