package com.natrajinfotech.aboardlaundry.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.natrajinfotech.aboardlaundry.R;
import com.natrajinfotech.aboardlaundry.model.GetItemListResponseModel;

public class CategoryDetailViewHolder extends ParentViewHolder {

    private static final float INITIAL_POSITION = 0.0f;
    private static final float ROTATED_POSITION = 90f;

    @NonNull
    private final ImageView mArrowExpandImageView;
    private TextView mCategoryTextView;
    private ImageView mCategoryImageView;
    private Context mContext;
    public CategoryDetailViewHolder(@NonNull View itemView, Context mContext) {
        super(itemView);
        this.mContext=mContext;
        mCategoryTextView = (TextView) itemView.findViewById(R.id.tvCategory);
        mCategoryImageView= (ImageView) itemView.findViewById(R.id.imgCategory);

        mArrowExpandImageView = (ImageView) itemView.findViewById(R.id.ivMore);
    }

    public void bind(@NonNull GetItemListResponseModel.GroupListData category) {
        mCategoryTextView.setText(category.getGroup_name());
        Glide.with(mContext)
                .load(category.getIcon_path())
                .placeholder(R.mipmap.ic_launcher)
                .into(mCategoryImageView);

    }

    @SuppressLint("NewApi")
    @Override
    public void setExpanded(boolean expanded) {
        super.setExpanded(expanded);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            if (expanded) {
                mArrowExpandImageView.setRotation(ROTATED_POSITION);
            } else {
                mArrowExpandImageView.setRotation(INITIAL_POSITION);
            }
        }
    }

    @Override
    public void onExpansionToggled(boolean expanded) {
        super.onExpansionToggled(expanded);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            RotateAnimation rotateAnimation;
            if (expanded) { // rotate clockwise
                 rotateAnimation = new RotateAnimation(ROTATED_POSITION,
                        INITIAL_POSITION,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f);
            } else { // rotate counterclockwise
                rotateAnimation = new RotateAnimation(-1 * ROTATED_POSITION,
                        INITIAL_POSITION,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f);
            }

            rotateAnimation.setDuration(200);
            rotateAnimation.setFillAfter(true);
            mArrowExpandImageView.startAnimation(rotateAnimation);
        }
    }
}
