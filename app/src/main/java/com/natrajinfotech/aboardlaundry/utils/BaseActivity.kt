package com.natrajinfotech.aboardlaundry.utils

import android.app.ActivityOptions
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View

import kotlin.reflect.KClass
import android.util.Patterns
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.*
import com.natrajinfotech.aboardlaundry.R
import java.text.SimpleDateFormat
import java.util.*


abstract class BaseActivity : AppCompatActivity() {

    //var retrofitClient: RetrofitApiClient? = null
    lateinit var mContext: Context

    val LOGIN_BROADCAST_FILTER = "onLoginSuccess"
    val EXTRA_INTENT = "getExtra"

    override fun onCreate(savedInstanceState: Bundle?) {
        //retrofitClient = RetrofitService.retrofit
        mContext = this
        super.onCreate(savedInstanceState)
    }

    fun proceedToNext(cls: KClass<*>) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            startActivity(Intent(this, cls.java),
                ActivityOptions.makeSceneTransitionAnimation(this).toBundle())
        } else {
            startActivity(Intent(this, cls.java))
        }
    }

    fun proceedToNextWithPutExtra(cls: KClass<*>, extra: String) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            startActivity(Intent(this, cls.java)
                .putExtra(EXTRA_INTENT, extra),
                ActivityOptions.makeSceneTransitionAnimation(this).toBundle())
        } else {
            startActivity(Intent(this, cls.java).putExtra(EXTRA_INTENT, extra))
        }
    }

    fun proceedToNextWithBundel(cls: KClass<*>, bundel: Any) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            startActivity(Intent(this, cls.java)
                .putExtra(EXTRA_INTENT, bundel as Parcelable),
                ActivityOptions.makeSceneTransitionAnimation(this).toBundle())
        } else {
            startActivity(Intent(this, cls.java)
                .putExtra(EXTRA_INTENT, bundel as Parcelable))
        }
    }

    fun showHomeBackOnActionbar() {
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    fun showHomeBackOnToolbar(toolbar: Toolbar) {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    fun isLogin(): Boolean {
        //return AppPreference.getBooleanPreference(mContext, Constant.IS_LOGIN)
        return true
    }

    fun isValidEmail(target: CharSequence): Boolean {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }

    /*fun showCustomToast(ctx: Context, type: String, msg: String) {
        val inflater = layoutInflater
        var layout = inflater . inflate (
            R.layout.layout_toast_success,
            findViewById<ViewGroup>(R.id.toast_layout_root))
        if (type == "Error"){
            layout = inflater . inflate (R.layout.layout_toast_error,
                findViewById<ViewGroup>(R.id.toast_layout_root))
        }

        val text =layout . findViewById<TextView>(R.id.tvToastSuccess)
        text.text = msg

        val toast = Toast(mContext)
        //toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0)
        toast.duration = Toast.LENGTH_SHORT
        toast.view = layout
        toast.show()
    }*/

    companion object {
        fun showToast(ctx: Context, msg: String) {
            Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show()
        }

        fun showSnackBar(view: View, msg: String) {
            Snackbar.make(view, msg, Snackbar.LENGTH_LONG).show()
        }

        fun showAlert(ctx: Context, msg: String) {
            AlertDialog.Builder(ctx) /* R.style.AlertDialogCustom*/
                .setTitle("Alert")
                .setMessage(msg)
                .setPositiveButton("Okay", null)
                .create()
                .show()
        }

        fun showAlertWithAction(ctx: Context, title: String, msg: String, cancelable: Boolean,
                                positiveString: String, nagativeString: String,
                                positiveAction: DialogInterface.OnClickListener,
                                negativeAction: DialogInterface.OnClickListener?) {
            AlertDialog.Builder(ctx)  /* R.style.AlertDialogCustom*/
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(cancelable)
                .setPositiveButton(positiveString, positiveAction)
                .setNegativeButton(nagativeString, negativeAction)
                .create()
                .show()
        }

        /*fun getDialog(ctx: Context): SpotsDialog {
            val dialog = SpotsDialog(ctx) //R.style.dialog_theme
            dialog.setCancelable(false)
            return dialog
        }*/

        fun getTodayDate(): String{
            val c = Calendar.getInstance().time
            println("Current time => $c")

            val df = SimpleDateFormat("d MMM yyyy", Locale.US)

            return df.format(c)
        }


        /*fun showSuccessToast(ctx: Context, msg: String){
            val inflater = Context.LAYOUT_INFLATER_SERVICE()
View layout = inflater.inflate(R.layout.toast_layout,
                               (ViewGroup) findViewById(R.id.toast_layout_root));

ImageView image = (ImageView) layout.findViewById(R.id.image);
image.setImageResource(R.drawable.android);
TextView text = (TextView) layout.findViewById(R.id.text);
text.setText("Hello! This is a custom toast!");

Toast toast = new Toast(getApplicationContext());
toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
toast.setDuration(Toast.LENGTH_LONG);
toast.setView(layout);
toast.show();
        }*/

    }
}