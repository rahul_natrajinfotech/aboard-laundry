package com.natrajinfotech.aboardlaundry.utils;

import android.annotation.SuppressLint;
import android.os.Build;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.TextView;
import com.natrajinfotech.aboardlaundry.R;
import com.natrajinfotech.aboardlaundry.model.OrderBasketListModel;

/**
 * Created by Dipendra Sharma  on 08-01-2019.
 */
public class BasketServiceViewHolder extends ParentViewHolder {

    private static final float INITIAL_POSITION = 0.0f;
    private static final float ROTATED_POSITION = 90f;

    @NonNull

    private TextView mServiceTextView;

    public BasketServiceViewHolder(@NonNull View itemView) {
        super(itemView);
        mServiceTextView = (TextView) itemView.findViewById(R.id.tvServiceName);



    }

    public void bind(@NonNull OrderBasketListModel service) {
        mServiceTextView.setText(service.getServiceName());
    }

    @SuppressLint("NewApi")
    @Override
    public void setExpanded(boolean expanded) {
        super.setExpanded(expanded);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            if (expanded) {
                //mArrowExpandImageView.setRotation(ROTATED_POSITION);
            } else {
                //mArrowExpandImageView.setRotation(INITIAL_POSITION);
            }
        }
    }

    @Override
    public void onExpansionToggled(boolean expanded) {
        super.onExpansionToggled(expanded);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            RotateAnimation rotateAnimation;
            if (expanded) { // rotate clockwise
                rotateAnimation = new RotateAnimation(ROTATED_POSITION,
                        INITIAL_POSITION,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f);
            } else { // rotate counterclockwise
                rotateAnimation = new RotateAnimation(-1 * ROTATED_POSITION,
                        INITIAL_POSITION,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f);
            }

            rotateAnimation.setDuration(200);
            rotateAnimation.setFillAfter(true);
            //mArrowExpandImageView.startAnimation(rotateAnimation);
        }
    }
}

