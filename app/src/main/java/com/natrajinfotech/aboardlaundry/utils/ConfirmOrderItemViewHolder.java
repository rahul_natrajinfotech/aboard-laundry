package com.natrajinfotech.aboardlaundry.utils;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;
import com.natrajinfotech.aboardlaundry.Database.OrmLiteDb;
import com.natrajinfotech.aboardlaundry.R;
import com.natrajinfotech.aboardlaundry.adapter.RvConfirmOrderItem;
import com.natrajinfotech.aboardlaundry.model.OrderBasketDbModel;
import com.natrajinfotech.aboardlaundry.ui.fragment.ConfirmOrderFragment;

/**
 * Created by Dipendra Sharma  on 21-01-2019.
 */
public class ConfirmOrderItemViewHolder extends ChildViewHolder {
    private Context mContext;
    private Activity mActivity;
    RvConfirmOrderItem mAdaptor;
    private TextView tvItemName;
    private TextView tvDecreaseQuantity;
    private TextView tvQuantity;
    private TextView tvIncreaseQuantity;
    private TextView tvPrice;
    private OrmLiteDb ormLiteDb;


    public ConfirmOrderItemViewHolder(@NonNull View itemView, Context mContext, Activity mActivity, RvConfirmOrderItem mAdaptor) {
        super(itemView);
        this.mContext=mContext;
        this.mActivity=mActivity;
        this.mAdaptor=mAdaptor;
        tvItemName = itemView.findViewById(R.id.tvItemName);
        tvDecreaseQuantity= itemView.findViewById(R.id.tvDecreaseQuantity);
        tvIncreaseQuantity= itemView.findViewById(R.id.tvIncreaseQuantity);
        tvQuantity= itemView.findViewById(R.id.tvItemQuantity);
        tvPrice= itemView.findViewById(R.id.tvPrice);


    }

    public void bind(@NonNull final OrderBasketDbModel itemDetail, final int parentPosition, final int childPosition) {
        tvItemName.setText(itemDetail.getItemName());
        tvQuantity.setText(itemDetail.getServicesItemQuantity());
        float servicePrice=Float.parseFloat(itemDetail.servicesRate);
        int itemQuantity=Integer.parseInt(itemDetail.servicesItemQuantity);
        float finalItemRate=itemQuantity*servicePrice;
        tvPrice.setText(String.valueOf(finalItemRate));
        String activityName=mActivity.getClass().getSimpleName();
        if (activityName.equalsIgnoreCase("ViewOrderActivity")){
            tvDecreaseQuantity.setVisibility(View.INVISIBLE);
            tvIncreaseQuantity.setVisibility(View.INVISIBLE);

        }else {
            tvDecreaseQuantity.setVisibility(View.VISIBLE);
            tvIncreaseQuantity.setVisibility(View.VISIBLE);


        }
        tvDecreaseQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentQuantity=Integer.parseInt(tvQuantity.getText().toString().trim());
                if (currentQuantity>0){
                    currentQuantity=currentQuantity-1;
                    insertUpdateItemIntoDb(currentQuantity,itemDetail,false);
                    OrderBasketDbModel  insertModel =new  OrderBasketDbModel(
                            itemDetail.itemId,
                            itemDetail.itemName,
                            itemDetail.itemImagePath,
                            itemDetail.servicesId,
                            itemDetail.servicesName,
                            itemDetail.servicesRate,
                            String.valueOf(currentQuantity),
                            itemDetail.servicesImagePath
                    );


                    mAdaptor.mServicesList.get(parentPosition).getItemData().set(childPosition,insertModel);
                    mAdaptor.notifyChildChanged(parentPosition, childPosition);
                    ConfirmOrderFragment.Companion.updateBottumData();
                }

            }
        });
        tvIncreaseQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentQuantity=Integer.parseInt(tvQuantity.getText().toString().trim());
                if (currentQuantity==0){
                    currentQuantity=currentQuantity+1;
                    insertUpdateItemIntoDb(currentQuantity,itemDetail,true);

                }else {
                    currentQuantity=currentQuantity+1;
                    insertUpdateItemIntoDb(currentQuantity,itemDetail,false);
                }
                OrderBasketDbModel  insertModel =new  OrderBasketDbModel(
                        itemDetail.itemId,
                        itemDetail.itemName,
                        itemDetail.itemImagePath,
                        itemDetail.servicesId,
                        itemDetail.servicesName,
                        itemDetail.servicesRate,
                        String.valueOf(currentQuantity),
                        itemDetail.servicesImagePath
                );

                mAdaptor.mServicesList.get(parentPosition).getItemData().set(childPosition,insertModel);
                mAdaptor.notifyChildChanged(parentPosition, childPosition);
                ConfirmOrderFragment.Companion.updateBottumData();
            }
        });

    }

    private void insertUpdateItemIntoDb(int currentQuantity,OrderBasketDbModel data,boolean isInsert) {
        try {

            if (currentQuantity>0 &&isInsert) {

                OrderBasketDbModel  insertModel =new  OrderBasketDbModel(
                        data.itemId,
                        data.itemName,
                        data.itemImagePath,
                        data.servicesId,
                        data.servicesName,
                        data.servicesRate,
                        String.valueOf(currentQuantity),
                        data.servicesImagePath
                );
                gethelper().getBucketDao().create(insertModel);

            } else if (currentQuantity==0) {
                DeleteBuilder deleteBuilder = gethelper().getBucketDao().deleteBuilder();

                Where where =deleteBuilder.where();
                where.eq("itemId", data.itemId);
                where.and();
                where.eq("servicesId", data.servicesId);
                deleteBuilder.prepare();

                deleteBuilder.delete();

            } else if (currentQuantity>0 && !isInsert) {
                UpdateBuilder updateBuilder = gethelper().getBucketDao().updateBuilder();





                updateBuilder.updateColumnValue("servicesItemQuantity" /* column */, String.valueOf(currentQuantity)/* value */);

                Where where =updateBuilder.where();
                where.eq("itemId", data.itemId);
                where.and();
                where.eq("servicesId", data.servicesId);
                updateBuilder.prepare();

                updateBuilder.update();


            }


        } catch (Exception e) {
            Log.v("dip", "insert or update error :" + e.getMessage());

        }
    }


    //For Database
    public OrmLiteDb gethelper() {
        if (ormLiteDb == null) {
            ormLiteDb = OpenHelperManager.getHelper(mContext, OrmLiteDb.class);
        }
        return ormLiteDb;
    }}
