package com.natrajinfotech.aboardlaundry.utils

import android.content.Context
import android.net.ConnectivityManager
import android.widget.Toast

class NetworkHandler (context: Context){
    var context: Context
    init {
        this.context=context
    }

    fun isNetworkAvailable(): Boolean {
            val manager: ConnectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val info = manager.activeNetworkInfo
            val isConnected = info != null && info.isConnected
            if (!isConnected) {
                //BaseActivity.showAlert(context,context.getString(R.string.NetworkError))
                Toast.makeText(context, "No Connection available!", Toast.LENGTH_SHORT).show()
            }
            return isConnected
        }

} 