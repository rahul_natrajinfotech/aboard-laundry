package com.natrajinfotech.aboardlaundry.RetroFitHelper

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import com.natrajinfotech.aboardlaundry.RetroFitHelper.Interface.ItemManagementInterface
import com.natrajinfotech.aboardlaundry.model.GetItemListResponseModel
import com.natrajinfotech.aboardlaundry.model.GetItemServiceRateResponseModel
import com.natrajinfotech.aboardlaundry.ui.SelectItemByCategoryActivity
import com.natrajinfotech.aboardlaundry.ui.SelectServicesActivity
import com.natrajinfotech.aboardlaundry.utils.AppProgressDialog
import com.natrajinfotech.aboardlaundry.utils.AppSharedPreference
import com.natrajinfotechindia.brainiton.RetroFitHelper.ApiClient
import com.natrajinfotechindia.brainiton.utils.AppAlerts
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Dipendra Sharma  on 03-01-2019.
 */
class ItemManagement (mContext: Context, mActivity: Activity, mProgressDialog: AppProgressDialog, requestDataMap: Map<String, String>){
    var mContext: Context
    var mActivity: Activity
    var mProgressDialog: AppProgressDialog
    private var requestDataMap:Map<String,String> = HashMap()
    private var requestInterface: ItemManagementInterface?= null

    init {
        this.mContext=mContext
        this.mActivity=mActivity
        this.mProgressDialog=mProgressDialog
        this.requestDataMap=requestDataMap
    }


    constructor(mContext: Context, mActivity: Activity, mProgressDialog: AppProgressDialog) : this(mContext, mActivity, mProgressDialog,HashMap())
//for getting item list
    fun gettingItemList(){
    requestInterface=ApiClient.getClient().create(ItemManagementInterface::class.java)

    val token=AppSharedPreference(mContext).getString("apiToken")
    val getItemCall=requestInterface!!.getItemData(token)
    getItemCall.enqueue(object :retrofit2.Callback<GetItemListResponseModel>{
        override fun onFailure(call: retrofit2.Call<GetItemListResponseModel>, t: Throwable) {
mProgressDialog.dialog.dismiss()
            Log.v("dip",t.message)
        }

        override fun onResponse(call: retrofit2.Call<GetItemListResponseModel>, response: Response<GetItemListResponseModel>) {
            mProgressDialog.dialog.dismiss()
            if (response.isSuccessful){
                val responceCode= response.body()!!.ResponseCode
                if (responceCode.equals("200")){
val itemList= response.body()!!.Data

                    mActivity.startActivity(Intent(mContext, SelectItemByCategoryActivity::class.java)
                        .putExtra("itemList",itemList)
                    )
                    mActivity.finish()

                }else{
                    AppAlerts().showAlertMessage(mContext,"Error", response.body()!!.ResponseMessage)

                }
            }else{
                AppAlerts().showAlertMessage(mContext,"Error",response.message())

            }
        }


    })

}
//for getting rate of par item service.
    fun getItemServiceRate(itemId:String,itemName:String,itemImage:String){
    requestInterface=ApiClient.getClient().create(ItemManagementInterface::class.java)
    val token=AppSharedPreference(mContext).getString("apiToken")
    val getRateCall=requestInterface!!.getItemServicesRate(token,itemId)
    getRateCall.enqueue(object :Callback<GetItemServiceRateResponseModel> {
        override fun onFailure(call: Call<GetItemServiceRateResponseModel>, t: Throwable) {
mProgressDialog.dialog.dismiss()
            AppAlerts().showAlertMessage(mContext,"Error", t.message!!)
        }

        override fun onResponse(
            call: Call<GetItemServiceRateResponseModel>,
            response: Response<GetItemServiceRateResponseModel>
        ) {
            mProgressDialog.dialog.dismiss()
            if (response.isSuccessful){
                val responceCode= response.body()!!.ResponseCode
                if (responceCode.equals("200")){
                    val rateList= response.body()!!.Data
                    mActivity.startActivity(Intent(mContext,SelectServicesActivity::class.java)
                        .putExtra("rateList",rateList)
                        .putExtra("itemImage",itemImage)
                        .putExtra("itemName",itemName)
                        .putExtra("itemId",itemId))
                    mActivity.finish()

                }else{
                    AppAlerts().showAlertMessage(mContext,"Error", response.body()!!.ResponseMessage)

                }

            }else{
                AppAlerts().showAlertMessage(mContext,"Error",response.message())

            }
        }


    })

}



}