package com.natrajinfotech.aboardlaundry.RetroFitHelper.Interface

import com.natrajinfotech.aboardlaundry.model.*
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by Dipendra Sharma  on 05-01-2019.
 */
interface OrderManagementInterface {
    //for getting charges by delivery type
    @GET("get_delivery_charges_by_delivery_type")
    fun getDeliveryCharges(@Query("token") token:String,@Query("delivery_type") delivery_type:String):Call<GetDeliveryChargesResponceModel>
    //for getting user latest order list
    @GET("get_user_order_data_app")
fun getLatestOrderList(@Query("token") token:String):Call<GetLatestOrderResponseModel>

    //for getting user all order history
    @GET("get_user_order_history_data_app")
fun getAllOrderHistory(@Query("token") token:String,@Query("page_no") page_no:Int):Call<GetOrderHistoryResponseModel>

//for confirm order
@Headers("Content-Type: application/json")
@POST("confirm_order")
fun doConfirmOrder(@Query("token") token:String, @Body requestData:HashMap<String,Any>):Call<ConfirmOrderResponseModel>
//for getting reason list for canceletion.
    @GET("cancel_reason_listing")
    fun getCancelReasonListing(@Query("token") token:String):Call<GetCancelReasonResponseModel>

    //for cancel order
    @Headers("Content-Type: application/json")
    @POST("cancel_user_order_data_by_id")
fun doCancelOrder(@Query("token") token:String,@Body requestData:HashMap<String,String>):Call<CommonResponceModel>

    //for getting order data by order id
    @GET("get_user_order_details_by_order_id")
    fun getOrderDataById(@Query ("order_id") order_id:String,@Query("token") token:String):Call<GetOrderDetailByIdResponseModel>

}