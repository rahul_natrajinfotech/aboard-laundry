package com.natrajinfotech.aboardlaundry.RetroFitHelper.Interface

import com.natrajinfotech.aboardlaundry.model.GetItemListResponseModel
import com.natrajinfotech.aboardlaundry.model.GetItemServiceRateResponseModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Dipendra Sharma  on 03-01-2019.
 */
interface ItemManagementInterface {
    //for getting item list for services
    @GET("get_items_data")
fun getItemData(@Query("token") token:String):retrofit2.Call<GetItemListResponseModel>
    //for getting item price list
    @GET("get_item_services_rates")
fun getItemServicesRate(@Query("token") token:String,@Query("item_id") itemId:String):Call<GetItemServiceRateResponseModel>


}