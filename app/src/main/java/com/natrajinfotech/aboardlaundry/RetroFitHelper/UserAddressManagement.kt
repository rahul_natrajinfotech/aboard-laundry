package com.natrajinfotech.aboardlaundry.RetroFitHelper

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.FragmentTransaction
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.TextView
import android.widget.Toast
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.RetroFitHelper.Interface.UserAddressManagementInterface
import com.natrajinfotech.aboardlaundry.adapter.RvSelectAddressAdaptor
import com.natrajinfotech.aboardlaundry.model.CommonResponceModel
import com.natrajinfotech.aboardlaundry.model.GetAreaListResponceModel
import com.natrajinfotech.aboardlaundry.model.GetUserAddressListResponceModel
import com.natrajinfotech.aboardlaundry.ui.SignInActivity
import com.natrajinfotech.aboardlaundry.ui.UserAddressListActivity
import com.natrajinfotech.aboardlaundry.ui.fragment.AddNewAddressFragment
import com.natrajinfotech.aboardlaundry.ui.fragment.ChangeAddAddressFragment
import com.natrajinfotech.aboardlaundry.utils.AppSharedPreference
import com.natrajinfotechindia.brainiton.RetroFitHelper.ApiClient
import com.natrajinfotechindia.brainiton.utils.AppAlerts
import com.natrajinfotech.aboardlaundry.utils.AppProgressDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Dipendra Sharma  on 05-01-2019.
 */
class UserAddressManagement(mContext: Context, mActivity: Activity, mProgressDialog: AppProgressDialog, requestDataMap: Map<String, String>){
    var mContext: Context
    var mActivity: Activity
    var mProgressDialog: AppProgressDialog
    private var requestDataMap:Map<String,String> = HashMap()
    private var requestInterface: UserAddressManagementInterface?= null

    init {
        this.mContext=mContext
        this.mActivity=mActivity
        this.mProgressDialog=mProgressDialog
        this.requestDataMap=requestDataMap
    }


    constructor(mContext: Context, mActivity: Activity, mProgressDialog: AppProgressDialog) : this(mContext, mActivity, mProgressDialog,HashMap())

    //for getting area list for add & edit address
    fun getAreaList(fragmentTransaction: FragmentTransaction,addressData:GetUserAddressListResponceModel.AddressListData) {
        requestInterface=ApiClient.getClient().create(UserAddressManagementInterface::class.java)

        val getAreaCall=requestInterface!!.getAreaList()
        getAreaCall.enqueue(object :Callback<GetAreaListResponceModel> {
            override fun onFailure(call: Call<GetAreaListResponceModel>, t: Throwable) {
                mProgressDialog.dialog.dismiss()
                AppAlerts().showAlertMessage(mContext,"Error", t.message!!)
            }

            override fun onResponse(call: Call<GetAreaListResponceModel>, response: Response<GetAreaListResponceModel>) {
                mProgressDialog.dialog.dismiss()
                if (response.isSuccessful){
                    val areaList= response.body()!!.Data
                    val dataBundle= Bundle()
                    dataBundle.putString("from","edit")
                    dataBundle.putSerializable("data",addressData)
                    dataBundle.putSerializable("area",areaList)
                    val editFragment= AddNewAddressFragment()
                    editFragment.arguments=dataBundle
                    fragmentTransaction.replace(R.id.flSchedulePickup, editFragment)
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()


                }else{
                    val errorMsgModel= ApiErrorParser().errorResponce(response)
                    AppAlerts().showAlertMessage(mContext,"Error",errorMsgModel.ResponseMessage)
                }
            }


        })
    }
    //for getting area list for add & edit address
    fun getAreaList(mIntent:Intent) {
        requestInterface=ApiClient.getClient().create(UserAddressManagementInterface::class.java)

        val getAreaCall=requestInterface!!.getAreaList()
        getAreaCall.enqueue(object :Callback<GetAreaListResponceModel> {
            override fun onFailure(call: Call<GetAreaListResponceModel>, t: Throwable) {
                mProgressDialog.dialog.dismiss()
                AppAlerts().showAlertMessage(mContext,"Error", t.message!!)
            }

            override fun onResponse(call: Call<GetAreaListResponceModel>, response: Response<GetAreaListResponceModel>) {
                mProgressDialog.dialog.dismiss()
                if (response.isSuccessful){
                    val areaList= response.body()!!.Data

                    mIntent.putExtra("from","add")

                    mIntent.putExtra("area",areaList)
mActivity.startActivity(mIntent)
                    mActivity.finish()

                }else{
                    val errorMsgModel= ApiErrorParser().errorResponce(response)
                    AppAlerts().showAlertMessage(mContext,"Error",errorMsgModel.ResponseMessage)
                }
            }


        })
    }

//for getting area list in case of add address from profile management
fun getAreaList(fragmentTransaction: FragmentTransaction) {
    requestInterface=ApiClient.getClient().create(UserAddressManagementInterface::class.java)

    val getAreaCall=requestInterface!!.getAreaList()
    getAreaCall.enqueue(object :Callback<GetAreaListResponceModel> {
        override fun onFailure(call: Call<GetAreaListResponceModel>, t: Throwable) {
            mProgressDialog.dialog.dismiss()
            AppAlerts().showAlertMessage(mContext,"Error", t.message!!)
        }

        override fun onResponse(call: Call<GetAreaListResponceModel>, response: Response<GetAreaListResponceModel>) {
            mProgressDialog.dialog.dismiss()
            if (response.isSuccessful){
                val areaList= response.body()!!.Data
                val dataBundle= Bundle()
                dataBundle.putString("from","add")

                dataBundle.putSerializable("area",areaList)
                val editFragment= AddNewAddressFragment()
                editFragment.arguments=dataBundle
                fragmentTransaction.replace(R.id.flSchedulePickup, editFragment)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()


            }else{
                val errorMsgModel= ApiErrorParser().errorResponce(response)
                AppAlerts().showAlertMessage(mContext,"Error",errorMsgModel.ResponseMessage)
            }
        }


    })
}


//for getting arealist in case of edit address from profile management
fun getAreaList(mIntent:Intent,addressData:GetUserAddressListResponceModel.AddressListData) {
    requestInterface=ApiClient.getClient().create(UserAddressManagementInterface::class.java)

    val getAreaCall=requestInterface!!.getAreaList()
    getAreaCall.enqueue(object :Callback<GetAreaListResponceModel> {
        override fun onFailure(call: Call<GetAreaListResponceModel>, t: Throwable) {
            mProgressDialog.dialog.dismiss()
            AppAlerts().showAlertMessage(mContext,"Error", t.message!!)
        }

        override fun onResponse(call: Call<GetAreaListResponceModel>, response: Response<GetAreaListResponceModel>) {
            mProgressDialog.dialog.dismiss()
            if (response.isSuccessful){
                val areaList= response.body()!!.Data
                Bundle()
                mIntent.putExtra("from","edit")
                mIntent.putExtra("data",addressData)
                mIntent.putExtra("area",areaList)
                mActivity.startActivity(mIntent)
                mActivity.finish()


            }else{
                val errorMsgModel= ApiErrorParser().errorResponce(response)
                AppAlerts().showAlertMessage(mContext,"Error",errorMsgModel.ResponseMessage)
            }
        }


    })
}

    //for getting user save address list
    fun getAddressList(
        rvSelectAddress: RecyclerView,
        mAdaptor: RvSelectAddressAdaptor,
        tvAddNew: TextView
    ) {
    requestInterface=ApiClient.getClient().create(UserAddressManagementInterface::class.java)
    val token= AppSharedPreference(mContext).getString("apiToken")
    val getAddressCall=requestInterface!!.getAddressList(token)
    getAddressCall.enqueue(object :Callback<GetUserAddressListResponceModel>{
        override fun onFailure(call: Call<GetUserAddressListResponceModel>, t: Throwable) {
mProgressDialog.dialog.dismiss()
            AppAlerts().showAlertMessage(mContext,"Error", t.message!!)
        }

        override fun onResponse(
            call: Call<GetUserAddressListResponceModel>,
            response: Response<GetUserAddressListResponceModel>
        ) {
           mProgressDialog.dialog.dismiss()
            if (response.isSuccessful){
                val responseCode= response.body()!!.ResponseCode
                if (responseCode.equals("200")){
                    val addressList= response.body()!!.Data
                    val isAddressSelected=AppSharedPreference(mContext).getBoolean("isAddressSelected")
                    if (isAddressSelected) {
val addressId=AppSharedPreference(mContext).getInt("addressId")
                        for (i in addressList!!.indices) {
if (addressList[i].id==addressId){
    val addressData=addressList[i]
    val updateData = GetUserAddressListResponceModel.AddressListData(
        addressData.id,
        addressData.name,
        addressData.building_no,
        "",
        addressData.block_no,
        addressData.street_no,
        addressData.area_name,
        addressData.phone_number,addressData.latitude,addressData.longitude,
        true
    )

    addressList.set(i,updateData)

}

                        }
                    }
                        rvSelectAddress.layoutManager=LinearLayoutManager(mContext)

                    mAdaptor.addressList= addressList!!

                    rvSelectAddress.adapter=mAdaptor
                    mAdaptor.notifyDataSetChanged()
                }else if (responseCode.equals("400")){
                    tvAddNew.performClick()

                }else if (responseCode.equals("404")){
                    logOutAfterSessionExpire()

                }

            }else{
                AppAlerts().showAlertMessage(mContext,"Error",response.message())

            }
        }


    })

}
    //for getting user save address list
    fun getAddressList(mIntent:Intent) {
        requestInterface=ApiClient.getClient().create(UserAddressManagementInterface::class.java)
        val token= AppSharedPreference(mContext).getString("apiToken")
        val getAddressCall=requestInterface!!.getAddressList(token)
        getAddressCall.enqueue(object :Callback<GetUserAddressListResponceModel>{
            override fun onFailure(call: Call<GetUserAddressListResponceModel>, t: Throwable) {
                mProgressDialog.dialog.dismiss()
                AppAlerts().showAlertMessage(mContext,"Error", t.message!!)
            }

            override fun onResponse(
                call: Call<GetUserAddressListResponceModel>,
                response: Response<GetUserAddressListResponceModel>
            ) {
                mProgressDialog.dialog.dismiss()
                if (response.isSuccessful){
                    val responseCode= response.body()!!.ResponseCode
                    if (responseCode.equals("200")){
                        val addressList= response.body()!!.Data

    mIntent.putExtra("isData","1")
    mIntent.putExtra("addressList",addressList)
                        mActivity.startActivity(mIntent)
                        mActivity.finish()
                    }else if (responseCode.equals("400")){


                                mIntent.putExtra("isData","0")
                        mActivity.startActivity(mIntent)
                        mActivity.finish()

                    }else if (responseCode.equals("404")){
                        logOutAfterSessionExpire()

                    }

                }else{
                    AppAlerts().showAlertMessage(mContext,"Error",response.message())

                }
            }


        })

    }
    //for save user new address
    fun addUpdateAddress(fragmentTransaction: FragmentTransaction) {


    requestInterface=ApiClient.getClient().create(UserAddressManagementInterface::class.java)
    val addAddressCall=requestInterface!!.addUpdateAddress(requestDataMap.get("token")!!,requestDataMap)
    addAddressCall.enqueue(object :Callback<CommonResponceModel> {
        override fun onFailure(call: Call<CommonResponceModel>, t: Throwable) {
mProgressDialog.dialog.dismiss()
            AppAlerts().showAlertMessage(mContext,"Error", t.message!!)
        }

        override fun onResponse(
            call: Call<CommonResponceModel>,
            response: Response<CommonResponceModel>
        ) {
            mProgressDialog.dialog.dismiss()
            if (response.isSuccessful){
                val responseCode= response.body()!!.ResponseCode
                if (responseCode.equals("200")){
                        //val addressList= response.body()!!.Data

                    fragmentTransaction.replace(R.id.flSchedulePickup, ChangeAddAddressFragment())
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()


                }else{
                    AppAlerts().showAlertMessage(mContext,"Error", response.body()!!.ResponseMessage)

                }

            }else{
                AppAlerts().showAlertMessage(mContext,"Error",response.message())

            }
        }


    })

}
//for add update address from account management
fun addUpdateAddress() {


    requestInterface=ApiClient.getClient().create(UserAddressManagementInterface::class.java)
    val addAddressCall=requestInterface!!.addUpdateAddress(requestDataMap.get("token")!!,requestDataMap)
    addAddressCall.enqueue(object :Callback<CommonResponceModel> {
        override fun onFailure(call: Call<CommonResponceModel>, t: Throwable) {
            mProgressDialog.dialog.dismiss()
            AppAlerts().showAlertMessage(mContext,"Error", t.message!!)
        }

        override fun onResponse(
            call: Call<CommonResponceModel>,
            response: Response<CommonResponceModel>
        ) {
            mProgressDialog.dialog.dismiss()
            if (response.isSuccessful){
                val responseCode= response.body()!!.ResponseCode
                if (responseCode.equals("200")){
                    //val addressList= response.body()!!.Data
Toast.makeText(mContext, response.body()!!.ResponseMessage,Toast.LENGTH_LONG).show()
                    getAddressList(Intent(mContext, UserAddressListActivity::class.java))


                }else{
                    AppAlerts().showAlertMessage(mContext,"Error", response.body()!!.ResponseMessage)

                }

            }else{
                AppAlerts().showAlertMessage(mContext,"Error",response.message())

            }
        }


    })

}

    //for delete user address by id
    fun deleteUserAddress(
    addressId: String,
    mAdaptor: RvSelectAddressAdaptor,
    selectedPosition: Int
){
    requestInterface=ApiClient.getClient().create(UserAddressManagementInterface::class.java)
    val token= AppSharedPreference(mContext).getString("apiToken")
val deleteCall=requestInterface!!.deleteUserAddress(token,addressId)
    deleteCall.enqueue(object :Callback<CommonResponceModel>{
        override fun onFailure(call: Call<CommonResponceModel>, t: Throwable) {
mProgressDialog.dialog.dismiss()
            AppAlerts().showAlertMessage(mContext,"Error", t.message!!)
        }

        override fun onResponse(call: Call<CommonResponceModel>, response: Response<CommonResponceModel>) {
            mProgressDialog.dialog.dismiss()
            if (response.isSuccessful){
                val responseCode= response.body()!!.ResponseCode
                if (responseCode.equals("200")){
Toast.makeText(mContext,"Address deleted successfully!",Toast.LENGTH_LONG).show()
                    mAdaptor.addressList.removeAt(selectedPosition)
                    mAdaptor.notifyDataSetChanged()

                }else if (responseCode.equals("404")){

                    logOutAfterSessionExpire()

                }else{
                    AppAlerts().showAlertMessage(mContext,"Error", response.body()!!.ResponseMessage)

                }

            }else{
                AppAlerts().showAlertMessage(mContext,"Error",response.message())

            }
        }


    })


}
    //for logout in case of token expire
    private fun logOutAfterSessionExpire() {
        AppAlerts().showAlertWithAction(mContext,"Info",mContext.resources.getString(R.string.sessionExpire),"Ok","",object :
            DialogInterface.OnClickListener{
            override fun onClick(p0: DialogInterface?, p1: Int) {
                AppSharedPreference(mContext).DeleteSharedPreference()
                mActivity.startActivity(Intent(mContext, SignInActivity::class.java))
                mActivity.finish()
            }


        },false)
    }
}