package com.natrajinfotechindia.brainiton.RetroFitHelper

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.natrajinfotech.aboardlaundry.BuildConfig
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by Dipendra Sharma  on 27-09-2018.
 */
class ApiClient {
    companion object {


        //val baseUrl: String = "http://metrolaundry.tecture.in/backend/api/"
        val baseUrl: String = "http://metrolaundry.online/backend/api/"


        fun getClient(): Retrofit {

            val mGson: Gson = GsonBuilder()
                    .setLenient()
                    .create()
            val mHttpLoginInterceptor = HttpLoggingInterceptor()
            //here we will set our log level
            mHttpLoginInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            val mOkClient: OkHttpClient.Builder = OkHttpClient.Builder().readTimeout(300,
                    TimeUnit.SECONDS).writeTimeout(300, TimeUnit.SECONDS).connectTimeout(300, TimeUnit.SECONDS)
// add logging as last interceptor always
            // we will add login interceptor only in case of debug.

            if (BuildConfig.DEBUG) {
                mOkClient.addInterceptor(mHttpLoginInterceptor)
            }
            val mRetrofit = Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(mOkClient.build())
                    .addConverterFactory(GsonConverterFactory.create(mGson))
                    .build()


            return mRetrofit
        }

        //this method will use in case of when user will go for authorize api
        fun getClient(apiToken: String): Retrofit {

            val mGson: Gson = GsonBuilder()
                    .setLenient()
                    .create()
            val mHttpLoginInterceptor = HttpLoggingInterceptor()
            //here we will set our log level
            mHttpLoginInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            val mOkClient: OkHttpClient.Builder = OkHttpClient.Builder().readTimeout(300,
                    TimeUnit.SECONDS).writeTimeout(300, TimeUnit.SECONDS).connectTimeout(300, TimeUnit.SECONDS)
// add logging as last interceptor always
            // we will add login interceptor only in case of debug.
//here firstly we will add our header interseptor
            mOkClient.addInterceptor(object : Interceptor {
                override fun intercept(chain: Interceptor.Chain): Response {
                    var newRequest: Request = chain.request().newBuilder()

                            .addHeader("token", apiToken)
                            .build()
                    return chain.proceed(newRequest)
                }


            })
            if (BuildConfig.DEBUG) {
                mOkClient.addInterceptor(mHttpLoginInterceptor)
            }
            var mRetrofit = Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(mOkClient.build())
                    .addConverterFactory(GsonConverterFactory.create(mGson))
                    .build()


            return mRetrofit
        }

        //this method will use for in case when header key & value will be dyanamic
        fun getClient(headerKey: String, headerValue: String): Retrofit {

            val mGson: Gson = GsonBuilder()
                    .setLenient()
                    .create()
            val mHttpLoginInterceptor = HttpLoggingInterceptor()
            //here we will set our log level
            mHttpLoginInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            val mOkClient: OkHttpClient.Builder = OkHttpClient.Builder().readTimeout(300,
                    TimeUnit.SECONDS).writeTimeout(300, TimeUnit.SECONDS).connectTimeout(300, TimeUnit.SECONDS)
// add logging as last interceptor always
            // we will add login interceptor only in case of debug.
//here firstly we will add our header interseptor
            mOkClient.addInterceptor(object : Interceptor {
                override fun intercept(chain: Interceptor.Chain): Response {
                    var newRequest: Request = chain.request().newBuilder()
                            .addHeader("Accept", "application/json")
                            .addHeader(headerKey, headerValue)
                            .build()
                    return chain.proceed(newRequest)
                }


            })
            if (BuildConfig.DEBUG) {
                mOkClient.addInterceptor(mHttpLoginInterceptor)
            }
            var mRetrofit = Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(mOkClient.build())
                    .addConverterFactory(GsonConverterFactory.create(mGson))
                    .build()


            return mRetrofit
        }
    }
}