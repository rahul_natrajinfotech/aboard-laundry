package com.natrajinfotech.aboardlaundry.RetroFitHelper.Interface

import com.natrajinfotech.aboardlaundry.model.*
import retrofit2.Call
import retrofit2.http.*


/**
 * Created by Dipendra Sharma  on 27-09-2018.
 */
interface UserManagementInterface {
//for getting area list for registration.
     @GET("get_areas")
fun getAreaList():Call<GetAreaListResponceModel>

//for do registration
     @Headers("Content-Type: application/json")
@POST("save_registration")
fun doRegistration(@Body requestData:Map<String,String>):Call<CommonResponceModel>

//for do login
     @Headers("Content-Type: application/json")
@POST("authenticate")
fun doLogin(@Body requestData:Map<String,String>):Call<DoLoginResponceModel>

     //for getting user data
     @GET("get_user_data_by_id_app")
fun getUserDetail(@Query("token") token:String):Call<GetUserDataResponseModel>

     //for updating user profile data
     @Headers("Content-Type: application/json")
     @POST("update_user_personal_details_app")
     fun doUpdateProfileData(@Query("token") token:String,@Body requestData:Map<String,String>):Call<CommonResponceModel>

     //for forgot password
     @Headers("Content-Type: application/json")
     @POST("send_forgot_password")
     fun sendForgotPassword(@Body requestData:Map<String,String>):Call<CommonResponceModel>
     //for gettting list of notification of user
     @GET("notification_listing")
     fun getNotificationList(@Query("token") token:String,@Query("page_no") page_no:Int):Call<GetNotificationResponseModel>

     //for delete notification by notification id
     @GET("delete_notification_by_id")
     fun deleteNotification(@Query ("token") token:String,@Query("id") id:String):Call<CommonResponceModel>

    //for getting company detail
     @GET("get_company_details")
     fun getCompanyDetail():Call<GetCompanyDetailResponseModel>
     }