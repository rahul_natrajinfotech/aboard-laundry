package com.natrajinfotech.aboardlaundry.RetroFitHelper

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.widget.Toast
import com.j256.ormlite.android.apptools.OpenHelperManager
import com.j256.ormlite.table.TableUtils
import com.natrajinfotech.aboardlaundry.Database.OrmLiteDb
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.RetroFitHelper.Interface.UserManagementInterface
import com.natrajinfotech.aboardlaundry.model.*
import com.natrajinfotech.aboardlaundry.ui.AccountSettingActivity
import com.natrajinfotech.aboardlaundry.ui.NotificationActivity
import com.natrajinfotech.aboardlaundry.ui.SignInActivity
import com.natrajinfotech.aboardlaundry.ui.SignupActivity
import com.natrajinfotech.aboardlaundry.utils.AppProgressDialog
import com.natrajinfotech.aboardlaundry.utils.AppSharedPreference
import com.natrajinfotechindia.brainiton.RetroFitHelper.ApiClient
import com.natrajinfotechindia.brainiton.utils.AppAlerts
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Dipendra Sharma  on 27-09-2018.
 */
class UserManagement(
    mContext: Context,
    mActivity: Activity,
    mProgressDialog: AppProgressDialog,
    requestDataMap: Map<String, String>
) {
    var mContext: Context
    var mActivity: Activity
    var mProgressDialog: AppProgressDialog
    private var requestDataMap: Map<String, String> = HashMap()
    private var requestInterface: UserManagementInterface? = null

    init {
        this.mContext = mContext
        this.mActivity = mActivity
        this.mProgressDialog = mProgressDialog
        this.requestDataMap = requestDataMap
    }


    constructor(mContext: Context, mActivity: Activity, mProgressDialog: AppProgressDialog) : this(
        mContext,
        mActivity,
        mProgressDialog,
        HashMap()
    )

    //for getting area list for registration
    fun getAreaList() {
        requestInterface = ApiClient.getClient().create(UserManagementInterface::class.java)

        val getAreaCall = requestInterface!!.getAreaList()
        getAreaCall.enqueue(object : Callback<GetAreaListResponceModel> {
            override fun onFailure(call: Call<GetAreaListResponceModel>, t: Throwable) {
                mProgressDialog.dialog.dismiss()
                AppAlerts().showAlertMessage(mContext, "Error", t.message!!)
            }

            override fun onResponse(
                call: Call<GetAreaListResponceModel>,
                response: Response<GetAreaListResponceModel>
            ) {
                mProgressDialog.dialog.dismiss()
                if (response.isSuccessful) {
                    val areaList = response.body()!!.Data
                    mActivity.startActivity(
                        Intent(mContext, SignupActivity::class.java)
                            .putExtra("areaList", areaList)
                    )
                    mActivity.finish()

                } else {
                    val errorMsgModel = ApiErrorParser().errorResponce(response)
                    AppAlerts().showAlertMessage(mContext, "Error", errorMsgModel.ResponseMessage)
                }
            }


        })
    }

    //for doing new registration
    fun doRegistration() {
        requestInterface = ApiClient.getClient().create(UserManagementInterface::class.java)
        val registerCall = requestInterface!!.doRegistration(requestDataMap)
        registerCall.enqueue(object : Callback<CommonResponceModel> {
            override fun onFailure(call: Call<CommonResponceModel>, t: Throwable) {
                mProgressDialog.dialog.dismiss()
                AppAlerts().showAlertMessage(mContext, "Error", t.message!!)
            }

            override fun onResponse(call: Call<CommonResponceModel>, response: Response<CommonResponceModel>) {
                mProgressDialog.dialog.dismiss()
                if (response.isSuccessful) {
                    AppAlerts().showAlertWithAction(mContext, "Info", response.body()!!.ResponseMessage, "Ok", "",
                        DialogInterface.OnClickListener { _, _ ->
                            mActivity.startActivity(Intent(mContext, SignInActivity::class.java))
                            mActivity.finish()
                        }, false
                    )

                } else {
                    val errorMsgModel = ApiErrorParser().errorResponce(response)
                    AppAlerts().showAlertMessage(mContext, "Error", errorMsgModel.ResponseMessage)

                }
            }


        })
    }

    //for do login
    fun doLogin() {
        requestInterface = ApiClient.getClient().create(UserManagementInterface::class.java)
        val loginCall = requestInterface!!.doLogin(requestDataMap)
        loginCall.enqueue(object : Callback<DoLoginResponceModel> {
            override fun onFailure(call: Call<DoLoginResponceModel>, t: Throwable) {
                mProgressDialog.dialog.dismiss()
                AppAlerts().showAlertMessage(mContext, "Error", t.message!!)
            }

            override fun onResponse(call: Call<DoLoginResponceModel>, response: Response<DoLoginResponceModel>) {

                if (response.isSuccessful) {
                    val responceCode = response.body()!!.ResponseCode
                    if (responceCode == "200") {
                        val apiToken = response.body()!!.Data!!.token
                        AppSharedPreference(mContext).saveString("apiToken", apiToken)
                        AppSharedPreference(mContext).saveBoolean("isLogin", true)
                        TableUtils.clearTable(gethelper().connectionSource, OrderBasketDbModel::class.java)
                        getCompanyDetail()
                    } else {
                        mProgressDialog.dialog.dismiss()
                        AppAlerts().showAlertMessage(mContext, "Error", response.body()!!.ResponseMessage)

                    }
                } else {
                    mProgressDialog.dialog.dismiss()
                    val errorMsgModel = ApiErrorParser().errorResponce(response)
                    AppAlerts().showAlertMessage(mContext, "Error", errorMsgModel.ResponseMessage)

                }
            }


        })

    }

    //for getting user data
    fun getUserData() {
        requestInterface = ApiClient.getClient().create(UserManagementInterface::class.java)
        val token = AppSharedPreference(mContext).getString("apiToken")
        val getUserData = requestInterface!!.getUserDetail(token)
        getUserData.enqueue(object : Callback<GetUserDataResponseModel> {
            override fun onFailure(call: Call<GetUserDataResponseModel>, t: Throwable) {
                mProgressDialog.dialog.dismiss()
                AppAlerts().showAlertMessage(mContext, "Error", t.message!!)
            }

            override fun onResponse(
                call: Call<GetUserDataResponseModel>,
                response: Response<GetUserDataResponseModel>
            ) {
                mProgressDialog.dialog.dismiss()
                if (response.isSuccessful) {
                    val responseCode = response.body()!!.ResponseCode
                    when {
                        responseCode == "200" -> {
                            val userData = response.body()!!.Data


                            mActivity.startActivity(
                                Intent(mContext, AccountSettingActivity::class.java)
                                    .putExtra("userData", userData)
                            )
                            mActivity.finish()


                        }
                        responseCode == "400" -> Toast.makeText(mContext, response.body()!!.ResponseMessage, Toast.LENGTH_LONG).show()
                        responseCode == "404" -> logOutAfterSessionExpire()
                    }

                } else {
                    AppAlerts().showAlertMessage(mContext, "Error", response.message())

                }

            }


        })

    }

    //for updating user profile data
    fun doUpdateProfileData() {
        requestInterface = ApiClient.getClient().create(UserManagementInterface::class.java)
        val token = AppSharedPreference(mContext).getString("apiToken")
        val updateCall = requestInterface!!.doUpdateProfileData(token, requestDataMap)
        updateCall.enqueue(object : Callback<CommonResponceModel> {
            override fun onFailure(call: Call<CommonResponceModel>, t: Throwable) {
                mProgressDialog.dialog.dismiss()
                AppAlerts().showAlertMessage(mContext, "Error", t.message!!)
            }

            override fun onResponse(call: Call<CommonResponceModel>, response: Response<CommonResponceModel>) {


                if (response.isSuccessful) {
                    val responseCode = response.body()!!.ResponseCode
                    if (responseCode == "200") {
                        mProgressDialog.dialog.dismiss()
                        Toast.makeText(mContext, response.body()!!.ResponseMessage, Toast.LENGTH_LONG).show()
                        if (requestDataMap["type"].equals("password")) {
                            AppSharedPreference(mContext).DeleteSharedPreference()
                            mActivity.startActivity(Intent(mContext, SignInActivity::class.java))
                            mActivity.finish()

                        }
                        //OrderManagement(mContext,mActivity,mProgressDialog).getLatestOrder()


                    } else if (responseCode == "400") {
                        mProgressDialog.dialog.dismiss()
                        Toast.makeText(mContext, response.body()!!.ResponseMessage, Toast.LENGTH_LONG).show()

                    } else if (responseCode == "404") {
                        mProgressDialog.dialog.dismiss()
                        logOutAfterSessionExpire()

                    }

                } else {
                    mProgressDialog.dialog.dismiss()
                    AppAlerts().showAlertMessage(mContext, "Error", response.message())

                }
            }


        })


    }

    //for forgot user password
    fun sendForgotPassword() {
        requestInterface = ApiClient.getClient().create(UserManagementInterface::class.java)

        val forgotCall = requestInterface!!.sendForgotPassword(requestDataMap)
        forgotCall.enqueue(object : Callback<CommonResponceModel> {
            override fun onFailure(call: Call<CommonResponceModel>, t: Throwable) {
                mProgressDialog.dialog.dismiss()
                AppAlerts().showAlertMessage(mContext, "Error", t.message!!)
            }

            override fun onResponse(call: Call<CommonResponceModel>, response: Response<CommonResponceModel>) {
                mProgressDialog.dialog.dismiss()
                if (response.isSuccessful) {
                    AppAlerts().showAlertMessage(mContext, "Info", response.body()!!.ResponseMessage)

                } else {

                    AppAlerts().showAlertMessage(mContext, "Error", response.message())

                }
            }


        })


    }

    //For Database
    private fun gethelper(): OrmLiteDb {

        var ormLiteDb: OrmLiteDb? = null
        if (ormLiteDb == null) {
            ormLiteDb =
                    OpenHelperManager.getHelper(mContext, OrmLiteDb::class.java)
        }
        return ormLiteDb!!
    }

    //for gettting notification list of userL̥
    fun getNotificationList() {
        requestInterface = ApiClient.getClient().create(UserManagementInterface::class.java)
        val token = AppSharedPreference(mContext).getString("apiToken")
        val getNotificationCall = requestInterface!!.getNotificationList(token,1)
        getNotificationCall.enqueue(object : Callback<GetNotificationResponseModel> {
            override fun onFailure(call: Call<GetNotificationResponseModel>, t: Throwable) {
                mProgressDialog.dialog.dismiss()
                AppAlerts().showAlertMessage(mContext, "Error", t.message!!)
            }

            override fun onResponse(
                call: Call<GetNotificationResponseModel>,
                response: Response<GetNotificationResponseModel>
            ) {
                mProgressDialog.dialog.dismiss()
                if (response.isSuccessful) {
                    val responseCode = response.body()!!.ResponseCode
                    when {
                        responseCode == "200" -> {
                            val notificationList = response.body()!!.Data


                            mActivity.startActivity(
                                Intent(mContext, NotificationActivity::class.java)
                                    .putExtra("notificationList", notificationList)
                            )
                            mActivity.finish()


                        }
                        responseCode == "400" -> Toast.makeText(mContext, response.body()!!.ResponseMessage, Toast.LENGTH_LONG).show()
                        responseCode == "404" -> logOutAfterSessionExpire()
                    }

                } else {
                    AppAlerts().showAlertMessage(mContext, "Error", response.message())

                }


            }


        })

    }

    //for deleting notification by id
    fun deleteNotification(notificationId: String) {
        requestInterface = ApiClient.getClient().create(UserManagementInterface::class.java)
        val token = AppSharedPreference(mContext).getString("apiToken")
        val deleteNotificationCall = requestInterface!!.deleteNotification(token, notificationId)
        deleteNotificationCall.enqueue(object : Callback<CommonResponceModel> {
            override fun onFailure(call: Call<CommonResponceModel>, t: Throwable) {
                mProgressDialog.dialog.dismiss()
                AppAlerts().showAlertMessage(mContext, "Error", t.message!!)
            }

            override fun onResponse(call: Call<CommonResponceModel>, response: Response<CommonResponceModel>) {
                mProgressDialog.dialog.dismiss()
                if (response.isSuccessful) {
                    val responseCode = response.body()!!.ResponseCode
                    when {
                        responseCode == "200" -> {
                            //here we have to write code for next process.


                        }
                        responseCode == "400" -> Toast.makeText(mContext, response.body()!!.ResponseMessage, Toast.LENGTH_LONG).show()
                        responseCode == "404" -> logOutAfterSessionExpire()
                    }

                } else {
                    AppAlerts().showAlertMessage(mContext, "Error", response.message())

                }

            }


        })

    }

    //for getting company detail
    fun getCompanyDetail(){
        requestInterface=ApiClient.getClient().create(UserManagementInterface::class.java)
        val getCompanyCall=requestInterface!!.getCompanyDetail()
        getCompanyCall.enqueue(object :Callback<GetCompanyDetailResponseModel>{
            override fun onFailure(call: Call<GetCompanyDetailResponseModel>, t: Throwable) {
mProgressDialog.dialog.dismiss()
                AppAlerts().showAlertMessage(mContext,"Error", t.message!!)
            }

            override fun onResponse(
                call: Call<GetCompanyDetailResponseModel>,
                response: Response<GetCompanyDetailResponseModel>
            ) {
                if (response.isSuccessful) {
                    val responseCode = response.body()!!.ResponseCode
                    when {
                        responseCode == "200" -> {
val companyData= response.body()!!.Data
                            val saveValue=AppSharedPreference(mContext)
                            saveValue.saveString("aboutUs", companyData!!.aboutus_uel)
                            saveValue.saveString("contactUs",companyData.contactus_url)
                            saveValue.saveString("termsCondition",companyData.term_condition_url)
                            saveValue.saveString("tollFree",companyData.toll_free_no)
                            saveValue.saveString("phoneNumber",companyData.phone_no)
                            saveValue.saveString("companyEmail",companyData.email)
                            OrderManagement(mContext, mActivity, mProgressDialog).getLatestOrder()


                        }
                        responseCode == "400" -> Toast.makeText(mContext, response.body()!!.ResponseMessage, Toast.LENGTH_LONG).show()
                        responseCode == "404" -> logOutAfterSessionExpire()
                    }

                } else {
                    AppAlerts().showAlertMessage(mContext, "Error", response.message())

                }

            }


        })

    }

    //for logout in case of token expire
    private fun logOutAfterSessionExpire() {
        AppAlerts().showAlertWithAction(
            mContext,
            "Info",
            mContext.resources.getString(R.string.sessionExpire),
            "Ok",
            "",
            DialogInterface.OnClickListener { _, _ ->
                AppSharedPreference(mContext).DeleteSharedPreference()
                mActivity.startActivity(Intent(mContext, SignInActivity::class.java))
                mActivity.finish()
            },
            false
        )
    }


}