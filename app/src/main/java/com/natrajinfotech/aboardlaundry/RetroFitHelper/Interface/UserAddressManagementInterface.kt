package com.natrajinfotech.aboardlaundry.RetroFitHelper.Interface

import com.natrajinfotech.aboardlaundry.model.CommonResponceModel
import com.natrajinfotech.aboardlaundry.model.GetAreaListResponceModel
import com.natrajinfotech.aboardlaundry.model.GetUserAddressListResponceModel
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by Dipendra Sharma  on 05-01-2019.
 */
interface UserAddressManagementInterface {
    //for getting area list for address.
    @GET("get_areas")
    fun getAreaList():Call<GetAreaListResponceModel>

    //for getting user all save address liss
    @GET("user_address_details_app")
fun getAddressList(@Query("token") token:String):Call<GetUserAddressListResponceModel>

    //for adding user new address
    @Headers("Content-Type: application/json")
    @POST("add_user_address_app")
fun addUpdateAddress(@Query("token") token:String, @Body requestData:Map<String,String>):Call<CommonResponceModel>

    //For delete user address by id
    @GET("delete_user_address_by_id")
fun deleteUserAddress(@Query ("token") token:String, @Query("address_id") address_id:String):Call<CommonResponceModel>

}