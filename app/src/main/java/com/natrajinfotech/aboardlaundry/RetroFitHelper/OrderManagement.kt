package com.natrajinfotech.aboardlaundry.RetroFitHelper

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.widget.Toast
import com.j256.ormlite.android.apptools.OpenHelperManager
import com.j256.ormlite.table.TableUtils
import com.natrajinfotech.aboardlaundry.Database.OrmLiteDb
import com.natrajinfotech.aboardlaundry.R
import com.natrajinfotech.aboardlaundry.RetroFitHelper.Interface.OrderManagementInterface
import com.natrajinfotech.aboardlaundry.model.*
import com.natrajinfotech.aboardlaundry.ui.*
import com.natrajinfotech.aboardlaundry.utils.AppProgressDialog
import com.natrajinfotech.aboardlaundry.utils.AppSharedPreference
import com.natrajinfotechindia.brainiton.RetroFitHelper.ApiClient
import com.natrajinfotechindia.brainiton.utils.AppAlerts
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Dipendra Sharma  on 05-01-2019.
 */
class OrderManagement (mContext: Context, mActivity: Activity, mProgressDialog: AppProgressDialog, requestDataMap: Map<String, String>){
    var mContext: Context
    var mActivity: Activity
    var mProgressDialog: AppProgressDialog
    private var requestDataMap:Map<String,String> = HashMap()
    private var requestInterface: OrderManagementInterface?= null

    init {
        this.mContext=mContext
        this.mActivity=mActivity
        this.mProgressDialog=mProgressDialog
        this.requestDataMap=requestDataMap
    }


    constructor(mContext: Context, mActivity: Activity, mProgressDialog: AppProgressDialog) : this(mContext, mActivity, mProgressDialog,HashMap())
    //for getting charges of Delivery type
    fun getDeliveryCharges(deliveryType:String){
        requestInterface=ApiClient.getClient().create(OrderManagementInterface::class.java)
        val token= AppSharedPreference(mContext).getString("apiToken")
val getChargesCall=requestInterface!!.getDeliveryCharges(token,deliveryType)
        getChargesCall.enqueue(object :Callback<GetDeliveryChargesResponceModel> {
            override fun onFailure(call: Call<GetDeliveryChargesResponceModel>, t: Throwable) {
mProgressDialog.dialog.dismiss()
                AppAlerts().showAlertMessage(mContext,"Error", t.message!!)
            }

            override fun onResponse(
                call: Call<GetDeliveryChargesResponceModel>,
                response: Response<GetDeliveryChargesResponceModel>
            ) {
                mProgressDialog.dialog.dismiss()
                if (response.isSuccessful){
                    val responseCode= response.body()!!.ResponseCode
                    if (responseCode.equals("200")){
                        val saveValue=AppSharedPreference(mContext)
                        val chargesData= response.body()!!.Data
                        saveValue.saveString("deliveryType", chargesData!!.delivery_type)
                        saveValue.saveString("deliveryId",chargesData.id.toString())
                        saveValue.saveString("deliveryCharges",chargesData.charges.toString())
                        UserAddressManagement(mContext,mActivity,mProgressDialog).getAddressList(Intent(mContext,ScheduleMainActivity::class.java))

                    }else if (responseCode.equals("404")) {
logOutAfterSessionExpire()
                    }else{
AppAlerts().showAlertMessage(mContext,"Error", response.body()!!.ResponseMessage)

                    }

                }else{
                    AppAlerts().showAlertMessage(mContext,"Error",response.message())

                }
            }


        })

    }

    //for getting Latest order list
    fun getLatestOrder(){
requestInterface=ApiClient.getClient().create(OrderManagementInterface::class.java)
        val token= AppSharedPreference(mContext).getString("apiToken")
        val getLatestCall=requestInterface!!.getLatestOrderList(token)
       getLatestCall.enqueue(object :Callback<GetLatestOrderResponseModel>{
           override fun onFailure(call: Call<GetLatestOrderResponseModel>, t: Throwable) {
mProgressDialog.dialog.dismiss()
               AppAlerts().showAlertMessage(mContext,"Error", t.message!!)
           }

           override fun onResponse(
               call: Call<GetLatestOrderResponseModel>,
               response: Response<GetLatestOrderResponseModel>
           ) {
               mProgressDialog.dialog.dismiss()
               if (response.isSuccessful){
                   val responseCode= response.body()!!.ResponseCode
                   if (responseCode.equals("200")){
                       val latestList: ArrayList<GetLatestOrderResponseModel.LatestOrderData>
                       latestList= response.body()!!.Data!!
val saveValue=AppSharedPreference(mContext)
                       saveValue.saveString("userName", response.body()!!.name)
                       saveValue.saveString("userEmail", response.body()!!.email)
                       mActivity.startActivity(Intent(mContext,HomeActivity::class.java)
    .putExtra("latestData",latestList))
mActivity.finish()
                   }else if (responseCode.equals("400")){
                       //AppAlerts().showAlertMessage(mContext,"Error", response.body()!!.ResponseMessage)
val latestList=ArrayList<GetLatestOrderResponseModel.LatestOrderData>()
                       mActivity.startActivity(Intent(mContext,HomeActivity::class.java)
                           .putExtra("latestData",latestList))
                       mActivity.finish()

                   }else {
                       logOutAfterSessionExpire()

                   }

               }else{
if (response.code()==400){
 logOutAfterSessionExpire()

}else {
    AppAlerts().showAlertMessage(mContext, "Error", response.message())
}
               }
           }


       })

    }

    //for getting all order History
    fun getOrderHistory(){
    requestInterface=ApiClient.getClient().create(OrderManagementInterface::class.java)
    val token= AppSharedPreference(mContext).getString("apiToken")
    val getOrderCall=requestInterface!!.getAllOrderHistory(token,1)
    getOrderCall.enqueue(object :Callback<GetOrderHistoryResponseModel> {
        override fun onFailure(call: Call<GetOrderHistoryResponseModel>, t: Throwable) {
            mProgressDialog.dialog.dismiss()
            AppAlerts().showAlertMessage(mContext,"Error", t.message!!)
        }

        override fun onResponse(
            call: Call<GetOrderHistoryResponseModel>,
            response: Response<GetOrderHistoryResponseModel>
        ) {
            mProgressDialog.dialog.dismiss()
            if (response.isSuccessful){
                val responseCode= response.body()!!.ResponseCode
                if (responseCode.equals("200")){
                    val orderList= response.body()!!.Data
mActivity.startActivity(Intent(mContext,MyOrdersActivity::class.java)
    .putExtra("orderList",orderList)
    .putExtra("totalPages", response.body()!!.total_page))
                    mActivity.finish()


                }else if (responseCode.equals("400")){
                    Toast.makeText(mContext, response.body()!!.ResponseMessage,Toast.LENGTH_LONG).show()

                }else if (responseCode.equals("404")){
                    logOutAfterSessionExpire()

                }

            }else{
                AppAlerts().showAlertMessage(mContext,"Error",response.message())

            }
        }


    })

}
//for confirm new order
    fun doConfirmOrder(mRequestData:HashMap<String,Any>){
    requestInterface=ApiClient.getClient().create(OrderManagementInterface::class.java)
    val token= AppSharedPreference(mContext).getString("apiToken")
    val doConfirmCall=requestInterface!!.doConfirmOrder(token,mRequestData)
    doConfirmCall.enqueue(object :Callback<ConfirmOrderResponseModel>{
        override fun onFailure(call: Call<ConfirmOrderResponseModel>, t: Throwable) {
mProgressDialog.dialog.dismiss()
            AppAlerts().showAlertMessage(mContext,"Error", t.message!!)
        }

        override fun onResponse(call: Call<ConfirmOrderResponseModel>, response: Response<ConfirmOrderResponseModel>) {
            mProgressDialog.dialog.dismiss()
            if (response.isSuccessful){
                val responseCode= response.body()!!.ResponseCode
                if (responseCode.equals("200")){
                    val orderData= response.body()!!.Data
//here we will clear all data of previous order
                    val clearValue = AppSharedPreference(mContext)
                    clearValue.saveString("deliveryType", "")
                    clearValue.saveString("deliveryId", "")
                    clearValue.saveString("deliveryCharges", "")
                    clearValue.saveString("pickupDate", "")
                    clearValue.saveString("pickupTime", "")
                    clearValue.saveString("deliveryDate", "")
                    clearValue.saveString("deliveryTime", "")
                    clearValue.saveString("paymentMode", "")

//here we will check the payment method
                    //if payment method will be Knet then we will open webview activity.
                    val paymentMode:String= mRequestData.get("payment_mode") as String

                    if (paymentMode.equals("KNET")){
                     val orderId:String= orderData!!.order_id.toString()
                        val paymentUrl="https://metrolaundry.online/knet/details.php?order_id="+orderId.trim()
                        mActivity.startActivity(Intent(mContext,KnetPaymentWebViewActivity::class.java)
                            .putExtra
                        ("webUrl",paymentUrl))
                        mActivity.finish()
                    }else {
                        TableUtils.clearTable(gethelper().connectionSource, OrderBasketDbModel::class.java)

                        mActivity.startActivity(
                            Intent(mContext, OrderConfirmationActivity::class.java)
                                .putExtra("orderData", orderData)
                        )
                        mActivity.finish()
                    }

                }else if (responseCode.equals("400")){
                    Toast.makeText(mContext, response.body()!!.ResponseMessage,Toast.LENGTH_LONG).show()

                }else if (responseCode.equals("404")){
                    logOutAfterSessionExpire()

                }

            }else{
                AppAlerts().showAlertMessage(mContext,"Error",response.message())

            }
        }


    })

}
//for getting list of cancel reasons.
    fun getCancelReasonListing(orderId:String){
    requestInterface=ApiClient.getClient().create(OrderManagementInterface::class.java)
    val token= AppSharedPreference(mContext).getString("apiToken")
val getReasonCall=requestInterface!!.getCancelReasonListing(token)
    getReasonCall.enqueue(object :Callback<GetCancelReasonResponseModel> {
        override fun onFailure(call: Call<GetCancelReasonResponseModel>, t: Throwable) {
mProgressDialog.dialog.dismiss()
            AppAlerts().showAlertMessage(mContext,"Error", t.message!!)
        }

        override fun onResponse(
            call: Call<GetCancelReasonResponseModel>,
            response: Response<GetCancelReasonResponseModel>
        ) {
            mProgressDialog.dialog.dismiss()
            if (response.isSuccessful){
                val responseCode= response.body()!!.ResponseCode
                if (responseCode.equals("200")){
val reasonList= response.body()!!.Data
                    mActivity.startActivity(Intent(mContext,CancelOrderActivity::class.java)
                        .putExtra("reasonList",reasonList)
                        .putExtra("orderId",orderId))
                    mActivity.finish()



                }else if (responseCode.equals("400")){
                    mProgressDialog.dialog.dismiss()
                    Toast.makeText(mContext, response.body()!!.ResponseMessage,Toast.LENGTH_LONG).show()

                }else if (responseCode.equals("404")){
                    mProgressDialog.dialog.dismiss()
                    logOutAfterSessionExpire()

                }

            }else{

                AppAlerts().showAlertMessage(mContext,"Error",response.message())

            }

        }


    })

}

    //for cancel order
    fun doCancelOrder(mRequestData: HashMap<String, String>) {
    requestInterface=ApiClient.getClient().create(OrderManagementInterface::class.java)
    val token= AppSharedPreference(mContext).getString("apiToken")
val cancelCall=requestInterface!!.doCancelOrder(token,mRequestData)
    cancelCall.enqueue(object :Callback<CommonResponceModel>{
        override fun onFailure(call: Call<CommonResponceModel>, t: Throwable) {
mProgressDialog.dialog.dismiss()
            AppAlerts().showAlertMessage(mContext,"Error", t.message!!)
        }

        override fun onResponse(call: Call<CommonResponceModel>, response: Response<CommonResponceModel>) {

            if (response.isSuccessful){
                val responseCode= response.body()!!.ResponseCode
                if (responseCode.equals("200")){
                    Toast.makeText(mContext, response.body()!!.ResponseMessage,Toast.LENGTH_LONG).show()
getOrderHistory()


                }else if (responseCode.equals("400")){
                    mProgressDialog.dialog.dismiss()
                    Toast.makeText(mContext, response.body()!!.ResponseMessage,Toast.LENGTH_LONG).show()

                }else if (responseCode.equals("404")){
                    mProgressDialog.dialog.dismiss()
                    logOutAfterSessionExpire()

                }

            }else{
                mProgressDialog.dialog.dismiss()
                AppAlerts().showAlertMessage(mContext,"Error",response.message())

            }

        }


    })


}
//for getting order detail by ordr id
    fun getOrderDetailById(orderId:String){
    requestInterface=ApiClient.getClient().create(OrderManagementInterface::class.java)
    val token= AppSharedPreference(mContext).getString("apiToken")
    val getOrderDetailCall=requestInterface!!.getOrderDataById(orderId,token)
    getOrderDetailCall.enqueue(object :Callback<GetOrderDetailByIdResponseModel>{
        override fun onFailure(call: Call<GetOrderDetailByIdResponseModel>, t: Throwable) {
mProgressDialog.dialog.dismiss()
            AppAlerts().showAlertMessage(mContext,"Error", t.message!!)
        }

        override fun onResponse(
            call: Call<GetOrderDetailByIdResponseModel>,
            response: Response<GetOrderDetailByIdResponseModel>
        ) {
            mProgressDialog.dialog.dismiss()
            if (response.isSuccessful){
                val responseCode= response.body()!!.ResponseCode
                if (responseCode.equals("200")){
val orderData= response.body()!!.Data
                    //here we will intent order detail activity with above data
mActivity.startActivity(Intent(mContext,ViewOrderActivity::class.java)
    .putExtra("orderData",orderData))
                    mActivity.finish()



                }else if (responseCode.equals("400")){

                    Toast.makeText(mContext, response.body()!!.ResponseMessage,Toast.LENGTH_LONG).show()

                }else if (responseCode.equals("404")){

                    logOutAfterSessionExpire()

                }

            }else{

                AppAlerts().showAlertMessage(mContext,"Error",response.message())

            }

        }


    })

}
    //for logout in case of token expire
private fun logOutAfterSessionExpire() {
    AppAlerts().showAlertWithAction(mContext,"Info",mContext.resources.getString(R.string.sessionExpire),"Ok","",object :DialogInterface.OnClickListener{
        override fun onClick(p0: DialogInterface?, p1: Int) {
            AppSharedPreference(mContext).DeleteSharedPreference()
            mActivity.startActivity(Intent(mContext,SignInActivity::class.java))
            mActivity.finish()
        }


    },false)
}



    //For Database
    private fun gethelper(): OrmLiteDb {

        var ormLiteDb: OrmLiteDb? = null
        if (ormLiteDb == null) {
            ormLiteDb =
                    OpenHelperManager.getHelper(mContext, OrmLiteDb::class.java)
        }
        return ormLiteDb!!
    }



}