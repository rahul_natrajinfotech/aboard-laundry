package com.natrajinfotech.aboardlaundry.model

import java.io.Serializable

/**
 * Created by Dipendra Sharma  on 08-01-2019.
 */
class OrderBasketListModel(serviceId:String,serviceName:String,serviceIconPath:String,itemData:List<OrderBasketDbModel>):Parent<OrderBasketDbModel>,Serializable {
    var serviceId:String=""
    var serviceName:String=""
    var serviceIconPath:String=""
    var itemData:List<OrderBasketDbModel>?=null
init {
    this.serviceId=serviceId
    this.serviceName=serviceName
    this.serviceIconPath=serviceIconPath
    this.itemData=itemData
}


    override fun getChildList(): List<OrderBasketDbModel>? {
        return this.itemData
    }

    override fun isInitiallyExpanded(): Boolean {
        return true
    }


}