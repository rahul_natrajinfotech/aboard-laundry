package com.natrajinfotech.aboardlaundry.model;

public class HomeSliderModel {

    private String sliderTitle;
    private String sliderMsg;
    private String sliderAction;
    private int sliderImage;

    public String getSliderTitle() {
        return sliderTitle;
    }

    public void setSliderTitle(String sliderTitle) {
        this.sliderTitle = sliderTitle;
    }

    public String getSliderMsg() {
        return sliderMsg;
    }

    public void setSliderMsg(String sliderMsg) {
        this.sliderMsg = sliderMsg;
    }

    public String getSliderAction() {
        return sliderAction;
    }

    public void setSliderAction(String sliderAction) {
        this.sliderAction = sliderAction;
    }

    public int getSliderImage() {
        return sliderImage;
    }

    public void setSliderImage(int sliderImage) {
        this.sliderImage = sliderImage;
    }
}
