package com.natrajinfotech.aboardlaundry.model;

import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

/**
 * Created by Dipendra Sharma  on 07-01-2019.
 */
public class OrderBasketDbModel implements Serializable {

    @DatabaseField(columnName = "itemId")
    public String itemId;
    @DatabaseField(columnName = "itemName")
    public String itemName;
    @DatabaseField(columnName = "itemImagePath")
    public String itemImagePath;
    @DatabaseField(columnName = "servicesId")
    public String servicesId;
    @DatabaseField(columnName = "servicesName")
    public String servicesName;
    @DatabaseField(columnName = "serviceRate")
    public String servicesRate;
    @DatabaseField(columnName = "servicesItemQuantity")
    public String servicesItemQuantity;

    @DatabaseField(columnName = "servicesImagePath")
    public String servicesImagePath;

    public OrderBasketDbModel() {
    }

    public OrderBasketDbModel(String itemId, String itemName, String itemImagePath, String servicesId, String servicesName, String servicesRate, String servicesItemQuantity, String servicesImagePath) {
        this.itemId = itemId;
        this.itemName = itemName;
        this.itemImagePath = itemImagePath;
        this.servicesId = servicesId;
        this.servicesName = servicesName;
        this.servicesRate = servicesRate;
        this.servicesItemQuantity = servicesItemQuantity;
        this.servicesImagePath = servicesImagePath;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemImagePath() {
        return itemImagePath;
    }

    public void setItemImagePath(String itemImagePath) {
        this.itemImagePath = itemImagePath;
    }

    public String getServicesId() {
        return servicesId;
    }

    public void setServicesId(String servicesId) {
        this.servicesId = servicesId;
    }

    public String getServicesName() {
        return servicesName;
    }

    public void setServicesName(String servicesName) {
        this.servicesName = servicesName;
    }

    public String getServicesRate() {
        return servicesRate;
    }

    public void setServicesRate(String servicesRate) {
        this.servicesRate = servicesRate;
    }

    public String getServicesItemQuantity() {
        return servicesItemQuantity;
    }

    public void setServicesItemQuantity(String servicesItemQuantity) {
        this.servicesItemQuantity = servicesItemQuantity;
    }

    public String getServicesImagePath() {
        return servicesImagePath;
    }

    public void setServicesImagePath(String servicesImagePath) {
        this.servicesImagePath = servicesImagePath;
    }
}
