package com.natrajinfotech.aboardlaundry.model

import java.io.Serializable

/**
 * Created by Dipendra Sharma  on 23-01-2019.
 */
class GetUserDataResponseModel:Serializable {
    var ResponseCode:String=""
    var ResponseMessage:String=""
    var Data:UserDetail?=null

    class UserDetail:Serializable{
        var id:Int=0
        var first_name:String=""
        var last_name:String=""
        var email:String=""
        var phone:String=""
        var gender:String=""


    }


}