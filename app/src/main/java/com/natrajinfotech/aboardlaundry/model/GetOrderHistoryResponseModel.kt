package com.natrajinfotech.aboardlaundry.model

import java.io.Serializable

/**
 * Created by Dipendra Sharma  on 05-01-2019.
 */
class GetOrderHistoryResponseModel :Serializable{
    var ResponseCode:String=""
    var ResponseMessage:String=""
    var Data:ArrayList<OrderHistoryData>?=null
var total_page:Int=0
    class OrderHistoryData:Serializable{
        var id:Int=0
        var user_id:Int=0
        var order_number:String=""
        var order_date:String=""
        var pickup_day:String=""
        var pickup_month:String=""
        var pickup_year:String=""
        var pickup_from_time:String=""
        var pickup_to_time:String=""
        var delivery_day:String=""
        var delivery_month:String=""
        var delivery_year:String=""
        var delivery_from_time:String=""
        var delivery_to_time:String=""
        var delivery_type:String=""
        var address_id:Int=0
        var total_qty:String=""
        var sub_total:String=""
        var delivery_charges:String=""
        var total:String=""
        var payment_status:String=""
        var notes:String=""
var payment_mode:String=""
        var order_status:String=""
        var status:String=""
        var ordered_approved_status_date:String=""
        var pickup_status_date:String=""
var cleaning_status_date:String=""
var instore_status_date:String=""
var delivery_status_date:String=""

        var created_at:String=""
        var updated_at:String=""
        var item_details:ArrayList<OrderItemDetail>?=null
    }
    class OrderItemDetail:Serializable{
        var id:Int=0
        var order_id:Int=0
        var services_id:Int=0
        var item_id:Int=0
        var rate:Float=0f
        var quantity:Int=0
var sub_total:Float=0f
var created_at:String=""
        var updated_at:String=""
        var services_name:String=""
var item_name:String=""

    }


}