package com.natrajinfotech.aboardlaundry.model

import java.io.Serializable

/**
 * Created by Dipendra Sharma  on 30-01-2019.
 */
class GetNotificationResponseModel:Serializable {
    var ResponseCode:String=""
    var ResponseMessage:String=""
var Data:ArrayList<NotificationData>?=null
var total_page:Int=0


    class NotificationData :Serializable {
    var id:Int=0
    var title:String=""
    var type:String=""
    var description:String=""
    var images:String=""
    var order_id:String=""
    var created_at:String=""



}

}