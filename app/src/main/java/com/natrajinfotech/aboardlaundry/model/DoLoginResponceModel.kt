package com.natrajinfotech.aboardlaundry.model

import java.io.Serializable

/**
 * Created by Dipendra Sharma  on 31-12-2018.
 */
class DoLoginResponceModel:Serializable {
    var ResponseCode:String=""
    var ResponseMessage:String=""
    var Data:LoginData?=null

    class LoginData:Serializable{
        var type:String=""
        var token:String=""


    }

}