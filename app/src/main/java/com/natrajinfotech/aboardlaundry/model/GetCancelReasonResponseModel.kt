package com.natrajinfotech.aboardlaundry.model

import java.io.Serializable

/**
 * Created by Dipendra Sharma  on 25-02-2019.
 */
class GetCancelReasonResponseModel:Serializable {
    var ResponseCode:String=""
    var ResponseMessage:String=""
    var Data:ArrayList<CancelReasonData> ?=null

    class CancelReasonData:Serializable{
var id:Int=0
        var reason:String=""
        var isCheck:Boolean=false

    }

}