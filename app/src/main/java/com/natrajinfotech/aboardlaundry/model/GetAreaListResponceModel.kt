package com.natrajinfotech.aboardlaundry.model

import java.io.Serializable

/**
 * Created by Dipendra Sharma  on 31-12-2018.
 */
class GetAreaListResponceModel :Serializable{
    var ResponseCode:String=""
    var ResponseMessage:String=""
    var Data:ArrayList<AreaListData>?=null

    class AreaListData:Serializable{
        var id:Int=0
        var area_name:String=""


    }

}