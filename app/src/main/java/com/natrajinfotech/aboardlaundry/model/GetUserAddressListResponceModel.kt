package com.natrajinfotech.aboardlaundry.model

import java.io.Serializable

/**
 * Created by Dipendra Sharma  on 05-01-2019.
 */
class GetUserAddressListResponceModel:Serializable {
    var ResponseCode:String=""
    var ResponseMessage:String=""
    var Data:ArrayList<AddressListData>?=null

class AddressListData(
    id: Int,
    name: String,
    building_no: String,
    floor_no: String,
    block_no: String,
    street_no: String,
    area_name: String,
    phone_number: String,
    latitude:String,longitude:String,
    isSelected: Boolean
) :Serializable{
    var id:Int=0
    var name:String=""
var block_no:String=""
var street_no:String=""
    var building_no:String=""

    var area_name:String=""
    var phone_number:String=""
var floor_no:String=""
    var isSelectedAddress:Boolean=false
var latitude:String=""
    var longitude:String=""

    init {
    this.id=id
    this.name=name
    this.building_no=building_no
    this.floor_no=floor_no
    this.block_no=block_no
    this.street_no=street_no
    this.area_name=area_name
    this.phone_number=phone_number
    this.latitude=latitude
        this.longitude=longitude
        this.isSelectedAddress=isSelected
}

}

}