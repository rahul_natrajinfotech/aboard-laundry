package com.natrajinfotech.aboardlaundry.model

import java.io.Serializable

/**
 * Created by Dipendra Sharma  on 04-01-2019.
 */
class GetItemServiceRateResponseModel :Serializable{
    var ResponseCode:String=""
    var ResponseMessage:String=""
    var Data:ArrayList<ItemServiceRateData>?=null
    class  ItemServiceRateData(id:Int,services_name:String,icons:String,rate:Float,icon_path:String,quantity:Int):Serializable{
        var id:Int=0
        var services_name:String=""
       var icons:String=""
        var rate:Float=0f
        var icon_path:String=""
var quantity:Int=0
init {
    this.id=id
    this.services_name=services_name
    this.rate=rate
this.icons=icons
    this.icon_path=icon_path
    this.quantity=quantity
}

    }


}