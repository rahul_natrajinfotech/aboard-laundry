package com.natrajinfotech.aboardlaundry.model

import java.io.Serializable

/**
 * Created by Dipendra Sharma  on 03-01-2019.
 */
class GetItemListResponseModel :Serializable{
    var ResponseCode:String=""
    var ResponseMessage:String=""
    var Data:ArrayList<GroupListData>?=null

    class GroupListData(
        id: String,
        group_name: String,
        icons: String,
        icon_path: String,
        newList: ArrayList<GroupItemListData>
    ) :Parent<GroupItemListData>,Serializable {
        var id:String=""
        var group_name:String=""
        var icons:String=""
        var icon_path:String=""
        var item_data:List<GroupItemListData>?=null
        init {
            this.id=id
            this.group_name=group_name
            this.icons=icons
            this.icon_path=icon_path
            this.item_data=newList
        }
        override fun getChildList(): List<GroupItemListData>? {
            return item_data
        }

        override fun isInitiallyExpanded(): Boolean {
            return false
        }



    }
    class GroupItemListData:Serializable{
        var id:String=""
        var item_name:String=""
        var icons:String=""
        var icon_path:String=""


    }

}