package com.natrajinfotech.aboardlaundry.model

/**
 * Created by Dipendra Sharma  on 24-01-2019.
 */
class GetOrderDetailByIdResponseModel {
    var ResponseCode:String=""
    var ResponseMessage:String=""
    var Data: GetOrderHistoryResponseModel.OrderHistoryData?=null


}

