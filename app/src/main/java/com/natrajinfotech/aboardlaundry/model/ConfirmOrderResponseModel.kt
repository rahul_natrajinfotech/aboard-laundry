package com.natrajinfotech.aboardlaundry.model

import java.io.Serializable

/**
 * Created by Dipendra Sharma  on 22-01-2019.
 */
class ConfirmOrderResponseModel:Serializable {
    var ResponseCode:String=""
    var ResponseMessage:String=""
    var Data:OrderDetail?=null

    class OrderDetail:Serializable{
        var order_id:Int=0
        var order_number:String=""
        var pickup_date:String=""
        var pickup_time:String=""
        var delivery_date:String=""
        var delivery_time:String=""


    }

}