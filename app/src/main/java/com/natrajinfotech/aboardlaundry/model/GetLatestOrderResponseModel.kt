package com.natrajinfotech.aboardlaundry.model

import java.io.Serializable

/**
 * Created by Dipendra Sharma  on 05-01-2019.
 */
class GetLatestOrderResponseModel :Serializable{
    var ResponseCode:String=""
    var ResponseMessage:String=""
    var name:String=""
    var email:String=""
    var Data:ArrayList<LatestOrderData>?=null


    class LatestOrderData:Serializable{
        var id:Int=0
var order_id:String=""
        var order_status:String=""

    }

}