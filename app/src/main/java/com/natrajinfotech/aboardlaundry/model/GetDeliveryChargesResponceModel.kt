package com.natrajinfotech.aboardlaundry.model

import java.io.Serializable

/**
 * Created by Dipendra Sharma  on 10-01-2019.
 */
class GetDeliveryChargesResponceModel :Serializable{
    var ResponseCode:String=""
    var ResponseMessage:String=""
    var Data:DeliveryChargesData?=null

    class DeliveryChargesData:Serializable{
        var id:Int=0
        var delivery_type:String=""
        var charges:Int=0


    }


}