package com.natrajinfotech.aboardlaundry.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.natrajinfotech.aboardlaundry.R;
import com.natrajinfotech.aboardlaundry.model.OrderBasketDbModel;

import java.sql.SQLException;


/**

 * Created by Dipendra Sharma on 1/07/2019.
 */
public class OrmLiteDb extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "orderBasket.db";
    private static final int DATABASE_VERSION = 1;
    public Dao<OrderBasketDbModel, Integer> bucketDao;


    public OrmLiteDb(Context context) {
    super(context, "/mnt/sdcard/" + DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {

        try {
            TableUtils.createTable(connectionSource, OrderBasketDbModel.class);



}    catch (SQLException var4) {
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {

        try {
            TableUtils.dropTable(connectionSource, OrderBasketDbModel.class, true);



            this.onCreate(database, connectionSource);
        } catch (SQLException var6) {
        }

    }

    public Dao<OrderBasketDbModel, Integer> getBucketDao() throws SQLException {
        if (bucketDao == null) {
            bucketDao = getDao(OrderBasketDbModel.class);
        }
        return bucketDao;
    }


}
